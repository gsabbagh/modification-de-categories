from Categorie import Categorie
from Foncteur import Foncteur
from Morphisme import Morphisme
from GrapheDeComposition import GC,MGC
from Diagramme import Diagramme, DiagrammeIdentite, Fleche, DiagrammeObjets
from ChampActif import ChampActif,Cocone
from ChampPerceptif import ChampPerceptif,Cone
from copy import copy
from TransformationNaturelle import TransfoNat
from typing import *

def modification(gc: GC, option_ajout:Tuple[Sequence[Any],Sequence[Morphisme]] = (set(),set()), option_elimination:Sequence[Any] = set(), option_complex_agissante:Sequence[Diagramme] = set(), option_complex_classifiante:Sequence[Diagramme] = set()) -> Tuple[GC,Foncteur]:
    nouveau_gc = copy(gc)
    fonct = Foncteur(gc,nouveau_gc,{o:o for o in gc.objets},{m:m for m in gc[gc.objets,gc.objets]})
    nouveaux_obj, nouvelles_fleches = option_ajout
    nouveau_gc |= set(nouveaux_obj)
    nouveau_gc |= set(nouvelles_fleches)
    nouveau_gc -= set(option_elimination)
    while True:
        print("a")
        nouveau_gc.transformer_graphviz()
        for diag in option_complex_agissante:
            c_a = ChampActif(fonct@diag)
            c_a.transformer_graphviz()
            colimites = c_a.objets_colimites()
            for cocone in c_a.objets:
                Cocone(cocone).transformer_graphviz()
            if len(colimites) == 0:
                # S'il n'y avait pas de colimite, on la créé
                nouvel_objet = "Colimite de "+str(diag)
                while nouvel_objet in nouveau_gc.objets:
                    nouvel_objet += "'"
                nouveau_gc |= {nouvel_objet}
                
                dico_index_jambe = dict() # {i:jambe}
                for i in diag.source.objets:
                    jambe = MGC(diag(i),nouvel_objet)
                    dico_index_jambe[i] = jambe
                    nouveau_gc |= {jambe}
                for obj in diag.source.objets:
                    for fleche in nouveau_gc({diag(obj)},{nouvel_objet}):
                        if dico_index_jambe[obj] != fleche:
                            MGC.identifier_morphismes(fleche,dico_index_jambe[obj])
                for fleche_i in diag.source(diag.source.objets, diag.source.objets):
                    if dico_index_jambe[fleche_i.source] != dico_index_jambe[fleche_i.cible]@diag(fleche_i):
                        if dico_index_jambe[fleche_i.cible]@diag(fleche_i) in nouveau_gc[nouveau_gc.objets,nouveau_gc.objets]:
                            nouveau_gc -= {dico_index_jambe[fleche_i.cible]@diag(fleche_i)}
                        else:
                            MGC.identifier_morphismes(dico_index_jambe[fleche_i.cible]@diag(fleche_i),dico_index_jambe[fleche_i.source])

                #ajouter les flèches vers les autres cocônes
                for cocone in c_a.objets:
                    fleches_arrivant_sur_cocone = c_a[c_a.objets,{cocone}]
                    if next(fleches_arrivant_sur_cocone).is_identite and next(fleches_arrivant_sur_cocone,None) == None:
                        cocone = Cocone(cocone) # gérer le cas où deux cocones ont le même nadir
                        nouvelle_fleche = MGC(nouvel_objet,cocone.nadir)
                        nouveau_gc |= {nouvelle_fleche}
                        for i in diag.source.objets:
                            MGC.identifier_morphismes(nouvelle_fleche@dico_index_jambe[i],cocone(i))
                break
        else:
            for diag in option_complex_classifiante:
                c_p = ChampPerceptif(fonct@diag)
                c_p.transformer_graphviz()
                limites = c_p.objets_limites()
                if len(limites) == 0:
                    # S'il n'y avait pas de limite, on la créé
                    nouvel_objet = "Limite de "+str(diag)
                    print('nouvel_objet'+str(nouvel_objet))
                    while nouvel_objet in nouveau_gc.objets:
                        nouvel_objet += "'"
                    nouveau_gc |= {nouvel_objet}
                    
                    dico_index_jambe = dict() # {i:jambe}
                    for i in diag.source.objets:
                        jambe = MGC(nouvel_objet,diag(i))
                        dico_index_jambe[i] = jambe
                        nouveau_gc |= {jambe}
                    for obj in diag.source.objets:
                        for fleche in nouveau_gc({nouvel_objet},{diag(obj)}):
                            if dico_index_jambe[obj] != fleche:
                                MGC.identifier_morphismes(fleche,dico_index_jambe[obj])
                    for fleche_i in diag.source(diag.source.objets, diag.source.objets):
                        if dico_index_jambe[fleche_i.cible] != diag(fleche_i)@dico_index_jambe[fleche_i.source]:
                            if diag(fleche_i)@dico_index_jambe[fleche_i.source] in nouveau_gc[nouveau_gc.objets,nouveau_gc.objets]:
                                nouveau_gc -= {diag(fleche_i)@dico_index_jambe[fleche_i.source]}
                            else:
                                MGC.identifier_morphismes(diag(fleche_i)@dico_index_jambe[fleche_i.source],dico_index_jambe[fleche_i.cible])
                    #ajouter les flèches depuis les autres cônes finaux
                    for cone in c_p.objets:
                        fleches_partant_de_cone = c_p[{cone},c_p.objets]
                        if next(fleches_partant_de_cone).is_identite and next(fleches_partant_de_cone,None) == None:
                            cone = Cone(cone)
                            nouvelle_fleche = MGC(cone.apex,nouvel_objet)
                            nouveau_gc |= {nouvelle_fleche}
                            for i in diag.source.objets:
                                MGC.identifier_morphismes(dico_index_jambe[i]@nouvelle_fleche,cone(i))
                    nouveau_gc.transformer_graphviz()
                    break
            else:
                break # si on a rien ajouté on peut enfin sortir
            
        
    return nouveau_gc,fonct
    
def test_Complexification():
    cat_i = GC(set("ABC"))
    f_i,g_i = [MGC('B','A','f'),MGC('B','C','g')]
    cat_i |= {f_i,g_i}

    cat = GC(set("ABCDEZ"))
    f,g,h,i,j,k,l,m,n = [MGC('B','A','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('B','D','i'),MGC('C','D','j'),MGC('A','E','k'),MGC('B','E','l'),MGC('C','E','m'),MGC('Z','B','n')]
    cat |= {f,g,h,i,j,k,l,m,n}

    d_fleche = Fleche(cat,m)
    d_fleche.transformer_graphviz()
    ChampActif(d_fleche).transformer_graphviz()

    diag = Diagramme(cat_i,cat,{'A':'A','B':'B','C':'C'},{f_i:f,g_i:g})

    c_p = ChampPerceptif(diag)
    for cone in c_p.objets:
        Cone(cone).transformer_graphviz(complet=False,afficher_identites=False)

    c_p.transformer_graphviz()

    diag2 = DiagrammeIdentite(cat)
    diag2.faire_commuter()

    diag.transformer_graphviz()
    cat.transformer_graphviz()

    ChampActif(diag).transformer_graphviz()
    cat2,fonct = modification(cat,(set(),set()),{},{diag},{diag})
    cat2.transformer_graphviz()
    fonct.transformer_graphviz()

    diag = fonct@diag
    diag.transformer_graphviz()
    ChampActif(diag).transformer_graphviz()
    for cone in ChampActif(diag).objets_colimites():
        Cocone(cone).transformer_graphviz()
        
    d_fleche = Fleche(cat,m)
    d_fleche.transformer_graphviz()
    ChampActif(d_fleche).transformer_graphviz()
        
# def test_Complexification():
    # cat = GC(set("ABCDEF"))
    # f,g,h,i,j,k,l = [MGC('A','B','f'),MGC('A','D','g'),MGC('C','B','h'),MGC('C','E','i'),MGC('C','F','j'),MGC('D','E','k'),MGC('D','F','l')]
    # cat |= {f,g,h,i,j,k,l}

    # diag1, diag2 = DiagrammeObjets(cat,set("BD")),DiagrammeObjets(cat,set("EF"))

    # ChampPerceptif(diag1).transformer_graphviz()
    # ChampPerceptif(diag2).transformer_graphviz()
    # cat2,fonct = modification(cat,(set(),set()),set(),set(),{diag1,diag2})
    # cat2.transformer_graphviz()
    # fonct.transformer_graphviz()

    # diag1 = fonct@diag1
    # diag2 = fonct@diag2
    # ChampPerceptif(diag1).transformer_graphviz()
    # ChampPerceptif(diag2).transformer_graphviz()
    # for cone in ChampPerceptif(diag1).objets_limites():
        # Cone(cone).transformer_graphviz()
    # for cone in ChampPerceptif(diag2).objets_limites():
        # Cone(cone).transformer_graphviz()
    
def test_Complexification2():
    import random
    from CategorieAleatoire import GCA, FoncteurAleatoire
    
    # random.seed(123)
    random.seed(12345)
    
    cat = GC(set("ABC"))
    f,g = [MGC('B','A','f'),MGC('B','C','g')]
    cat |= {f,g}
    
    for i in range(20):
        c_i = GCA(random.randint(1,5),random.randint(0,5))
        # c_i = GCA(random.randint(1,5),random.randint(0,10))
        c_i.transformer_graphviz()
        diag = FoncteurAleatoire(c_i,cat)
        diag.as_diagram().transformer_graphviz()
        diag.transformer_graphviz()
        cat2,fonct = modification(cat,(set(),set()),{},{diag},{diag})
        cat2.transformer_graphviz()
        fonct.transformer_graphviz()
        cat=cat2
        
        
if __name__ == '__main__':
    test_Complexification()
    # test_Complexification2()

  