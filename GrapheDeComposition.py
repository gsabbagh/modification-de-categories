from Categorie import Categorie, unique_generator
from CategorieLibre import CategorieLibre
from Morphisme import Morphisme
from collections import defaultdict
from config import *
from typing import *

class MorphismeGrapheDeComposition(Morphisme):
    """
    `MorphismeGrapheDeComposition` peut être abrégé en MGC
    Un morphisme dans un graphe de composition est un chemin dans ce graphe.
    Deux morphismes peuvent être identifiés dans le graphe de composition grâce à la loi de composition statique.
    Le chemin est représenté par un tuple de morphismes de graphe de composition.
    Si un morphisme appartient à deux catégories, les lois de composition des deux graphes de composition sont partagées.
    """
    loi_de_composition = dict() # on associe à certains chemins un autre chemin aussi long ou plus court
    #{tuple de morphismes : tuple de morphismes}
    
    def identifier_morphismes(morph1:'MorphismeGrapheDeComposition',morph2:'MorphismeGrapheDeComposition'):
        """Identifie le `morph1` au `morph2` /!\ dans cet ordre (c-a-d morph1 devient morph2) /!\.
        `morph1` ne doit pas être élémentaire.
        Cette fonction statique modifie la loi de composition de la classe `MorphismeGrapheDeComposition`.
        Les morphismes seront identifiés indépendamment des catégories auxquelles ils appartiennent."""
        if len(morph1.__chemin) <= 1:
            print("Warning : identification d'un morphisme élémentaire : "+str(morph1)+" identifie a "+str(morph2))
        MorphismeGrapheDeComposition.loi_de_composition[morph1.__chemin] = morph2.__chemin
        MorphismeGrapheDeComposition.verifier_coherence()
    
    def verifier_coherence():
        '''Vérifie la cohérence de la loi de composition de la classe `MorphismeGrapheDeComposition`.'''
        for chemin in MorphismeGrapheDeComposition.loi_de_composition:
            if chemin == MorphismeGrapheDeComposition.loi_de_composition[chemin]:
                raise Exception("Entree de la loi de composition inutile "+str(chemin))
        
    def __simplifier_chemin(morph1:'MorphismeGrapheDeComposition',morph2:'MorphismeGrapheDeComposition') -> tuple:
        """
        On simplifie le chemin défini par la composition des deux morphismes grâce à la loi de composition.
        On renvoie un tuple de morphismes.
        """
        chemin = tuple([morph for morph in morph1.__chemin+morph2.__chemin if not morph.is_identite])
        if len(chemin) == 0: #s'il n'y avait que des identités
            return (morph1.__chemin[0],) #on renvoie une des identités
        # on unpack les différents chemins en un seul chemin et on retire les identités
        while True: #on continue tant qu'on a simplifié le chemin
            for nb_morphismes_consideres in range(len(chemin), 1, -1):
                for offset in range(0, len(chemin) - nb_morphismes_consideres + 1):
                    sous_composee = chemin[offset:offset + nb_morphismes_consideres]
                    if sous_composee in MorphismeGrapheDeComposition.loi_de_composition: # si il y a une simplification dans la loi de composition
                        chemin = chemin[0:offset] + MorphismeGrapheDeComposition.loi_de_composition[sous_composee]\
                        + chemin[offset + nb_morphismes_consideres:]
                        break
                else:
                    continue
                break
            else:
                break #on quitte si on a jamais break avant (si on a jamais simplifié)
        chemin_resultat = tuple([morph for morph in chemin if not morph.is_identite])# on retire les identités
        if len(chemin_resultat) == 0: #s'il n'y avait que des identités
            return (chemin[0],) #on renvoie une des identités
        return chemin_resultat


    def __init__(self, source:Any, cible:Any, representant:Any=None, is_identite:bool = False):
        self.__chemin = (self,) # le chemin sera overwrite par matmul si le morphisme est une composition
        Morphisme.__init__(self, source, cible, representant, is_identite)
        if TOUJOURS_VERIFIER_COHERENCE:
            MorphismeGrapheDeComposition.verifier_coherence()

    def __getitem__(self,entier:int) -> Morphisme:
        '''Renvoie un morphisme du chemin.'''
        return self.__chemin[entier]
    
    def __iter__(self):
        '''On peut itérer sur les morphismes du chemin.'''
        for morph in self.__chemin:
            yield morph
            
    def __len__(self) -> int:
        '''Taille du chemin.'''
        return len(self.__chemin)
        
    def __matmul__(self,other:'MorphismeGrapheDeComposition') -> 'MorphismeGrapheDeComposition':
        """
            `self` @ `other` <=> `self` o `other`
            On compose les morphismes en concaténant les chemins et en simplifiant le résultat avec la loi de composition.
        """
        if TOUJOURS_VERIFIER_COHERENCE_COMPOSEE:
            MorphismeGrapheDeComposition.verifier_coherence()
        if not issubclass(type(other),MorphismeGrapheDeComposition):
            raise Exception("Composition d'un morphisme de graphe de composition avec un morphisme de type different : "+str(self)+" o "+str(other)+"\n"+str(type(self))+" et "+str(type(other)))
        if self.source != other.cible:
            raise Exception("Composition de morphismes pas composables : "+str(self)+" o "+str(other))
        if self.is_identite:
            return other
        if other.is_identite:
            return self
        # à partir d'ici on doit instancier un nouveau morphisme
        chemin_simplifie = MorphismeGrapheDeComposition.__simplifier_chemin(self,other)
        if len(chemin_simplifie) == 1: # si on a simplifié à un morphisme on le renvoie
            return chemin_simplifie[0]
        else: #sinon on doit créer un morphisme dont le chemin est celui qu'on a trouvé
            nouveau_morph = MorphismeGrapheDeComposition(other.source,self.cible,representant='o'.join(map(str,chemin_simplifie)),is_identite=False)
            nouveau_morph.__chemin = chemin_simplifie
            return nouveau_morph
            
    def __eq__(self,other:'MorphismeGrapheDeComposition') -> bool:
        if not issubclass(type(other),MorphismeGrapheDeComposition):
            return False
        if len(self.__chemin) == 1:
            return len(other.__chemin) == 1 and self.__chemin[0] is other.__chemin[0]
        return self.__chemin == other.__chemin
        
    def __hash__(self) -> int:
        if len(self.__chemin) == 1:
            return super().__hash__()
        return hash(self.__chemin)
        
MGC = MorphismeGrapheDeComposition

class GrapheDeComposition(CategorieLibre):
    """
    `GrapheDeComposition` peut être abrégé en `GC`.
    Un graphe de composition pour nous est un graphe muni d'une loi de composition et d'identités.
    On peut en tirer une catégorie : la catégorie libre engendrée par ce graphe de composition.
    Cette classe implémente les deux concepts ci-dessus, on peut modifier le graphe de composition et obtenir les morphismes du modèle.
    Concrètement, pour un modèle de graphe de composition, on peut :
        - ajouter un objet ou un morphisme avec |=
        - faire commuter un diagramme qui a pour cible ce modèle pour identifier des flèches
    """
    __id = 0
    
    def __init__(self, objets:set = set(), nom:str = None):
        GrapheDeComposition.__id += 1
        self.__id = GrapheDeComposition.__id
        self.__identites = dict()
        self.__morph_entrants = defaultdict(frozenset)
        self.__morph_sortants = defaultdict(frozenset)
        CategorieLibre.__init__(self,objets,"Graphe de Composition "+str(self.__id) if nom == None else nom)
     
    def __copy__(self) -> "GrapheDeComposition":
        nouveau_gc = GrapheDeComposition(self.objets)
        for f in self[self.objets,self.objets]:
            if not f.is_identite:
                nouveau_gc |= {f}
            else:
                nouveau_gc.__morph_entrants[f.source] -= {nouveau_gc.identite(f.source)}
                nouveau_gc.__morph_entrants[f.source] |= {f}
                nouveau_gc.__morph_sortants[f.source] -= {nouveau_gc.identite(f.source)}
                nouveau_gc.__morph_sortants[f.source] |= {f}
                nouveau_gc.__identites[f.source] = f
        return nouveau_gc
     
    def verifier_coherence(self):
        CategorieLibre.verifier_coherence(self)
        if self.objets != set(self.__identites.keys()):
            raise Exception("Incohérence GrapheDeComposition : il n'y a pas correspondance entre identités et objets : "+str(self.objets)+"\n"+str(self.__identites))
    
    @unique_generator
    def __getitem__(self, couple_sources_cibles:Tuple[frozenset,frozenset]) -> Generator[MorphismeGrapheDeComposition,None,None]:
        sources,cibles = couple_sources_cibles
        for source in sources:
            for cible in cibles:
                for fleche in self.morph_sortants(source)&self.morph_entrants(cible):
                    yield fleche
    
    def __eq__(self,other:'GrapheDeComposition') -> bool:
        return type(other) == type(self) and self.__identites == other.__identites and\
        self.__morph_entrants == other.__morph_entrants and self.__morph_sortants == other.__morph_sortants
    
    def __hash__(self) -> int:
        return hash((CategorieLibre.__hash__(self),frozenset(self.__identites.items()),\
        frozenset(self.__morph_entrants.items()),frozenset(self.__morph_sortants.items())))
    
    def identite(self,objet:Any) -> MorphismeGrapheDeComposition:
        if objet not in self.__identites:
            raise Exception("Tentative d'acceder a l'identite d'un objet qui n'est pas dans la categorie, "+str(objet)+" n'est pas dans "+str(self.__identites))
        return self.__identites[objet]
            
    def __ior__(self,ensemble_objet_ou_morphisme:set) -> 'GrapheDeComposition':
        """
        `objet_ou_morphisme` est considéré comme un morphisme s'il hérite de MorphismeGrapheDeComposition, c'est un objet sinon.
        Si on veut ajouter un morphisme en tant qu'objet, appeler Categorie.__ior__(cat,morphisme).
        Si le `morphisme` n'appartient pas déjà à la catégorie :
            on met à jour le graphe de composition en mettant à jour les dictionnaires morph_entrants et morph_sortants.
        Si l'`objet` n'appartient pas déjà à la catégorie :
            on ajoute l'`objet`,
            on créé une identité,
            on met à jour le graphe de composition en mettant à jour les dictionnaires morph_entrants et morph_sortants.
        """
        for objet_ou_morphisme in ensemble_objet_ou_morphisme:
            if issubclass(type(objet_ou_morphisme), MorphismeGrapheDeComposition):
                if not objet_ou_morphisme.is_identite:
                    self.__morph_entrants[objet_ou_morphisme.cible] |= {objet_ou_morphisme}
                    self.__morph_sortants[objet_ou_morphisme.source] |= {objet_ou_morphisme}
            elif objet_ou_morphisme not in self.objets:
                CategorieLibre.__ior__(self,{objet_ou_morphisme})
                self.__identites[objet_ou_morphisme] = MorphismeGrapheDeComposition(objet_ou_morphisme,objet_ou_morphisme,'Id'+str(objet_ou_morphisme),True)
                self.__morph_entrants[objet_ou_morphisme] |= {self.__identites[objet_ou_morphisme]}
                self.__morph_sortants[objet_ou_morphisme] |= {self.__identites[objet_ou_morphisme]}
        return self
        
    def __isub__(self, ensemble_objet_ou_morphisme:set) -> 'GrapheDeComposition':
        '''
        On supprime l'`objet` et son identité.
        On supprime le `morphisme` et on met à jour le graphe de composition.'''
        for objet_ou_morphisme in ensemble_objet_ou_morphisme:
            if issubclass(type(objet_ou_morphisme),MorphismeGrapheDeComposition):
                self.__morph_entrants[objet_ou_morphisme.cible] -= {objet_ou_morphisme}
                self.__morph_sortants[objet_ou_morphisme.source] -= {objet_ou_morphisme}
            else:
                CategorieLibre.__isub__(self,{objet_ou_morphisme})
                del self.__identites[objet_ou_morphisme]
                for m in self.__morph_entrants[objet_ou_morphisme]:
                    self.__morph_sortants[m] -= {objet_ou_morphisme}
                for m in self.__morph_sortants[objet_ou_morphisme]:
                    self.__morph_entrants[m] -= {objet_ou_morphisme}
                del self.__morph_entrants[objet_ou_morphisme]
                del self.__morph_sortants[objet_ou_morphisme]
            if TOUJOURS_VERIFIER_COHERENCE:
                self.verifier_coherence()
        return self
        
    def morph_entrants(self,objet:Any) -> frozenset():
        return self.__morph_entrants[objet]
       
    def morph_sortants(self,objet:Any) -> frozenset:
        return self.__morph_sortants[objet]
        
    
GC = GrapheDeComposition

def test_GrapheDeComposition():
    GC = GrapheDeComposition()
    GC |= {'A','B','C'}
    morphismes = [MGC('A','B','f'),MGC('B','C','g')]
    GC |= morphismes
    GC.transformer_graphviz()
  
if __name__ == '__main__':
    test_GrapheDeComposition()