from CommaCategorie import CommaCategorie, ObjetCommaCategorie
from TransformationNaturelle import CatTransfoNat,TransfoNat
from Foncteur import Foncteur
from Diagramme import DiagrammeConstant,DiagrammeObjets
from Cluster import ClusterActif,ProtoClusterActif
from typing import *
import config
if config.GRAPHVIZ_ENABLED:
    from graphviz import Digraph
import itertools

class Cocone(TransfoNat):
    """Un cocône sur D de nadir N est une transformation naturelle de D vers le foncteur constant Δ_N."""
    def __init__(self, objet_champ_actif:ObjetCommaCategorie):
        self.__nadir = objet_champ_actif.d # attribut read-only
        f = objet_champ_actif.f
        TransfoNat.__init__(self,f.source,f.cible,f._composantes,f.nom)
    
    @property
    def nadir(self):
        return self.__nadir
        
    def as_ObjetCommaCategorie(self) -> ObjetCommaCategorie:
        return ObjetCommaCategorie(0,self,self.nadir)
        
    def as_Cluster(self) -> ClusterActif:
        return ClusterActif(ProtoClusterActif(self.source,self.cible,self._composantes,"Cluster issu du cocône "+str(self)))

class ChampActif(CommaCategorie):
    """Le champ actif d'un diagramme(ou foncteur) D:P->Q est défini comme étant la comma-catégorie (D|Δ)
    (cf. https://en.wikipedia.org/wiki/Cone_(category_theory)#Equivalent_formulations)
    Un objet (Id,C,A) du champ actif nous donne un cocône C de nadir A sur le diagramme D.
    Δ:Q -> (P->Q) est un foncteur dans une catégorie infinie, on ne s'intéresse qu'aux diagrammes constants Δq : P->Q, _|->q,_|->1q avec q \in Q
    et au diagramme D.
    En ne s'intéressant qu'à ces diagrammes, on obtient quand même tous les cocônes sur D.
    """
    nb_viz = 0
    
    def __init__(self, diagramme:Foncteur, nom:str = None):
        P = diagramme.source
        Q = diagramme.cible
        self.categorie_diagrammes = CatTransfoNat({DiagrammeConstant(P,Q,o) for o in Q.objets},
        "Categorie de diagrammes du champ actif de "+str(diagramme))
        self.categorie_diagrammes |= {diagramme}
        
        self.foncteur_diagonal = Foncteur(Q,self.categorie_diagrammes,{o:DiagrammeConstant(P,Q,o) for o in Q.objets},
        {f: TransfoNat(DiagrammeConstant(P,Q,f.source),DiagrammeConstant(P,Q,f.cible),{p:f for p in P.objets}) for f in Q(abs(Q),abs(Q))})
        ## on associe à chaque objet son diagramme constant et à chaque flèche F:a->b la transformation naturelle qui va de Δa à Δb
        
        self.foncteur_vers_D = DiagrammeObjets(self.categorie_diagrammes,{diagramme})
        
        CommaCategorie.__init__(self,self.foncteur_vers_D,self.foncteur_diagonal, "Champ actif de "+str(diagramme) if nom == None else nom)
        
    def objets_cocones(self,nadirs:set) -> set:
        """`nadirs` est un ensemble d'objets de Q
        Cette fonction renvoie l'ensemble tous les cocônes de nadir un objets d'`nadirs`.
        Un cocône est une transformation naturelle de D vers le diagramme constant sur le nadir.
        Cette fonction renvoie les objets de la comma-catégorie qui contiennent ces cocônes."""
        return {obj for obj in self.objets for nadir in nadirs if obj.d == nadir}
    
    def objets_colimites(self) -> set:
        """Renvoie un ensemble de cocônes colimites.
        Les cocônes colimites sont des objets initiaux dans le champ actif.
        Cette fonction renvoie les objets de la comma-catégorie qui contiennent ces cocônes."""
        if len(self.objets) == 0:
            return set()
        cocones = list(self.objets)
        cocone_courant = cocones[0]
        cocones_visites = {cocone_courant}
        cocones_restants = self.objets-cocones_visites
        fleche_accessible = next(self[cocones_restants,{cocone_courant}],None)
        if fleche_accessible == None:
            fleche_accessible = next(self(cocones_restants,{cocone_courant}),None)
        while fleche_accessible != None:            
            cocone_courant = fleche_accessible.source
            cocones_visites |= {cocone_courant}
            cocones_restants = self.objets-cocones_visites
            fleche_accessible = next(self[cocones_restants,{cocone_courant}],None)
            if fleche_accessible == None:
                fleche_accessible = next(self(cocones_restants,{cocone_courant}),None)
        # ici cocone_courant est un objet initial de la catégorie
        result = set()
        for cocone in self.objets:
            generateur = self({cocone_courant},{cocone}) #permet de vérifier qu'il n'y a pas plus d'une flèche entre les deux cocones
            next(generateur,None)
            if self.existe_isomorphisme(cocone_courant,cocone) != None:
                result |= {cocone}
            elif self.existe_morphisme(cocone_courant,cocone) == None:
                return set()
            elif cocone != cocone_courant and next(generateur,None) != None:
                return set()
        return result
        
    def transformer_graphviz(self, destination:Union[str,None] = None, afficher_identites:bool = True, complet:bool = True, limite_fleches:int = 30):
        """Permet de visualiser la catégorie avec graphviz.
        `complet` indique s'il faut afficher toutes les flèches ou seulement les flèches élémentaires.
        `limite_fleches` est le nombre maximal de flèches entre deux objets qu'on s'autorise à afficher."""
        ChampActif.nb_viz += 1
        if destination == None:
            destination = "graphviz/"+type(self).__name__+ str(ChampActif.nb_viz)

        graph = Digraph('champ_actif')
        graph.attr(concentrate="true" if config.GRAPHVIZ_CONCENTRATE_GRAPHS else "false")
        graph.attr(label=self.nom)
        
        colimites = self.objets_colimites()
        for o in self.objets:
            graph.node(str(o),color="yellow" if o in colimites else "black")
        fleches_elem = set(self[self.objets,self.objets])
        if complet:
            func = lambda x,y : self({x},{y})
        else:
            func = lambda x,y : self[{x},{y}]
        for source,cible in itertools.product(self.objets,repeat=2):
            nb_fleches = 0
            for fleche in func(source,cible):
                if afficher_identites or not fleche.is_identite:
                    if fleche.source not in self.objets:
                        a = fleche.source not in self.objets
                        raise Exception("Source d'une fleche pas dans les objets de la categorie "+repr(fleche.source)+"\n"+str(list(self.objets))+"\n"+str(fleche.source in self.objets))
                    if fleche.cible not in self.objets:
                        raise Exception("Source d'une fleche pas dans les objets de la categorie."+str(fleche.cible)+"\n"+str(self.objets))
                    if len({obj for obj in self.objets if obj == fleche.source}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est source de la fleche.")
                    if len({obj for obj in self.objets if obj == fleche.cible}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est cible de la fleche.")
                    source = {obj for obj in self.objets if obj == fleche.source}.pop() #permet d'avoir toujours un objet de la catégorie comme source
                    cible = {obj for obj in self.objets if obj == fleche.cible}.pop() #permet d'avoir toujours un objet de la catégorie comme cible
                    graph.edge(str(source), str(cible), label=str(fleche), weight="1000", color="grey80" if fleche not in fleches_elem else "black")
                    nb_fleches += 1
                if nb_fleches > limite_fleches:
                    if config.WARNING_LIMITE_FLECHES_ATTEINTE:
                        print("Warning : limite fleches entre "+str(source)+" et "+str(cible)+" atteinte.")
                        break

        graph.render(destination)
        if config.CLEAN_GRAPHVIZ_MODEL:
            import os
            os.remove(destination)
        
def test_ChampActif():
    from GrapheDeComposition import GC,MGC
    from Diagramme import Triangle,DiagrammeIdentite
    
    gc = GC()
    gc |= set("ABCDEF")
    f,g,h,i,j,k,l = [MGC('A','B','f'),MGC('B','C','g'),MGC('D','E','h'),MGC('E','F','i'),MGC('A','D','j'),MGC('B','E','k'),MGC('C','F','l')]
    gc |= {f,g,h,i,j,k,l}
    
    diag_identite = DiagrammeIdentite(gc)
    # diag_identite.faire_commuter()
    gc.transformer_graphviz()
    
    d1 = Triangle(gc,"ABC",[f,g,g@f])
    d1.transformer_graphviz()
    c_p = ChampActif(d1)
    for cocone in c_p.objets_cocones(gc.objets):
        Cocone(cocone).transformer_graphviz()
    c_p.transformer_graphviz()
    
    colimites = list(c_p.objets_colimites())
    for i in range(len(colimites)):   
        print("colimite")
        Cocone(colimites[i]).nom = "Colimite "+str(i)
        print(str(colimites))
        Cocone(colimites[i]).transformer_graphviz()
 
if __name__ == '__main__':
    test_ChampActif()