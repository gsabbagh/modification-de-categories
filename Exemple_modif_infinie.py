from GrapheDeComposition import GC, MGC
from ChampPerceptif import ChampPerceptif, Cone
from ChampActif import ChampActif, Cocone
from Diagramme import Diagramme, DiagrammeIdentite

gc = GC(range(8))
f,g,h,i,j,k,l,m,n,o,p,q = [MGC(0,2,'f'),MGC(1,2,'g'),MGC(3,5,'h'),MGC(4,5,'i'),MGC(6,0,'j'),MGC(6,1,'k'),MGC(7,3,'l'),MGC(7,4,'m'),MGC(7,0,'n'),MGC(7,1,'o'),MGC(6,3,'p'),MGC(6,4,'q')]
gc |= {f,g,h,i,j,k,l,m,n,o,p,q}
d = DiagrammeIdentite(gc)
d.faire_commuter()
gc.transformer_graphviz()

J = GC(range(3))
a,b = [MGC(0,2),MGC(1,2)]
J |= {a,b}

d1 = Diagramme(J,gc,{0:0,1:1,2:2},{a:f,b:g})
d1.transformer_graphviz()
d2 = Diagramme(J,gc,{0:3,1:4,2:5},{a:h,b:i})
d2.transformer_graphviz()

c_p1 = ChampPerceptif(d1)
c_p1.transformer_graphviz()
c_p2 = ChampPerceptif(d2)
c_p2.transformer_graphviz()

x = MGC(6,7,'x')
y = MGC(7,6,'y')
gc |= {x,y}

MGC.identifier_morphismes(o@x,k)
MGC.identifier_morphismes(n@x,j)
MGC.identifier_morphismes(q@y,m)
MGC.identifier_morphismes(p@y,l)

# MGC.identifier_morphismes(x@y,gc.identite(7))
# MGC.identifier_morphismes(y@x,gc.identite(6))

# MGC.identifier_morphismes(x@y@x@y,x@y)
# MGC.identifier_morphismes(y@x@y@x,y@x)

MGC.identifier_morphismes(x@y@x@y@x@y,gc.identite(7))
MGC.identifier_morphismes(y@x@y@x@y@x,gc.identite(6))

# MGC.identifier_morphismes(x@y@x@y@x@y,x@y@x@y)
# MGC.identifier_morphismes(y@x@y@x@y@x,y@x@y@x)

gc.transformer_graphviz()

c_p1 = ChampPerceptif(d1)
c_p1.transformer_graphviz()
c_p2 = ChampPerceptif(d2)
c_p2.transformer_graphviz()

for cone in c_p1.objets_limites():
    Cone(cone).transformer_graphviz()