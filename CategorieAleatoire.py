from GrapheDeComposition import GC,MGC
from Morphisme import Morphisme
from Foncteur import Foncteur
from Monoide import Monoide,ElementMonoideGC,MonoideGC
from CategoriePreordre import CategoriePreordreInstanciable
from Categorie import Categorie
from CategorieProduit import CategorieProduit
from CategorieLibre import CategorieLibre
from CategorieComposantesConnexes import CatCC
from CategorieAcyclique import CatAcyclique
from CategorieOrdre import CatOrdre
from CategorieFine import CatFine
from CategorieOrdreTotal import CatOrdreTotal
import random
import copy
import itertools
from collections import defaultdict
from config import *
from typing import *

from pprint import pprint

if PROGRESS_BAR:
    from tqdm import tqdm

from graphviz import Digraph
  
class GrapheCompositionAleatoire(GC):
    """Construit un graphe de composition aléatoire.
    GrapheCompositionAleatoire peut être abrégé en GCA"""
    _id = 0
    
    def __init__(self, nb_fleches:Union[int,None] = None, nb_tentatives_complexification_loi_de_compo:Union[int,None] = None, nb_tentatives_correction_composition:Union[Callable,int,None] = None, proba_identifier_identites:float = 0.2, nom:Union[str,None] = None):
        """`nb_fleches` est le nombre de flèches élémentaires dans la catégorie aléatoire.
        `nb_tentatives_complexification_loi_de_compo` détermine à quel point la loi de composition sera complexifiée,
        si le nombre est faible on obtient une catégorie où les flèches sont isolées.
        Par défaut, un nombre est tiré au hasard entre 0 et 10*`nb_fleches`
        `nb_tentatives_correction_composition` détermine à quel point on pousse loin la recherche de correction de la loi de composition par
        rapport à la contrainte de composition.
        Si le nombre est faible, on aura quasiment aucun cycle, si le nombre est grand, la création de cycles et favorisée mais le temps de calcul est plus long.
        Par défaut, un nombre est tiré au hasard à chaque fois entre 3 et 6.
        `proba_identifier_identites` donne la probabilité que la tentative de complexification soit une identification d'identités,
        plus la probabilité est grande et plus les catégories seront connexes."""
        GrapheCompositionAleatoire._id += 1
        self._id = GrapheCompositionAleatoire._id
        if nom == None:
            nom = "Catégorie aléatoire "+str(self._id)
        
        if nb_fleches == None:
            nb_fleches = random.randint(1,20)
            
        if nb_tentatives_complexification_loi_de_compo == None:
            nb_tentatives_complexification_loi_de_compo = random.randint(0,10*nb_fleches)
        
        GC.__init__(self,nom=nom)
        
        class LoiDeComposition:
            """La loi de composition va suivre une structure d'auto-graphe : chaque morphisme a pour source et pour cible une auto-flèche identité."""
            def __init__(self,nb_fleches):
                self.table = defaultdict(lambda:None) #){ (i,j) : j o i}
                #on initialise les identités
                for i in range(nb_fleches):
                    self.table[(i+nb_fleches,i)] = i
                    self.table[(i+nb_fleches,i+nb_fleches)] = i+nb_fleches
                    self.table[(i,i+2*nb_fleches)] = i
                    self.table[(i+2*nb_fleches,i+2*nb_fleches)] = i+2*nb_fleches
                self.source = dict() # dictionnaire qui associe à chaque flèche son identité source
                for i in range(nb_fleches):
                    self.source[i] = i+nb_fleches
                    self.source[i+nb_fleches] = i+nb_fleches
                    self.source[i+2*nb_fleches] = i+2*nb_fleches
                self.cible = dict() # dictionnaire qui associe à chaque flèche son identité cible
                for i in range(nb_fleches):
                    self.cible[i] = i+2*nb_fleches
                    self.cible[i+nb_fleches] = i+nb_fleches
                    self.cible[i+2*nb_fleches] = i+2*nb_fleches
            
            def identifier_identites(self,id1:int,id2:int):
                #on va identifier l'identité 1 à l'identité 2 
                assert(id1 in self.source.values() or id1 in self.cible.values())
                assert(id2 in self.source.values() or id2 in self.cible.values())
                if id1 != id2:
                    # on update les contraintes de composition de id1 à celles de id2
                    for i in range(nb_fleches):
                        if self.table[(id1,i)] != None:
                            assert(self.table[(id2,i)] == None)
                            self.table[(id2,i)] = i
                        if self.table[(i,id1)] != None:
                            assert(self.table[(i,id2)] == None)
                            self.table[(i,id2)] = i
                    # on supprime l'auto-flèche id1
                    del self.source[id1]
                    del self.cible[id1]
                    for i in range(nb_fleches):
                        if (id1,i) in self.table:
                            del self.table[(id1,i)]
                        if (i,id1) in self.table:
                            del self.table[(i,id1)]
                    # on remplace toutes les occurences de id1 par id2
                    for i in range(nb_fleches):
                        for j in range(nb_fleches):
                            if self.table[(i,j)] == id1:
                                self.table[(i,j)] = id2
                        if self.source[i] == id1:
                            self.source[i] = id2
                        if self.cible[i] == id1:
                            self.cible[i] = id2
            
            @property
            def fleches(self) -> set:
                assert(set(self.source.keys()) == set(self.cible.keys()))
                return set(self.source.keys())
            
            @property
            def identites(self) -> set:
                return {f for f in self.source if f >= nb_fleches}
        
        def tentative_complexification(loi:LoiDeComposition,i:int,j:int,nb_tentatives:int = 3) -> Union[LoiDeComposition,None]:
            """Tente de complexifier la loi de composition en définissant la composition j o i.
            Si i est une identité, alors on tente d'identifier i à une autre identité.
            Renvoie une nouvelle loi de composition si succès, None sinon."""
            nouvelle_loi = copy.deepcopy(loi)
            if i < nb_fleches:
                # définition d'une composée
                if nouvelle_loi.table[(i,j)] != None:
                    return None
                ij = random.choice(list(nouvelle_loi.fleches))
                nouvelle_loi.table[(i,j)] = ij
                nouvelle_loi.identifier_identites(nouvelle_loi.source[i],nouvelle_loi.source[ij])
                nouvelle_loi.identifier_identites(nouvelle_loi.cible[j],nouvelle_loi.cible[ij])
                nouvelle_loi.identifier_identites(nouvelle_loi.cible[i],nouvelle_loi.source[j])
            else:
                # identification d'identités
                ij = random.choice(list(nouvelle_loi.identites))
                nouvelle_loi.identifier_identites(i,ij)
            while True:
                for a,b,c in itertools.product(range(nb_fleches),repeat=3):
                    ab = nouvelle_loi.table[(a,b)]
                    bc = nouvelle_loi.table[(b,c)]
                    if ab != None and bc != None:
                        if nouvelle_loi.table[(ab,c)] != nouvelle_loi.table[(a,bc)]:
                            # l'associativité n'est pas vérifiée
                            if nouvelle_loi.table[(ab,c)] == None:
                                #on peut corriger la table
                                abc = nouvelle_loi.table[(a,bc)]
                                nouvelle_loi.table[(ab,c)] = abc
                                nouvelle_loi.identifier_identites(nouvelle_loi.source[ab],nouvelle_loi.source[abc])
                                nouvelle_loi.identifier_identites(nouvelle_loi.cible[c],nouvelle_loi.cible[abc])
                                nouvelle_loi.identifier_identites(nouvelle_loi.cible[ab],nouvelle_loi.source[c])
                            if nouvelle_loi.table[(a,nouvelle_loi.table[(b,c)])] == None:
                                #on peut corriger la table
                                abc = nouvelle_loi.table[(ab,c)]
                                nouvelle_loi.table[(a,bc)] = abc
                                nouvelle_loi.identifier_identites(nouvelle_loi.source[a],nouvelle_loi.source[abc])
                                nouvelle_loi.identifier_identites(nouvelle_loi.cible[bc],nouvelle_loi.cible[abc])
                                nouvelle_loi.identifier_identites(nouvelle_loi.cible[a],nouvelle_loi.source[bc])
                            else:
                                # on peut pas corriger la table
                                return None
                            break # on a corrigé la table, il faut recommencer le check de l'associativité
                else:
                    # aucune erreur d'associativité
                    break
            # ici l'associativité est vérifiée
            # on doit checker si la composition est respectée
            while True: # tant qu'on a un problème de composition
                for f,g in itertools.product(nouvelle_loi.fleches,repeat=2):
                    if nouvelle_loi.cible[f] == nouvelle_loi.source[g]:
                        # f et g sont censées être composables
                        if nouvelle_loi.table[(f,g)] == None:
                            # problème de composition
                            for k in range(nb_tentatives):
                                nouvelle_loi2 = tentative_complexification(nouvelle_loi,f,g,nb_tentatives-1) #on tente de complexifier
                                if nouvelle_loi2 != None:
                                    nouvelle_loi = nouvelle_loi2
                                    break
                            else:
                                # on a pas réussi à complexifier
                                return None
                            break
                else:
                    break

            return nouvelle_loi

        loi = LoiDeComposition(nb_fleches)
        if PROGRESS_BAR:
            print("Création d'une catégorie aléatoire à "+str(nb_fleches)+" flèches ("+str(nb_tentatives_complexification_loi_de_compo)+' complexifications de la loi de composition)')
            iterator = tqdm(range(nb_tentatives_complexification_loi_de_compo))
        else:
            iterator = range(nb_tentatives_complexification_loi_de_compo)
        for tentative in iterator:
            if random.random() < proba_identifier_identites:
                # ici on identifie deux identités plutôt que de définir une composée
                i = j = random.choice(list(loi.identites))
            else:
                # ici on définit une composée
                i,j = random.randint(0,nb_fleches-1),random.randint(0,nb_fleches-1)
            if nb_tentatives_correction_composition == None: 
                loi2 = tentative_complexification(loi,i,j,random.randint(3,6))
            elif type(nb_tentatives_correction_composition) is int:
                loi2 = tentative_complexification(loi,i,j,nb_tentatives_correction_composition)
            else:
                loi2 = tentative_complexification(loi,i,j,nb_tentatives_correction_composition())
            if loi2 != None:
                loi = loi2
        
        # on a une table de loi de composition aléatoire
        
        # on ajoute un objet par identité
        self = GC.__ior__(self,{loi.source[fleche] for fleche in loi.source} | {loi.cible[fleche] for fleche in loi.cible})    
            
        # on créé les flèches
        fleches = {i:MGC(loi.source[i],loi.cible[i]) for i in range(nb_fleches)}
                        

        # on définit les flèches élémentaires
        self._fleches_elem = set(range(nb_fleches))
        # on va retirer les flèches qui peuvent être trouvées par composition de flèches élémentaires
        while True:
            for k in self._fleches_elem:
                for i,j in itertools.product(self._fleches_elem-{k},repeat=2):
                    if loi.table[(i,j)] == k: # k est composite
                        self._fleches_elem -= {k}
                        break
                else:
                    continue
                break
            else:
                break
        self._fleches_elem = {fleches[i] for i in self._fleches_elem} # on récupère les flèches et non leur index
        
        # on fait les commutations
        for i,j in itertools.product(range(nb_fleches),repeat=2):
            if loi.table[(i,j)] != None:
                if loi.table[(i,j)] >= nb_fleches:
                    MGC.identifier_morphismes(fleches[j]@fleches[i],self.identite(loi.table[(i,j)]))
                elif fleches[loi.table[(i,j)]] != fleches[j]@fleches[i]:
                    MGC.identifier_morphismes(fleches[j]@fleches[i],fleches[loi.table[(i,j)]])
        # on ajoute les flèches
        self = GC.__ior__(self,self._fleches_elem)

    def __ior__(self, objets_ou_morphismes:Union[Set[Any],Set[MGC]]) -> "GCA":
        for objet_ou_morphisme in objets_ou_morphismes:
            if issubclass(type(objet_ou_morphisme),Morphisme):
                self._fleches_elem |= {objet_ou_morphisme}
        return GC.__ior__(self,objets_ou_morphismes)

GCA = GrapheCompositionAleatoire
             
def test_GrapheCompositionAleatoire():
    random.seed(1)
    for i in range(10):
        GCA = GrapheCompositionAleatoire()
        GCA.transformer_graphviz()
        GCA.loi_de_composition_to_csv('lois de composition/loi_de_compo.csv')
    return GCA

class CategoriePreordreAleatoire(CategoriePreordreInstanciable):
    """On s'inspire de la création d'une graphe aléatoire pour créer la catégorie préordre aléatoire.
    nb_objets est tiré au hasard uniformément dans l'intervale [1,20] par défaut.
    proportion_arcs est tirée au hasard uniformément dans l'intervale [0,1] par défaut."""
    def __init__(self, nb_objets:int = None, proportion_arcs:float = None, nom:str = "Categorie Préordre aléatoire"):
        if nb_objets == None:
            nb_objets = random.randint(1,20)
        if proportion_arcs == None:
            proportion_arcs = random.random()
        CategoriePreordreInstanciable.__init__(self,set(range(nb_objets)), nom)
        for source in range(nb_objets):
            for cible in range(nb_objets):
                if source != cible:
                    if random.random() < proportion_arcs:
                        self.ajouter_morphisme(source,cible)

def test_CategoriePreordreAleatoire():
    random.seed(1567365678)
    for i in range(10):
        CategoriePreordreAleatoire().transformer_graphviz()

class PetitMonoideAleatoire(MonoideGC):
    """Monoïde fini dont la table de loi de composition interne est choisie aléatoirement.
    Les éléments du monoïde seront des flèches nommées par des nombres allant de 1 
    jusqu'au nombre d'éléments moins un sauf l'identité qui représente l'élément neutre.
    La taille maximale de ce monoïde est 5, sinon le temps de calcul est trop grand.
    Pour un monoïde aléatoire plus grand, faire un produit de `PetitMonoideAleatoire`."""
    
    def __init__(self, nb_elements:Union[int,None] = None, nom:str = "Petit monoïde aléatoire"):
        """`nb_elements` est le nombre d'éléments du monoïde, si `nb_elements` = 1, alors il n'y a que l'identité dans le monoïde.
        `nb_elements` doit être inférieur à 5, sinon le temps de calcul est trop long.
        Pour un monoïde plus grand, utiliser `MonoideAleatoire`."""
        if nb_elements == None:
            nb_elements = random.randint(1,5)
        if nb_elements > 5:
            raise Exception("Le nombre d'elements d'un PetitMonoideAleatoire doit etre inferieur ou egal a 5.")
        elements = list(range(nb_elements)) #le 0 va représenter l'identité 
        associativite_verifiee = False
        while not associativite_verifiee:
            table = dict()
            associativite_verifiee = True
            for a,b in itertools.product(elements,repeat=2):
                if (a,b) not in table:
                    table[(a,b)] = random.choice(elements) if a*b != 0 else a+b
                    # si a ou b est 0 (le produit est 0) alors on renvoie l'autre element (a+b vaut l'autre element puisque 0 est neutre par l'addition)
            
                    # on force l'associativité si on peut
                    for c in elements:
                        if (table[(a,b)],c) in table and (b,c) in table:
                            if (a,table[(b,c)]) not in table:
                                table[(a,table[(b,c)])] = table[(table[(a,b)],c)]
                            elif table[(table[(a,b)],c)] != table[(a,table[(b,c)])]:
                                associativite_verifiee = False
                                table[(a,table[(b,c)])] = table[(table[(a,b)],c)]
                                break
                        if (c,table[(a,b)]) in table and (c,a) in table:
                            if (table[(c,a)],b) not in table:
                                table[(table[(c,a)],b)] = table[(c,table[(a,b)])]
                            elif table[(table[(c,a)],b)] != table[(c,table[(a,b)])]:
                                associativite_verifiee = False
                                break
                    else:
                        continue
                    break
            if not associativite_verifiee:
                continue
                     
            #on a une table aléatoire
            #maintenant on vérifie l'associativité
            associativite_verifiee = False 
            for a,b,c in itertools.product(elements,repeat=3):
                if table[(table[(a,b)],c)] != table[(a,table[(b,c)])]:
                    break
            else:
                associativite_verifiee = True
        # on a notre table de composition
        MonoideGC.__init__(self,nom)
        fleches = {i:ElementMonoideGC(str(i)) for i in range(1,nb_elements)}
        fleches[0] = self.identite()
        self |= {fleches[i] for i in range(1,nb_elements)}
        for a,b in itertools.product(elements,repeat=2):
            if fleches[a]@fleches[b] != fleches[table[(a,b)]]:
                MGC.identifier_morphismes(fleches[a]@fleches[b],fleches[table[(a,b)]])

def facteurs_premiers(n):
    while n > 1:
        for i in range(2, int(n**0.5)+1):
            if not n % i:
                n //= i
                yield i
                break
        else:
            yield n
            break

class MonoideAleatoire(CategorieProduit,Monoide):
    """Monoïde dont la table de loi de composition interne est tirée aléatoirement.
    Si le nombre d'éléments du monoïde est supérieur à 5, on créé un produit de `PetitMonoideAleatoire`.
    Si le (grand) nombre d'éléments est premier ou peu décomposable, on se réserve le droit de changer
    le nombre d'élément pour qu'il soit factorisable en facteurs inferieurs ou égaux à 5."""
    def __new__(cls, nb_elements:Union[int,None] = None, nom:str = "Monoïde aléatoire"):
        if PROGRESS_BAR:
            print("Création d'un monoïde à "+str(nb_elements)+" éléments")
        if nb_elements == None:
            nb_elements = random.randint(1,20)
        while max(facteurs_premiers(nb_elements)) > 5:
            nb_elements -= 1
            if PROGRESS_BAR:
                print("Simplification du nombre d'éléments à "+str(nb_elements))
        petits_monoides = tuple(PetitMonoideAleatoire(x) for x in tqdm(list(facteurs_premiers(nb_elements))))
        instance = CategorieProduit.__new__(cls,*petits_monoides)
        return instance
        
    def __init__(self, nb_elements:int = random.randint(1,20), nom:str = "Monoïde aléatoire"):
        CategorieProduit.__init__(self,*self)
        Monoide.__init__(self,nom)
    
    def __ior__(self, objets:set) -> 'MonoideAleatoire':
        if objets == {tuple(1 for i in range(len(self)))}:
            return CategorieLibre.__ior__(self,{tuple(1 for i in range(len(self)))})
        elif objets == {1}:
            return CategorieLibre.__ior__(self,{tuple(1 for i in range(len(self)))})
        raise Exception("Tentative d'ajout d'objet dans un monoide "+str(objets))

def test_MonoideGC():
    random.seed(22453)
    mon = PetitMonoideAleatoire(5)
    mon.transformer_graphviz()
    mon.loi_de_composition_to_csv()
    
    mon = MonoideAleatoire(72)
    mon.transformer_graphviz()
    mon.loi_de_composition_to_csv(destination="lois de composition/monoide.csv")


class MorphismeMonoideAleatoire(Foncteur):
    """Classe lente pour des grands monoïdes."""
    def __init__(self, monoide1:Monoide, monoide2:Monoide):
        obj1,obj2 = list(monoide1.objets)[0],list(monoide2.objets)[0]
        app_obj = {obj1:obj2}
        fleches = list(monoide2(monoide2.objets,monoide2.objets))
        while True:
            app_morph = {monoide1.identite(obj1):monoide2.identite(obj2)}
            for morph in monoide1[monoide1.objets,monoide1.objets]:
                if not morph.is_identite:
                    app_morph[morph] = random.choice(fleches)
            for f in monoide1[monoide1.objets,monoide1.objets]:
                for g in monoide1(monoide1.objets,monoide1.objets):
                    if app_morph[g@f] != app_morph[g]@app_morph[f]:
                        break
                else:
                    continue
                break
            else:
                break
            continue
        Foncteur.__init__(self,monoide1,monoide2,app_obj,app_morph)
        
def test_MorphismeMonoideAleatoire():
    random.seed(245)
    for i in range(5):
        mon1 = PetitMonoideAleatoire()
        mon2 = PetitMonoideAleatoire()
        mon1.transformer_graphviz()
        mon2.transformer_graphviz()
        morph = MorphismeMonoideAleatoire(mon1,mon2)
        morph.transformer_graphviz()

class FoncteurAleatoireCatCC(Foncteur):
    """Foncteur aléatoire entre deux catégories composantes connexes."""
    def __init__(self,categorie_indexante:CatCC, categorie_cible:CatCC, nom:Union[str,None] = None, diagramme_etendu:bool = True):
        '''L'option diagramme_etendu permet de spécifier si on préfére un diagramme étendu (où les objets sont mappés au maximum sur des objets différents à l'arrivée) ou pas.'''
        if diagramme_etendu:
            objets1,objets2 = list(categorie_indexante.objets),list(categorie_cible.objets)
            image = []
            while len(image)+len(objets2) < len(objets1):
                image += objets2
            image += random.sample(objets2,len(objets1)-len(image))
            random.shuffle(image)
            Foncteur.__init__(self,categorie_indexante,categorie_cible,{objets1[i]:image[i] for i in range(len(objets1))},dict(),nom)
        else:
            Foncteur.__init__(self,categorie_indexante,categorie_cible,{obj:random.choice(list(categorie_cible.objets)) for obj in categorie_indexante.objets},dict(),nom)
    
def test_FoncteurAleatoireCatCC():
    import random
    random.seed(25)
    
    c1,c2 = GrapheCompositionAleatoire(),GrapheCompositionAleatoire()
    c1.transformer_graphviz()
    c2.transformer_graphviz()
    cc1,cc2 = CatCC(c1),CatCC(c2)
    cc1.transformer_graphviz()
    cc2.transformer_graphviz()
    f = FoncteurAleatoireCatCC(cc1,cc2)
    f.as_diagram().transformer_graphviz()
    f.transformer_graphviz()
    f = FoncteurAleatoireCatCC(cc1,cc2)
    f.as_diagram().transformer_graphviz()
    f.transformer_graphviz()
    
class FoncteurAleatoireCatOrdreTotal(Foncteur):
    """Foncteur aléatoire entre deux catégories ordres totaux."""
    def __init__(self, categorie_indexante:CatOrdreTotal, categorie_cible:CatOrdreTotal, nom:Union[str,None] = None, diagramme_etendu:bool = True):
        '''L'option diagramme_etendu permet de spécifier si on préfére un diagramme étendu (où les objets sont mappés au maximum sur des objets différents à l'arrivée) ou pas.'''
        cat1,cat2 = categorie_indexante,categorie_cible
        objets1,objets2 = list(categorie_indexante.tri_topologique()),list(categorie_cible.tri_topologique())
        if diagramme_etendu:
            image = []
            while len(image)+len(objets2) < len(objets1):
                image += list(range(len(objets2)))
            image += random.sample(range(len(objets2)),len(objets1)-len(image))
            random.shuffle(image)
        else: #diagramme pas étendu
            image = [random.randint(0,len(objets2)-1) for i in range(len(objets1))]
        image.sort()
        app_obj = {objets1[i]:objets2[image[i]] for i in range(len(objets1))}
        app_morph = {m:next(cat2({app_obj[m.source]},{app_obj[m.cible]})) for m in cat1[cat1.objets,cat1.objets]}
        Foncteur.__init__(self,categorie_indexante,categorie_cible,app_obj,app_morph)
 
def test_FoncteurAleatoireCatOrdreTotal():
    import random
    random.seed(549875894875894)
    
    c1,c2 = GrapheCompositionAleatoire(20,150),GrapheCompositionAleatoire(20,150)
    c1.transformer_graphviz()
    c2.transformer_graphviz()
    c1_,c2_ = CatOrdreTotal(c1),CatOrdreTotal(c2)
    c1_.transformer_graphviz()
    c2_.transformer_graphviz()
    
    f = FoncteurAleatoireCatOrdreTotal(c1_,c2_)
    f.transformer_graphviz()
    f.as_diagram().transformer_graphviz()
 
class FoncteurAleatoireCatOrdreConnexe(Foncteur):
    """Foncteur aléatoire entre deux catégories ordre et connexes.
    L'option diagramme_etendu permet de spécifier si on préfére un diagramme étendu (où les objets sont mappés au maximum sur des objets différents à l'arrivée) ou pas."""
    def __init__(self, categorie_indexante:CatOrdre, categorie_cible:CatOrdre, nom:Union[str,None] = None, diagramme_etendu:bool = True):
        '''Les deux catégories doivent être des catégories connexes.'''
        assert(len(CatCC(categorie_indexante).objets) == 1) # on vérifie qu'il n'y a qu'une seule composante connexe
        assert(len(CatCC(categorie_cible).objets) == 1) # on vérifie qu'il n'y a qu'une seule composante connexe
        cat1,cat2 = categorie_indexante,categorie_cible
        if len(cat1.objets) == 1:
            Foncteur.__init__(self,cat1,cat2,{obj:random.choice(cat2.objets) for obj in cat1.objets},dict())
        else: # on a au moins une flèche dans la catégorie indexante
            app_obj, app_morph = dict(),dict()
            cat_ordre_total1,cat_ordre_total2 = CatOrdreTotal(cat1),CatOrdreTotal(cat2)
            fonct_ordre_total = FoncteurAleatoireCatOrdreTotal(cat_ordre_total1,cat_ordre_total2,diagramme_etendu=diagramme_etendu)
            tri = cat_ordre_total1.tri_topologique()[::-1] # on commence par les fils parce qu'on est sûr qu'ils ont un père (d'où le reverse du tri avec [::-1])
            for obj in tri[0]:
                app_obj[obj] = random.choice(list(fonct_ordre_total(tri[0])))
            # cas spécial où le dernier rang et l'avant dernier sont mappés vers la même classe à l'arrivée
            # on a alors une flèche de l'avant dernière classe vers la dernière, alors qu'il n'y en aurait aucune
            # entre les objets si on en choisis des différents
            if fonct_ordre_total(tri[0]) == fonct_ordre_total(tri[1]):
                objets = list(tri[0])
                for obj in objets[1:]:
                    app_obj[obj] = app_obj[objets[0]]
            # les objets du dernier rang sont mappés
            for i in range(1,len(tri)):
                for k in range(i-1,-1,-1):
                    for fleches_a_map in cat_ordre_total1[{tri[i]},{tri[k]}]:
                        for f in fleches_a_map:
                            sources_possibles = {app_obj[f.source]} if f.source in app_obj else fonct_ordre_total(tri[i])
                            cibles_possibles = {app_obj[f.cible]} if f.cible in app_obj else fonct_ordre_total(cat_ordre_total1.fonct_surjection(f.cible))
                            app_morph[f] = random.choice(list(cat2(sources_possibles,cibles_possibles)))
                            for obj in cat_ordre_total1.fonct_surjection(f.source):
                                app_obj[obj] = app_morph[f].source
            Foncteur.__init__(self,cat1,cat2,app_obj,app_morph,representant=nom)

                            
    
def test_FoncteurAleatoireCatOrdreConnexe():
    import random
    # random.seed(4)
    # random.seed(19098765456)
    
    for i in range(20):
        random.seed(i)
        
        c1,c2 = GrapheCompositionAleatoire(20,150),GrapheCompositionAleatoire(20)
        c1.transformer_graphviz()
        c2.transformer_graphviz()
        c1,c2 = CatCC(c1),CatCC(c2)
        c1_,c2_ = CatOrdre(c1.sous_cat_equiv(list(c1.objets)[0])),CatOrdre(c2.sous_cat_equiv(list(c2.objets)[0]))   
        c1_.transformer_graphviz()
        c2_.transformer_graphviz()
        
        f = FoncteurAleatoireCatOrdreConnexe(c1_,c2_)
        f.transformer_graphviz()
        f.as_diagram().transformer_graphviz()
    
class FoncteurAleatoireCatAcycliques(Foncteur):
    """Foncteur aléatoire entre deux catégories acycliques."""
    def __init__(self,categorie_indexante:CatAcyclique, categorie_cible:CatAcyclique, nom:Union[str,None] = None, diagramme_etendu:bool = True):
        ccc1,ccc2 = CatCC(categorie_indexante),CatCC(categorie_cible)
        fonct_ccc = FoncteurAleatoireCatCC(ccc1,ccc2,diagramme_etendu = diagramme_etendu)
        # ccc1.transformer_graphviz()
        # ccc2.transformer_graphviz()
        # fonct_ccc.transformer_graphviz()
        
        app_obj = dict()
        app_morph = dict()
        for cc in ccc1.objets:
            cat_connexe1,cat_connexe2 = ccc1.sous_cat_equiv(cc),ccc2.sous_cat_equiv(fonct_ccc(cc))
            
            cat_ordre_connexe1 = CatOrdre(cat_connexe1)
            cat_ordre_connexe2 = CatOrdre(cat_connexe2)
            fonct_ordre_connexe = FoncteurAleatoireCatOrdreConnexe(cat_ordre_connexe1,cat_ordre_connexe2,diagramme_etendu=diagramme_etendu)
            
            for obj_quotient in cat_ordre_connexe1.objets:
                for obj in obj_quotient:
                    app_obj[obj] = list(fonct_ordre_connexe(obj_quotient))[0]
            
            fonct_ordre_connexe.transformer_graphviz()
            fonct_ordre_connexe.as_diagram().transformer_graphviz()
            
            # on a la structure du foncteur, il faut choisir quel morphisme prendre à chaque fois maintenant
            # on fait un arbre de composition (un morphisme h a pour fils les morphismes f et g si g o f = h)
            # on doit affecter les pères avant les fils
            # arbre = defaultdict(set) # {morph : {morph à traiter avant}}
            # for morph1 in cat_ordre_connexe1(cat_ordre_connexe1.objets,cat_ordre_connexe1.objets):
                # for morph2 in cat_ordre_connexe1({morph1.cible},cat_ordre_connexe1.objets):
                    # if not morph1.is_identite and not morph2.is_identite:
                        # arbre[morph2@morph1] |= {(morph1,morph2)}
            
            #je suppose temporairement que les morphismes élémentaires ne sont pas décomposables en morphismes élémentaires
            fleches_elems = set(categorie_indexante[categorie_indexante.objets,categorie_indexante.objets])
            for morphismes in cat_ordre_connexe1[cat_ordre_connexe1.objets,cat_ordre_connexe1.objets]:
                for morph_elem in morphismes:
                    if morph_elem in fleches_elems and not morph_elem.is_identite:
                        app_morph[morph_elem] = random.choice(list(fonct_ordre_connexe(morphismes)))
            # print(arbre)
            # def choisir_image(morph):
                # for f in arbre[morph]:
                    # choisir_image(f)
                
            # for morph in cat_ordre_connexe1(cat_ordre_connexe1.objets,cat_ordre_connexe1.objets):
                # if len(arbre[morph]) == 0:
                    # for f in morph:
                        # app_morph[f] = random.choice(list(iter(fonct_ordre_connexe(morph))))
        Foncteur.__init__(self,categorie_indexante,categorie_cible,app_obj,app_morph)
    
def test_FoncteurAleatoireCatAcycliques():
    import random
    random.seed(4)
    
    c1,c2 = GrapheCompositionAleatoire(20,100),GrapheCompositionAleatoire(20,100)
    c1.transformer_graphviz()
    c2.transformer_graphviz()
    for i in range(10):
        f = FoncteurAleatoireCatAcycliques(c1,c2)
        f.transformer_graphviz()
    
class FoncteurAleatoire(Foncteur):
    """Foncteur aléatoire sur une catégorie.
    L'option diagramme_etendu permet de spécifier si on préfére un diagramme étendu (où les objets sont mappés au maximum sur des objets différents à l'arrivée) ou pas."""
    def __init__(self, categorie_indexante:Union[Categorie,None] = None, categorie_cible:Union[Categorie,None] = None, nom:Union[str,None] = None, diagramme_etendu:bool = True):
        if nom == None:
            nom = "Diagramme aléatoire sur "+str(categorie_cible)
        if categorie_indexante == None:
            categorie_indexante = GrapheCompositionAleatoire()
        if categorie_cible == None:
            categorie_cible = GrapheCompositionAleatoire()
        
        categorie_indexante.transformer_graphviz()
        categorie_cible.transformer_graphviz()
        
        app_obj = dict()
        app_morph = dict() 
        
        cat_acy1 = CatAcyclique(categorie_indexante)
        cat_acy2 = CatAcyclique(categorie_cible)
        
        fonct_cat_acy = FoncteurAleatoireCatAcycliques(cat_acy1,cat_acy2,diagramme_etendu=diagramme_etendu)
        fonct_cat_acy.transformer_graphviz()
        fonct_cat_acy.as_diagram().transformer_graphviz()
        
        for classe_equiv in cat_acy1.objets:
            if len(classe_equiv) == 1:
                app_obj[list(classe_equiv)[0]] = random.choice(list(fonct_cat_acy(classe_equiv)))
            else:
                raise Exception("Pas encore implémenté")
        for classe_equiv in cat_acy1[cat_acy1.objets,cat_acy1.objets]:
            morphs_elem_equiv = {morph for morph in classe_equiv.classe_morphismes if morph in categorie_indexante[{morph.source},{morph.cible}] and not morph.is_identite}
            if len(morphs_elem_equiv) == 1:
                morph = list(morphs_elem_equiv)[0]
                app_morph[morph] = random.choice([e for e in fonct_cat_acy(classe_equiv).classe_morphismes\
                if e.source == app_obj[morph.source] and  e.cible == app_obj[morph.cible]])
            else:
                raise Exception("Pas encore implémenté")
        print(app_morph)
        print(app_obj)
        Foncteur.__init__(self,categorie_indexante,categorie_cible,app_obj,app_morph)
        
def test_FoncteurAleatoire():
    import random
    random.seed(5)
    for i in range(10):
        f = FoncteurAleatoire()
        f.transformer_graphviz()
        f.as_diagram().transformer_graphviz()


if __name__ == '__main__':
    # test_GrapheCompositionAleatoire()
    # test_MonoideGC()
    # test_FoncteurAleatoireCatCC()
    # test_FoncteurAleatoireCatOrdreTotal()
    # test_FoncteurAleatoireCatOrdreConnexe()
    # test_FoncteurAleatoireCatAcycliques()
    # test_MorphismeMonoideAleatoire()
    test_FoncteurAleatoire()
    # test_CategoriePreordreAleatoire()