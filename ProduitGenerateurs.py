from typing import *

def produit_cartesien_generateurs(*iterables:Generator[any,None,None]) -> Generator[any,None,None]:
    """Fonction similaire à itertools.product sauf qu'elle ne consomme pas les iterables avant de créer le générateur."""
    if len(iterables) > 0:
        curseurs = [0 for i in range(len(iterables))] #curseurs qui bouclent en fonction de la taille de l'iterable
        tableau_backup = [[next(iterables[i],None)] for i in range(len(iterables))] #on a une liste d'éléments déjà calculés par iterable
        tableau_taille_iterables = [None for i in range(len(iterables))] #donne la taille de chacun des iterables
        if not any({tableau_backup[i][-1] == None for i in range(len(iterables))}):
            yield tuple(tableau_backup[i][curseurs[i]] for i in range(len(tableau_backup)))
            while tableau_taille_iterables[-1] == None:
                curseur_a_incrementer = 0
                while curseur_a_incrementer != None and curseur_a_incrementer < len(curseurs):
                    curseurs[curseur_a_incrementer] += 1
                    if tableau_taille_iterables[curseur_a_incrementer] == None:
                        suivant = next(iterables[curseur_a_incrementer],None)
                        if suivant == None:
                            tableau_taille_iterables[curseur_a_incrementer] = len(tableau_backup[curseur_a_incrementer])
                        else:
                            tableau_backup[curseur_a_incrementer] += [suivant]
                    if curseurs[curseur_a_incrementer] == tableau_taille_iterables[curseur_a_incrementer]:
                        #on doit boucler
                        curseurs[curseur_a_incrementer] = 0
                        curseur_a_incrementer += 1
                    else:
                        curseur_a_incrementer = None
                if any({curseurs[i] != 0 for i in range(len(curseurs))}):
                    yield tuple(tableau_backup[i][curseurs[i]] for i in range(len(tableau_backup)))
                else:
                    break
            
def test_mon_produit_cartesien():
    import itertools
    def generateur():
        for i in range(3):
            print("Generer "+str(i))
            yield i
    
    tab1 = [generateur() for i in range(4)]
    tab2 = [generateur() for i in range(4)]
    
    for prod in produit_cartesien_generateurs(*tab2):
        print(prod)
    
    for prod in itertools.product(*tab1):
        print(prod)
        
if __name__ == '__main__':
    test_mon_produit_cartesien()