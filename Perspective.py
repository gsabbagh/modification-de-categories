from Categorie import Categorie
from Foncteur import Foncteur
from Interaction import Interaction,CategorieInteractions
from CommaCategorie import ObjetCommaCategorie,CategorieFSous
from CategoriePreordreZigZag import CatPZigZag
import config

class Perspective(Interaction):
    """Un perspective est une interaction d'un diagramme constant D1 vers un diagramme D2 tel que :
        - ses composantes font partie de la même composante connexe de (D1(0)|D2)
        - la famille de composantes est maximale."""
        
    def verifier_struct(interaction:Interaction) -> tuple:
        '''Renvoie (True,None) si la structure de perspective est vérifiée par l'`interaction`, (False,Exception) sinon.'''
        D1,D2 = interaction.source,interaction.cible
        
        # contrainte 1) au moins une flèche part de chacun des objets de D1
        for e in D1.source.objets:
            for composante in interaction:
                if composante.e == e:
                    break
            else:
                return False,Exception("Incoherence ProtoClusterActif : l'objet "+str(e)+" de l'index de D1 n'a pas de de composante associée.")
        
        # contrainte 2) les composantes qui sortent d'un objet D1(i) mènent à des objets D2(j_i) reliés par un zig zag dans la comma catégorie (D1(i)|D2)
        for i in D1.source.objets:
            G_i = {compo for compo in interaction if compo.e == i}
            comma_categorie = CategorieFSous(D2,D1(i))
            cat_cc = CatPZigZag(comma_categorie)
            # on vérifie que tous les E(e) sont isomorphes dans la catégorie des composantes connexes de la catégorie sous D(d)
            temp = G_i.pop()
            composante_reference = ObjetCommaCategorie(0,temp.f,temp.d) #on met un 0 dans e puisque c'est un objet à gauche de la comma catégorie (D1(i)|D2)
            for composante in G_i:
                composante = ObjetCommaCategorie(0,composante.f,composante.d) #on met un 0 dans e puisque c'est un objet à gauche de la comma catégorie (D1(i)|D2)
                if not cat_cc.existe_morphisme(composante,composante_reference):
                    return False,Exception("Incoherence ProtoClusterActif : la composante"+str(composante)+" n'est pas dans la même composante connexe que "+str(composante_reference))
                    
        # contrainte 3) hog = g'
        for composante in interaction:
            for h in D2.source({composante.d},abs(D2.source)):
                if ObjetCommaCategorie(composante.e,D2(h)@composante.f,h.cible) not in interaction:
                    return False,Exception("Incoherence ProtoClusterActif : la composee de la composante "+str(composante)+" avec la fleche "+str(D2(h))+" de D2 n'est pas dans le cluster.")
        
        # contrainte 4) goh = g'
        for composante in interaction:
            for h in D1.source(abs(D1.source),{composante.e}):
                if ObjetCommaCategorie(h.source,composante.f@D1(h),composante.d) not in interaction:
                    return False,Exception("Incoherence ProtoClusterActif : la composee de la fleche "+str(D1(h))+" de D1 avec la composante "+str(composante)+" n'est pas dans le cluster")
        return True,None