from Categorie import Categorie, unique_generator, SousCategoriePleine, SousCategorie
from CategorieLibre import CategorieLibre
from Foncteur import Foncteur
from Morphisme import Morphisme
import itertools
import config

from typing import *

class ObjetQuotient(frozenset):
    """Objet d'une catégorie après le passage au quotient.
    Un `ObjetQuotient` est donc une classe d'équivalence d'objets de la catégorie initiale."""
    pass
    
class MorphismeQuotient(Morphisme):
    """Morphisme d'une catégorie après le passage au quotient.
    Un `MorphismeQuotient` est donc une classe d'équivalence de morphismes de la catégorie initiale.
    On peut itérer sur le morphisme pour obtenir tous les morphismes initiaux qui ont généré le `MorphismeQuotient`.
    On peut aussi accéder en lecture à classe_morphismes."""
    
    def __init__(self, source:ObjetQuotient, cible:ObjetQuotient, morphismes:Set[Morphisme], cat_quotient_associee:'CategorieQuotient', nom:str = None):
        Morphisme.__init__(self,source,cible,nom,any(map(lambda x:x.is_identite,morphismes)))
        self._classe_morphismes = frozenset(morphismes)
        self._cat_quotient_associee = cat_quotient_associee
        
    @property
    def classe_morphismes(self) -> frozenset:
        return self._classe_morphismes
        
    def __eq__(self, other:'MorphismeQuotient') -> bool:
        return issubclass(type(other),MorphismeQuotient) and self.classe_morphismes == other.classe_morphismes
       
    def __hash__(self) -> int:
        return hash((self.source,self.cible))
        
    def __iter__(self) -> Generator[Morphisme,None,None]:
        for m in self._classe_morphismes:
            yield m
            
    def __str__(self) -> str:
        return '{'+','.join(map(str,self._classe_morphismes))+'}'            
   
    def __repr__(self) -> str:
        return '{'+','.join(map(str,self._classe_morphismes))+'}'
        
    def __matmul__(self,other:'MorphismeQuotient') -> 'MorphismeQuotient':
        for m1 in other:
            for m2 in self:
                if m1.cible == m2.source:
                    return self._cat_quotient_associee._morph_vers_classe[m2@m1]
        if config.WARNING_CATEGORIE_QUOTIENT:
            raise Exception("Erreur lors du calcul de la composition "+str(self)+" o "+str(other)+", aucun morphisme composable")
        return MorphismeQuotient(other.source,self.cible,set(),self)
        
    def sous_cat_equiv(self) -> SousCategorie:
        """Renvoie la sous-catégorie qui est envoyée sur le morphisme par le foncteur de surjection.
        Pour la même fonction mais qui renvoie la sous-catégorie qui est envoyée sur un objet, voir `CategorieQuotient.sous_cat_equiv`"""
        return SousCategorie(self._cat_quotient_associee.cat_a_quotienter,self.source|self.cible,self._classe_morphismes)
        
class CategorieQuotient(CategorieLibre):
    """
    Définit ce qu'est une catégorie quotient. Cette classe est lourde et ne passe pas à l'échelle sur de grosses catégories.
    Elle donne accès au foncteur de sujection canonique qui est un foncteur qui nécessite de spécifier les applications d'objets et de
    morphismes complètement (et pas seulement des morphismes élémentaires).
    Si on n'a pas besoin de ce foncteur de sujection canonique, il est recommandé de faire sa propre classe qui définit
    les méthodes __getitem__ et __call__ pour qu'elles soient plus efficaces que par la catégorie quotient qui a une granularité plus fine.
    
    La principale utilité des catégories quotients est l'énumération des foncteurs entre deux catégories en factorisant
    par une catégorie quotient qui reflète une propriété que le foncteur devra vérifier.
    
    On peut définir la relation d'équivalence en appelant la méthode identifier_morphismes.
    Cette classe donne accès en lecture au foncteur de surjection canonique : fonct_surjection.
    Cette classe donne accès en lecture à la catégorie à quotienter : cat_a_quotienter.
    Pour voir un exemple, voir `CategorieAcyclique` et `CategorieComposantesConnexes`."""
        
        
    def __init__(self,categorie_a_quotienter:Categorie, nom:str = None):
        c = categorie_a_quotienter
        self._categorie_a_quotienter = c
        
        self._obj_vers_classe = {o:ObjetQuotient({o}) for o in c.objets} # associe à chaque objet son `ObjetQuotient` associé
        self._morph_vers_classe = {m:MorphismeQuotient(self._obj_vers_classe[m.source],\
        self._obj_vers_classe[m.cible],{m},self) for m in c(c.objets,c.objets)} # associe à chaque morphisme son `MorphismeQuotient` associé
        CategorieLibre.__init__(self,set(self._obj_vers_classe.values()),nom if nom != None else "Catégorie quotient de "+str(categorie_a_quotienter))
    
    def identifier_morphismes(self, morph1:Morphisme, morph2:Morphisme):
        """Identifie le `morph1` au `morph2`.
        La relation d'équivalence force la réflexivité, la symétrie et la transitivité.
        La relation d'équivalence doit aussi être compatible avec la composition.
        Ces contraintes sont automatiquement vérifiées par construction."""
        
        if self._morph_vers_classe[morph1] != self._morph_vers_classe[morph2]:
            if morph2.source not in self._obj_vers_classe[morph1.source]:
                # Il faut identifier les deux sources
                obj_a_update = ObjetQuotient(self._obj_vers_classe[morph1.source]|self._obj_vers_classe[morph2.source])
                self -= {self._obj_vers_classe[morph1.source],self._obj_vers_classe[morph2.source]}
                self |= {obj_a_update}
                for obj in obj_a_update:
                    self._obj_vers_classe[obj] = ObjetQuotient(obj_a_update)
                for obj in obj_a_update:
                    self.identifier_morphismes(self.cat_a_quotienter.identite(morph1.source),self.cat_a_quotienter.identite(obj))
                for morph in self._morph_vers_classe:
                    m = self._morph_vers_classe[morph]
                    if len(m.source-obj_a_update) == 0: #source inclus dans les obj_a_update
                        self._morph_vers_classe[morph] = MorphismeQuotient(obj_a_update,m.cible,m.classe_morphismes,self)
                for morph in self._morph_vers_classe:
                    m = self._morph_vers_classe[morph]  
                    if len(m.cible-obj_a_update) == 0: #cible inclus dans les obj_a_update
                        self._morph_vers_classe[morph] = MorphismeQuotient(m.source,obj_a_update,m.classe_morphismes,self)
            if morph2.cible not in self._obj_vers_classe[morph1.cible]:
                # Il faut identifier les deux cibles
                obj_a_update = ObjetQuotient(self._obj_vers_classe[morph1.cible]|self._obj_vers_classe[morph2.cible])
                self -= {self._obj_vers_classe[morph1.cible],self._obj_vers_classe[morph2.cible]}
                self |= {obj_a_update}
                for obj in obj_a_update:
                    self._obj_vers_classe[obj] = ObjetQuotient(obj_a_update)
                for obj in obj_a_update:
                    self.identifier_morphismes(self.cat_a_quotienter.identite(morph1.cible),self.cat_a_quotienter.identite(obj))
                for morph in self._morph_vers_classe:
                    m = self._morph_vers_classe[morph]
                    if len(m.source-obj_a_update) == 0: #source inclus dans les obj_a_update
                        self._morph_vers_classe[morph] = MorphismeQuotient(obj_a_update,m.cible,m.classe_morphismes,self)
                for morph in self._morph_vers_classe:
                    m = self._morph_vers_classe[morph]  
                    if len(m.cible-obj_a_update) == 0: #cible inclus dans les obj_a_update
                        self._morph_vers_classe[morph] = MorphismeQuotient(m.source,obj_a_update,m.classe_morphismes,self)
            if self._morph_vers_classe[morph1] != self._morph_vers_classe[morph2]:
                morph_a_update = self._morph_vers_classe[morph1].classe_morphismes|self._morph_vers_classe[morph2].classe_morphismes
                nouveau_morph = MorphismeQuotient(self._obj_vers_classe[morph1.source],self._obj_vers_classe[morph1.cible],morph_a_update,self)
                for morph in morph_a_update:
                    self._morph_vers_classe[morph] = nouveau_morph
            
            for m1 in self.cat_a_quotienter(self.cat_a_quotienter.objets,self.cat_a_quotienter.objets):
                for m2 in self.cat_a_quotienter({m1.cible},self.cat_a_quotienter.objets):
                    for m3 in self._morph_vers_classe[m1]:
                        for m4 in self._morph_vers_classe[m2]:
                            if m3.cible == m4.source:
                                if self._morph_vers_classe[m2@m1] != self._morph_vers_classe[m4@m3]:
                                    self.identifier_morphismes(m2@m1,m4@m3)
    
    def identifier_ensemble_morphismes(self, ens_morph:set):
        """Identifie l'ensemble des morphismes entre eux."""
        liste_morph = list(ens_morph)
        for m in liste_morph[1:]:
            self.identifier_morphismes(m,liste_morph[0])
        
    @property
    def fonct_surjection(self) -> Foncteur:
        '''Renvoie le foncteur de sujection canonique.'''
        return Foncteur(self.cat_a_quotienter,self,self._obj_vers_classe,self._morph_vers_classe)
        
    @property
    def cat_a_quotienter(self) -> Categorie:
        '''Renvoie la catégorie à quotienter.'''
        return self._categorie_a_quotienter
    
    @unique_generator
    def __getitem__(self,couple_sources_cibles:Tuple[Set[ObjetQuotient]]) -> Generator[MorphismeQuotient,None,None]:
        sources,cibles = couple_sources_cibles
        for source in sources:
            for cible in cibles:
                for fleche_elem in self.cat_a_quotienter[source,cible]:
                    yield self._morph_vers_classe[fleche_elem]
                            
    def identite(self, objet:ObjetQuotient) -> MorphismeQuotient:
        return self._morph_vers_classe[self.cat_a_quotienter.identite(next(iter(objet)))]
        
    def sous_cat_equiv(self, objet:ObjetQuotient) -> SousCategoriePleine:
        """Renvoie la sous-catégorie qui est envoyée sur un objet par le foncteur de surjection.
        Pour la même fonction mais qui renvoie la sous-catégorie qui est envoyée sur un morphisme, voir `MorphismeQuotient.sous_cat_equiv`"""
        return SousCategoriePleine(self.cat_a_quotienter,{obj for obj in self.cat_a_quotienter.objets if self._obj_vers_classe[obj] == objet})
                                                  

def test_CategorieQuotient():
    from CategorieAleatoire import GrapheCompositionAleatoire
    import random

    random.seed(13)
    c = CategorieQuotient(GrapheCompositionAleatoire())
    c.transformer_graphviz()
    for i in range(3):
        print("liste des morphismes : ",list(c(c.objets,c.objets)))
        morph1,morph2 = random.sample([m for m in c(c.objets,c.objets) if len(list(iter(m))) > 0],2)
        morph1 = random.choice(list(morph1.classe_morphismes))
        morph2 = random.choice(list(morph2.classe_morphismes))
        print("Identification de deux morphismes :")
        print(morph1)
        print(morph2)
        c.identifier_morphismes(morph1,morph2)
        c.transformer_graphviz()
        c.fonct_surjection.transformer_graphviz()
        print()

if __name__ == '__main__':
    test_CategorieQuotient()
                
                
                    
        