from Categorie import Categorie
from CategoriePreordre import CategoriePreordreEngendree,MorphismePreordre
from Morphisme import Morphisme
from typing import *


class CategoriePreordreZigZag(CategoriePreordreEngendree):
    """`CategoriePreordreZigZag` peut être abrégé en `CatPZigZag`
    `CategoriePreordreZigZag` est la catégorie préordre des zig-zag d'une catégorie.
    Soit C une catégorie et CPZZ la catégorie préordre des zig-zag de C.
    Les objets de CPZZ sont les mêmes que ceux de C.
    Soient deux objets a et b de CPZZ.
    Il y a une flèche entre a et b dans CPZZ s'il existe un zig-zag reliant a et b dans C.
    
    On peut aussi voir cette catégorie de la façon suivante :
    Les objets de CPZZ sont les mêmes que ceux de C.
    Deux objets sont isomorphes dans CPZZ s'ils sont dans la même composante connexe de C.
    """
    
    def __init__(self, categorie:Categorie, nom:str = None):
        CategoriePreordreEngendree.__init__(self,categorie,"Categorie préordre des zig-zag de "+str(categorie) if nom == None else nom)
    
    def existe_morphisme(self, source:Any, cible:Any) -> Union[Morphisme,None]:
        """Renvoie s'il existe un zig-zag entre source et cible."""
        objets_atteints = {source}
        objets_a_explorer = {source}
        while len(objets_a_explorer) > 0:
            explore = objets_a_explorer.pop()
            objets_a_explorer |= {morph.cible for morph in self._categorie_initiale({explore},abs(self)) if morph.cible not in objets_atteints}
            objets_a_explorer |= {morph.source for morph in self._categorie_initiale(abs(self),{explore}) if morph.source not in objets_atteints}
            objets_atteints |= objets_a_explorer
        return MorphismePreordre(source,cible) if cible in objets_atteints else None
        
CatPZigZag = CategoriePreordreZigZag

def test_CategoriePreordreZigZag():
    from GrapheDeComposition import GC,MGC
    from Diagramme import Triangle
    gc = GC()
    gc |= set("ABCDEFGHIJKL")
    f,g,h,i,j,k,l,m,n,o,p = [MGC('A','B','f'),MGC('B','C','g'),MGC('D','E','h'),MGC('E','F','i'),MGC('A','D','j'),
    MGC('B','E','k'),MGC('C','F','l'),MGC('G','H','m'),MGC('H','G','n'),MGC('I','J','o'),MGC('K','L','p')]
    gc |= {f,g,h,i,j,k,l,m,n,o,p}
    
    triangle = Triangle(gc, "GGG", [n@m@n@m,n@m,n@m@n@m])
    triangle.faire_commuter()
    triangle.transformer_graphviz()
    
    gc.transformer_graphviz()
    
    czz = CatPZigZag(gc)
    czz.transformer_graphviz()
    
if __name__ == '__main__':
    test_CategoriePreordreZigZag()