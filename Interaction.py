from Morphisme import Morphisme
from Diagramme import DiagrammeObjets
from Categorie import Categorie
from CommaCategorie import ObjetCommaCategorie
from Foncteur import Foncteur
import itertools
import config
if config.GRAPHVIZ_ENABLED:
    from graphviz import Digraph
from typing import *

class Interaction(Morphisme):
    """
    Soit D1 et D2 deux diagrammes (ou foncteurs) sur C.
    Une interaction est une partie de l'ensemble des objets de la comma-category (D1|D2).
    (cf. Modélisation catégorielle des organisations Vincent Robin et Guillaume Sabbagh Printemps 2021.)
    On peut itérer sur une intéraction, auquel cas on obtient toutes les flèches de l'intéraction les unes après les autres.
    """
    
    def __init__(self, D1:Foncteur, D2:Foncteur, ensemble_fleches:set, nom:str = None):
        """ensemble_fleches est un ensemble d'objets de la comma-category (D1|D2)"""
        self.__fleches = ensemble_fleches
        self.__C = D1.cible
        self.__D1 = D1
        self.__D2 = D2
        Morphisme.__init__(self,D1,D2,','.join(map(lambda x:str(x.f),self.__fleches)) if nom == None else nom,
        is_identite= (D1==D2) and (self.__fleches == {ObjetCommaCategorie(j,self.__C.identite(D1(j)),j) for j in D1.source.objets}))
        if config.TOUJOURS_VERIFIER_COHERENCE:
            self.verifier_coherence()
        
    def verifier_coherence(self):
        if self.__D1.cible != self.__D2.cible:
            raise Exception("Incoherence Interaction : les deux diagrammes d'une interaction n'ont pas la meme categorie cible.")
        for f in self.__fleches:
            if not issubclass(type(f),ObjetCommaCategorie):
                raise TypeError("Incoherence Interaction : les fleches de l'interaction ne sont pas des objets de comma categorie.")
            if f.e not in self.__D1.source.objets:    
                raise Exception("Incoherence Interaction : la fleche "+str(f)+" ne prend pas sa source dans la categorie indexante de D1.\n"+str(self.__D1.source.objets))
            if f.d not in self.__D2.source.objets:
                raise Exception("Incoherence Interaction : la fleche "+str(f)+" n'a pas pour cible un objet dans la categorie indexante de D2.\n"+str(self.__D2.source.objets))
    
    def __iter__(self):
        for f in self.__fleches:
            yield f
            
    def __len__(self) -> int:
        return len(self.__fleches)
    
    def __matmul__(self, other:'Interaction') -> 'Interaction':
        return Interaction(other.source,self.cible,{f2@f1 for f1 in other for f2 in self if f1.cible == f2.source})
        
    def __eq__(self, other:'Interaction') -> bool:
        if not issubclass(type(other),Interaction):
            return False
        return self.source == other.source and self.cible == other.cible and {f for f in self} == {f for f in other}
        
    def __hash__(self) -> int:
        return hash((self.source,self.cible,frozenset(self.__fleches)))
        
    def transformer_graphviz(self, destination:str = None, afficher_identites:bool = True, complet:bool = True, limite_fleches:int = 20):
        """Permet de visualiser l'interaction avec graphviz
        Composante rouge : diagramme 1
        Composante verte : diagramme 2
        Composante bleue: interaction
        On ne peut pas voir si un même objet est indexé par deux objets différents
        """
        CategorieInteractions.nb_viz += 1
        
        if destination == None:
            destination = "graphviz/"+type(self).__name__+str(CategorieInteractions.nb_viz)
        graph = Digraph('interaction')
        graph.attr(concentrate="true" if config.GRAPHVIZ_CONCENTRATE_GRAPHS else "false")
        graph.attr(label=str(self))
        
        noeuds = {o: [0,0,0] for o in self.__C.objets} # associe à chaque noeud une couleur
        for i in self.__D1.source.objets:
            noeuds[self.__D1(i)][0] = 200
        for j in self.__D2.source.objets:
            noeuds[self.__D2(j)][1] = 200
        
        ##on affiche les objets
        for o in noeuds:
            graph.node(str(o),color="#"+''.join(map(lambda x:(2-len(str(hex(x))[2:]))*"0"+str(hex(x))[2:],noeuds[o])))
        
        fleches_elem = set(self.__C[self.__C.objets,self.__C.objets])
        if complet:
            generateur = self.__C(self.__C.objets,self.__C.objets)
        else:
            generateur = fleches_elem|set(map(lambda x:x.f,self.__fleches))
        for fleche in generateur:
            couleur = [0,0,0]
            if fleche in set(map(self.__D1,self.__D1.source(self.__D1.source.objets,self.__D1.source.objets))):
                couleur[0] = 200
            if fleche in set(map(self.__D2,self.__D2.source(self.__D2.source.objets,self.__D2.source.objets))):
                couleur[1] = 200
            if fleche in set(map(lambda x:x.f,self.__fleches)):
                couleur[2] = 200
            ##on affiche les flèches
            if not fleche.is_identite or afficher_identites:
                graph.edge(str(fleche.source),str(fleche.cible),label=str(fleche), color="#"+''.join(map(lambda x:(2-len(str(hex(x))[2:]))*"0"+str(hex(x))[2:],couleur)))
                
        graph.render(destination)
        if config.CLEAN_GRAPHVIZ_MODEL:
            import os
            os.remove(destination)
        
        
class CategorieInteractions(Categorie):
    """
    Soit U une catégorie qui constitue notre univers.
    On s'intéresse aux diagrammes qui représentent les organisations de notre univers et
    aux interactions entre nos organisations.
    CategorieInteractions est la catégorie dont les objets sont des diagrammes de cible U et les morphismes sont des interactions.
    Par défaut, on considère pour chaque objet O le diagramme 1_O qui associe à un objet l'objet O.
    On retrouve ainsi l'univers dans la catégorie des interactions.
    """
    
    nb_viz = 0
    def __init__(self, univers:Categorie ,diagrammes:set = set(), nom:str = None):
        """
        CategorieInteractions est la catégorie dont les objets sont des diagrammes de cible `univers` et les morphismes sont des interactions.
        Par défaut, on considère pour chaque objet O de U le diagramme 1_O qui associe à un objet l'objet O.
        On retrouve ainsi l'`univers` dans la catégorie des interactions.
        `diagrammes` est un ensemble de diagrammes d'intérêts à ajouter à la catégorie.
        """
        Categorie.__init__(self,diagrammes,"Catégorie des diagrammes et interactions sur "+str(univers) if nom == None else nom)
        self.univers = univers
        self |= {DiagrammeObjets(univers,{obj}) for obj in univers.objets }
        if config.TOUJOURS_VERIFIER_COHERENCE:
            self.verifier_coherence()
            
    def verifier_coherence(self):
        Categorie.verifier_coherence(self)
        for diag in self.objets:
            if diag.cible != self.univers:
                raise Exception("Incoherence CategorieInteractions : le diagramme "+str(diag)+" n'a pas pour cible l'univers.")
    
    def identite(self, diag:Foncteur) -> Interaction:
        return Interaction(diag,diag,{ObjetCommaCategorie(obj,self.univers.identite(diag(obj)),obj) for obj in diag.source.objets})
    
    def __call__(self, sources:set, cibles:set) -> Generator[Interaction,None,None]:
        for source in sources:
            for cible in cibles:
                if source.cible != cible.cible:
                    raise Exception("Les deux diagrammes n'ont pas la même catégorie cible")
                fleches = {ObjetCommaCategorie(i,f,j) for i in source.source.objets for j in cible.source.objets for f in source.cible({source(i)},{cible(j)})}
                for k in range(len(fleches),0,-1): 
                    print(k,len(fleches))
                    for combi in itertools.combinations(fleches,k):
                        yield Interaction(source,cible,set(combi))
        
def test_CategorieInteractions():
    from GrapheDeComposition import GC,MGC
    from Diagramme import DiagrammeObjets
    
    gc = GC()
    gc |= {'A','B','C'}
    morphismes = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','C','h')]
    gc |= morphismes
    gc.transformer_graphviz()
    
    c_i = CategorieInteractions(gc)
    c_i.transformer_graphviz()
    
    c_i |= {DiagrammeObjets(gc,{'A','B'})}
    c_i.transformer_graphviz()
    
    for interact in c_i({DiagrammeObjets(gc,{'A','B'})},{DiagrammeObjets(gc,{'C'})}):
        interact.transformer_graphviz(afficher_identites=True)
    
if __name__ == '__main__':
    test_CategorieInteractions()