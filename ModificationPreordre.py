from CategoriePreordre import CategoriePreordre, CategoriePreordreInstanciable, MPO
from ChampActif import ChampActif,Cocone
from ChampPerceptif import ChampPerceptif,Cone
from Foncteur import Foncteur
from Diagramme import Diagramme
from GrapheDeComposition import GC,MGC
import copy
from typing import *

def modification(cat:CategoriePreordreInstanciable,
                 option_ajout:Tuple[Sequence[Any],Sequence[Tuple]] = (set(),set()),
                 option_elimination:Sequence[Any] = set(),
                 option_modif_agissante:Sequence[Cocone] = set(),
                 option_modif_classifiante:Sequence[Cone] = set()) -> Tuple[CategoriePreordreInstanciable,Foncteur]:
    nouvelle_cat = copy.copy(cat)
    
    fonct = Foncteur(cat,nouvelle_cat,{o:o for o in cat.objets},{m:m for m in cat[cat.objets,cat.objets]})
    
    nouveaux_obj, nouvelles_fleches = option_ajout
    nouvelle_cat |= set(nouveaux_obj)
    nouvelle_cat.ajouter_morphismes(*nouvelles_fleches)
    nouvelle_cat -= set(option_elimination)
    
    for cocone_objectif in option_modif_agissante:
        diag = cocone_objectif.source
        (fonct@diag).transformer_graphviz()
        c_a = ChampActif(fonct@diag)
        c_a.transformer_graphviz()
        colimites = [Cocone(obj) for obj in c_a.objets_colimites()]
        if cocone_objectif not in colimites:
            # Si le cocône n'était pas colimite, il faut rajouter les flèches nécessaires
            for cocone in c_a.objets:
                cocone = Cocone(cocone)
                if nouvelle_cat.existe_morphisme(cocone_objectif.nadir,cocone.nadir) == None:
                    nouvelle_cat.ajouter_morphisme(cocone_objectif.nadir,cocone.nadir)
        
    for cone_objectif in option_modif_classifiante:
        diag = cone_objectif.cible
        c_p = ChampPerceptif(fonct@diag)
        limites = [Cone(obj) for obj in c_p.objets_limites()]
        if cone_objectif not in limites:
            # Si le cône n'était pas limite, il faut rajouter les flèches nécessaires
            for cone in c_p.objets:
                cone = Cone(cone)
                if nouvelle_cat.existe_morphisme(cone.apex,cone_objectif.apex) == None:
                    nouvelle_cat.ajouter_morphisme(cone.apex,cone_objectif.apex)
                    
    return nouvelle_cat, fonct
        
        

def test_modification():
    cat = CategoriePreordreInstanciable(set("ABCDEFGH"))
    cat.ajouter_morphismes("AC","BC","DF","EF","GA","GB","GD","GE","HA","HB","HD","HE")
    cat.transformer_graphviz()
    
    J = GC(range(3))
    a,b = [MGC(0,2),MGC(1,2)]
    J |= {a,b}
    
    diag1 = Diagramme(J,cat,{0:"A",1:"B",2:"C"},{a:MPO("A","C"),b:MPO("B","C")})
    diag1.transformer_graphviz()
    
    diag2 = Diagramme(J,cat,{0:"D",1:"E",2:"F"},{a:MPO("D","F"),b:MPO("E","F")})
    diag2.transformer_graphviz()
    
    c_p1 = ChampPerceptif(diag1)
    c_p1.transformer_graphviz()
    
    c_p2 = ChampPerceptif(diag2)
    c_p2.transformer_graphviz()

    cone1 = Cone([cone for cone in c_p1.objets if Cone(cone).apex == "G"][0])
    cone2 = Cone([cone for cone in c_p2.objets if Cone(cone).apex == "H"][0])
    
    cone1.transformer_graphviz()
    cone2.transformer_graphviz()
    
    nouvelle_cat, fonct = modification(cat,option_modif_classifiante = {cone1,cone2})
    nouvelle_cat.transformer_graphviz()
    fonct.transformer_graphviz()
    
    c_p1 = ChampPerceptif(fonct@diag1)
    c_p1.transformer_graphviz()
    
    c_p2 = ChampPerceptif(fonct@diag2)
    c_p2.transformer_graphviz()
    
def test_modification2():
    from CategorieAleatoire import CategoriePreordreAleatoire
    import random
    from Diagramme import Carre
    
    random.seed(1256789)
    cat = CategoriePreordreAleatoire(10,0.1)
    cat.transformer_graphviz()
    
    J = GC(range(4))
    a,b = [MGC(0,1),MGC(2,3)]
    J |= {a,b}
    
    diag = Diagramme(J,cat,{0:8,1:1,2:6,3:3},{a:MPO(8,1),b:MPO(6,3)})
    diag.transformer_graphviz()
    
    c_p = ChampPerceptif(diag)
    c_p.transformer_graphviz()
    c_a = ChampActif(diag)
    c_a.transformer_graphviz()
    
    cocone = Cocone([cocone for cocone in c_a.objets if cocone.d == 2][0])
    cocone.transformer_graphviz()
    
    nouvelle_cat, fonct = modification(cat,option_modif_agissante = {cocone})
    nouvelle_cat.transformer_graphviz()
    fonct.transformer_graphviz()
    
    c_a = ChampActif(fonct@diag)
    c_a.transformer_graphviz()
    
    
    
if __name__ == '__main__':
    test_modification()