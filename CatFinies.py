from Categorie import Categorie
from Foncteur import Foncteur
from EnsFinis import EnsFinis,Application
import itertools
from typing import *
import config

class CatFinies(Categorie):
    """Catégorie des catégories finies, les morphismes sont des foncteurs.
    Cette catégorie est infinie, par défaut il n'y a aucune catégorie dedans, il faut rajouter les catégories d'intérêt."""
    
    def identite(self, objet:Categorie) -> Foncteur:
        return Foncteur(objet,objet, {o:o for o in objet.objets}, {m:m for m in objet[objet.objets,objet.objets]})
    
    def __call__(self, sources:set, cibles:set) -> Generator[Foncteur,None,None]:
        for cat_source in sources:
            for cat_cible in cibles:
                # cat_source et cat_cible sont des catégories, on cherche tous les foncteurs de cat_source vers cat_cible
                ens_objets = EnsFinis({cat_source.objets,cat_cible.objets})
                for app_obj in ens_objets({cat_source.objets},{cat_cible.objets}):
                    app_fleche_foncteur = dict() # à chaque couple d'objets on associe un ensemble d'applications
                    for c,d in itertools.product(cat_source.objets, repeat=2):
                        ens_fleches = EnsFinis({frozenset(cat_source[{c},{d}]),frozenset(cat_cible({app_obj(c)},{app_obj(d)}))})
                        app_fleches_c_vers_d = set(ens_fleches({frozenset(cat_source[{c},{d}])},{frozenset(cat_cible({app_obj(c)},{app_obj(d)}))}))
                        app_fleche_foncteur[(c,d)] = app_fleches_c_vers_d
                        if len(app_fleches_c_vers_d) == 0:
                            break
                    if len(app_fleche_foncteur[(c,d)]) == 0:
                        continue
                    for applications_fleches in itertools.product(*app_fleche_foncteur.values()):
                        app_fleches = dict()
                        for app in applications_fleches:
                            app_fleches.update(app.as_dict())
                        backup = config.TOUJOURS_VERIFIER_COHERENCE
                        config.TOUJOURS_VERIFIER_COHERENCE = False
                        fonct = Foncteur(cat_source,cat_cible,app_obj.as_dict(),app_fleches)
                        for f in cat_source(abs(cat_source),abs(cat_source)):
                            for g in cat_source(abs(cat_source),{f.source}):
                                if fonct(f@g) != fonct(f)@fonct(g):
                                    config.TOUJOURS_VERIFIER_COHERENCE = backup
                                    break
                            else:
                                continue
                            break
                        else:
                            config.TOUJOURS_VERIFIER_COHERENCE = backup
                            yield Foncteur(cat_source,cat_cible,app_obj.as_dict(),app_fleches)
                    
def test_CatFinies():
    from GrapheDeComposition import GC,MGC
    
    gc = GC()
    gc |= set("ABC")
    f,g = [MGC('A','B','f'),MGC('B','C','g')]
    gc |= {f,g}

    gc2 = GC()
    gc2 |= set("DEF")
    f,g = [MGC('D','E','a'),MGC('E','F','b')]
    gc2 |= {f,g}

    cat = CatFinies({gc,gc2})
    cat.transformer_graphviz()
    for fonct in cat({gc},{gc2}):
        fonct.transformer_graphviz()
        fonct.as_diagram().transformer_graphviz()
        
    print(next(cat.isomorphismes(gc,gc2)))
    list(cat.isomorphismes(gc,gc2))[0][0].transformer_graphviz()
    cat.identite(gc).transformer_graphviz()
    
if __name__ == '__main__':
    test_CatFinies()