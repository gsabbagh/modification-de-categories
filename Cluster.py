from Categorie import Categorie
from Interaction import Interaction,CategorieInteractions
from Foncteur import Foncteur
from CommaCategorie import ObjetCommaCategorie,CategorieFSous
from CategoriePreordreZigZag import CatPZigZag
from Interaction import CategorieInteractions
import config
from copy import copy
from typing import *

class ProtoClusterActif(Interaction):
    """
    Un proto-cluster actif ou simplement protocluster est une interaction G entre deux diagrammes D1:I->C et D2:J->C tel que :
    1) Pour tout objet i de I, il existe un objet j de J et une flèche f dans C tel que <i,f,j> \in G
    2) soit G(i) l'ensemble des <i,f,_>, alors G(i) est inclus dans une composante connexe de la comma-catégorie (D1(i)|D2)
    3) Pour toute flèche h:j->j' dans J et <i,g,j> \in G(i), on a <i,h@g,j'> \in G(i)
    4) Pour toute flèche h:i'->i dans I et <i,g,j> \in G(i), on a <i',g@h,j> \in G(i')
    """
    
    def verifier_struct(interaction:Interaction) -> tuple:
        '''Renvoie (True,None) si la structure de cluster est vérifiée par l'`interaction`, (False,Exception) sinon.'''
        D1,D2 = interaction.source,interaction.cible
        
        # contrainte 1) au moins une flèche part de chacun des objets de D1
        for e in D1.source.objets:
            for composante in interaction:
                if composante.e == e:
                    break
            else:
                return False,Exception("Incoherence ProtoClusterActif : l'objet "+str(e)+" de l'index de D1 n'a pas de de composante associée.")
        
        # contrainte 2) les composantes qui sortent d'un objet D1(i) mènent à des objets D2(j_i) reliés par un zig zag dans la comma catégorie (D1(i)|D2)
        for i in D1.source.objets:
            G_i = {compo for compo in interaction if compo.e == i}
            comma_categorie = CategorieFSous(D2,D1(i))
            cat_cc = CatPZigZag(comma_categorie)
            # on vérifie que tous les E(e) sont isomorphes dans la catégorie des composantes connexes de la catégorie sous D(d)
            temp = G_i.pop()
            composante_reference = ObjetCommaCategorie(0,temp.f,temp.d) #on met un 0 dans e puisque c'est un objet à gauche de la comma catégorie (D1(i)|D2)
            for composante in G_i:
                composante = ObjetCommaCategorie(0,composante.f,composante.d) #on met un 0 dans e puisque c'est un objet à gauche de la comma catégorie (D1(i)|D2)
                if not cat_cc.existe_morphisme(composante,composante_reference):
                    return False,Exception("Incoherence ProtoClusterActif : la composante"+str(composante)+" n'est pas dans la même composante connexe que "+str(composante_reference))
                    
        # contrainte 3) hog = g'
        for composante in interaction:
            for h in D2.source({composante.d},abs(D2.source)):
                if ObjetCommaCategorie(composante.e,D2(h)@composante.f,h.cible) not in interaction:
                    return False,Exception("Incoherence ProtoClusterActif : la composee de la composante "+str(composante)+" avec la fleche "+str(D2(h))+" de D2 n'est pas dans le cluster.")
        
        # contrainte 4) goh = g'
        for composante in interaction:
            for h in D1.source(abs(D1.source),{composante.e}):
                if ObjetCommaCategorie(h.source,composante.f@D1(h),composante.d) not in interaction:
                    return False,Exception("Incoherence ProtoClusterActif : la composee de la fleche "+str(D1(h))+" de D1 avec la composante "+str(composante)+" n'est pas dans le cluster")
        return True,None

    def __init__(self, D1:Foncteur, D2:Foncteur, composantes:set, nom:str = None):
        """composantes est un ensemble d'objets de la comma-catégorie (D1|D2)"""
        Interaction.__init__(self,D1, D2, composantes,nom)
        if config.TOUJOURS_VERIFIER_COHERENCE:
            ProtoClusterActif.verifier_coherence(self)

    def verifier_coherence(self):
        Interaction.verifier_coherence(self)
        coherence,exception_levee = ProtoClusterActif.verifier_struct(self)
        if not coherence:
            raise exception_levee
    
    def __lt__(self, other:'ProtoClusterActif') -> bool:
        '''G1 < G2 si G1 est inclus strictement dans G2'''
        for composante in self:
            if composante not in other:
                return False
        return self != other
        
    def __gt__(self, other:'ProtoClusterActif') -> bool:
        '''G1 > G2 si G2 est inclus strictement dans G1'''
        for composante in other:
            if composante not in self:
                return False
        return self != other
        
    def __lte__(self, other:'ProtoClusterActif') -> bool:
        '''G1 <= G2 si G1 est inclus dans G2'''
        for composante in self:
            if composante not in other:
                return False
        return True
        
    def __gte__(self, other:'ProtoClusterActif') -> bool:
        '''G1 >= G2 si G2 est inclus dans G1'''
        for composante in other:
            if composante not in self:
                return False
        return True

class ClusterActif(ProtoClusterActif):
    """Un cluster est un proto-cluster maximal."""
    def __init__(self,proto_cluster:ProtoClusterActif):
        self.__categorie_proto_cluster = None # sert à calculer tous les proto-clusters pour tester la maximalité
        ProtoClusterActif.__init__(self,proto_cluster.source,proto_cluster.cible,{compo for compo in proto_cluster},str(proto_cluster))
        if config.TOUJOURS_VERIFIER_COHERENCE:
            self.verifier_coherence()
    
    def __get_categorie_proto_cluster(self):
        if self.__categorie_proto_cluster == None:
            backup = config.TOUJOURS_VERIFIER_COHERENCE
            config.TOUJOURS_VERIFIER_COHERENCE = 0 # on empêche la vérification de la cohérence de CategorieClusters qui ferait une boucle infinie
            self.__categorie_proto_cluster = CategorieProtoClusters(self.source.cible,{self.source,self.cible},"Catégorie cluster de "+str(self))
            config.TOUJOURS_VERIFIER_COHERENCE = backup
        return self.__categorie_proto_cluster
    
    def verifier_coherence(self):
        ProtoClusterActif.verifier_coherence(self)
        proto_clusters = self.__get_categorie_proto_cluster()({self.source},{self.cible})
        for proto_cluster in proto_clusters:
            if proto_cluster > self:
                raise Exception("Incoherence ClusterActif : cluster pas maximal "+str(proto_cluster))
    
    def __matmul__(self,other:'ProtoClusterActif') -> 'ProtoClusterActif':
        """La composition de deux clusters G1 et G2 est le cluster engendré par la composition de G1 et G2 vus comme des interactions."""
        composition_interaction = Interaction.__matmul__(self,other) # on calcule la composition des interactions
        #on doit trouver le cluster engendré par la composition_interaction
        cat_proto = self.__get_categorie_proto_cluster()
        cat_proto |= {other.source}
        clusters_candidats = cat_proto({other.source},{self.cible})
        clusters_finaux = set()
        for cluster in clusters_candidats:
            for obj_comma in composition_interaction:
                if obj_comma not in cluster:
                    break
            else:
                for cluster_final in clusters_finaux:
                    assert(cluster_final != cluster)
                    if cluster_final < cluster:
                        clusters_finaux |= {cluster}
                        clusters_finaux -= {cluster_final}
                        break
                    if cluster_final > cluster:
                        break
                else: # ici cluster est incomparable avec tous les clusters finaux
                    clusters_finaux |= {cluster}
        if len(clusters_finaux) > 1:
            raise Exception("Plus d'une composition possible "+str(self)+"o"+str(other)+"\n"+str(clusters_finaux))
        if len(clusters_finaux) == 0:
            raise Exception("Aucune composition trouvee pour "+str(self)+"o"+str(other)+"\n"+str(clusters_candidats))
        return ProtoClusterActif(other.source,self.cible,{composante for composante in clusters_finaux.pop()},str(self)+'o'+str(other))

class CategorieProtoClusters(CategorieInteractions):
    """
    CategorieProtoClusters est une catégorie d'organisations où les intéractions sont des proto-clusters. 
    """
    
    def __init__(self, univers:Categorie ,diagrammes:set = set(), nom:str = None):
        """
        CategorieProtoClusters est la catégorie dont les objets sont des diagrammes de cible `univers` et les morphismes sont des proto-clusters.
        Par défaut, on considère pour chaque objet O de l'`univers` le diagramme 1_O qui associe à un objet l'objet O.
        On retrouve ainsi l'`univers` dans la catégorie des proto-clusters.
        `diagrammes` est un ensemble de diagrammes d'intérêts à ajouter à la catégorie.
        """
        CategorieInteractions.__init__(self,univers,diagrammes,"Catégorie des diagrammes et clusters sur "+str(univers) if nom == None else nom)

    def identite(self,objet:Foncteur) -> ProtoClusterActif:
        return ProtoClusterActif(objet,objet,{ObjetCommaCategorie(obj_indexant,objet.cible.identite(objet(obj_indexant)),obj_indexant) for obj_indexant in objet.source.objets})

    def __call__(self, sources:set, cibles:set) -> set:
        result = set()
        for source in sources:
            for cible in cibles:
                result |= {interact for interact in CategorieInteractions.__call__(self,{source},{cible}) if ProtoClusterActif.verifier_struct(interact)[0]}
        return result

class CategorieClusters(CategorieInteractions):
    """
    CategorieClusters est une catégorie d'organisations où les intéractions sont des clusters. 
    """
    
    def __init__(self, univers:Categorie ,diagrammes:set = set(), nom:str = None):
        """
        CategorieClusters est la catégorie dont les objets sont des diagrammes de cible `univers` et les morphismes sont des clusters.
        Par défaut, on considère pour chaque objet O de l'`univers` le diagramme 1_O qui associe à un objet l'objet O.
        On retrouve ainsi l'`univers` dans la catégorie des clusters.
        `diagrammes` est un ensemble de diagrammes d'intérêts à ajouter à la catégorie.
        """
        CategorieInteractions.__init__(self,univers,diagrammes,"Catégorie des diagrammes et clusters sur "+str(univers) if nom == None else nom)

    def identite(self,objet:Foncteur) -> ClusterActif:
        clusters_candidats = self({objet},{objet})
        clusters_finaux = set()
        for cluster in clusters_candidats:
            for obj_indexant in objet.source.objets:
                if ObjetCommaCategorie(obj_indexant,objet.cible.identite(objet(obj_indexant)),obj_indexant) not in cluster:
                    break
            else:
                clusters_finaux |= {cluster} # on a tous les clusters qui contiennent toutes les identités dans clusters_finaux
        if len(clusters_finaux) > 1:
            raise Exception("Plus d'une identite possible pour l'objet "+str(objet)+"\n"+str(clusters_finaux))
        if len(clusters_finaux) == 0:
            raise Exception("Aucune identite trouvee pour l'objet "+str(objet)+"\n"+str(clusters_candidats))
        return clusters_finaux.pop()
        
        
    """
    def __call__(self, sources:set, cibles:set) -> Generator[ClusterActif,None,None]:
        for source in sources:
            for cible in cibles:
                print(source,cible)
                interactions = CategorieInteractions.__call__(self,{source},{cible})
                print(source,cible)
                proto_cluster_maximaux = set()
                while len(interactions) > 0:
                    interact = interactions.pop()
                    if not ProtoClusterActif.verifier_struct(interact)[0]:
                        continue
                    else:
                        proto_cluster_candidat = ProtoClusterActif(source,cible,{composante for composante in interact})
                        for proto_cluster_maximal in proto_cluster_maximaux:
                            assert(proto_cluster_maximal != proto_cluster_candidat)
                            if proto_cluster_maximal < proto_cluster_candidat:
                                proto_cluster_maximaux -= {proto_cluster_maximal}
                                proto_cluster_maximaux |= {proto_cluster_candidat}
                                break
                            if proto_cluster_maximal > proto_cluster_candidat:
                                break
                        else:
                            # ici on n'a jamais break, ce qui signifie que le proto_cluster_candidat était incomparable avec tous les proto_cluster_maximaux
                            proto_cluster_maximaux |= {proto_cluster_candidat}
                for proto_cluster in proto_cluster_maximaux:
                    yield ClusterActif(proto_cluster)"""
                    
    def __call__(self, sources:set, cibles:set) -> Generator[ClusterActif,None,None]:
        for source in sources:
            for cible in cibles:
                print(source,cible)
                interactions = CategorieInteractions.__call__(self,{source},{cible})
                print(source,cible)
                borne_min = None
                for interact in interactions:
                    if not ProtoClusterActif.verifier_struct(interact)[0]:
                        continue
                    else:
                        print("ha")
                        if borne_min == None or len(interact) == borne_min:
                            yield ClusterActif(ProtoClusterActif(source,cible,{composante for composante in interact}))
                            borne_min = len(interact)
                        else:
                            print("zut")
                            break
   


    
            
def test_CategorieClusters():
    from GrapheDeComposition import GC,MGC
    from Diagramme import DiagrammeObjets,Fleche
    
    gc = GC()
    gc |= {'A','B','C'}
    f,g,h = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','C','h')]
    gc |= {f,g,h}
    gc.transformer_graphviz()
    ProtoClusterActif(Fleche(gc,f),Fleche(gc,f),{ObjetCommaCategorie(1,f,2),ObjetCommaCategorie(1,gc.identite('A'),1),
    ObjetCommaCategorie(2,gc.identite('B'),2)})
    
    c_i = CategorieClusters(gc)
    c_i.transformer_graphviz(afficher_identites=True)
    
    
    c_i |= {DiagrammeObjets(gc,{'A','B'})}
    c_i.transformer_graphviz(afficher_identites=True)
    
    c_i |= {Fleche(gc,f)}
    c_i.transformer_graphviz(afficher_identites=True)
    
    # c_i.identite(Fleche(gc,f)).transformer_graphviz(afficher_identites=True)
    # c_i.identite(DiagrammeObjets(gc,{'A','B'})).transformer_graphviz(afficher_identites=True)
    
    a = set(c_i({Fleche(gc,f)},{DiagrammeObjets(gc,{'A','B'})})).pop()
    a.transformer_graphviz()
    b = set(c_i({DiagrammeObjets(gc,{'A','B'})},{Fleche(gc,f)})).pop()
    b.transformer_graphviz()
    
    (a@b).transformer_graphviz()
    for cluster in c_i({DiagrammeObjets(gc,{'A','B'})},{DiagrammeObjets(gc,{'A','B'})}):
        cluster.nom="AB vers AB"
        cluster.transformer_graphviz(afficher_identites=True)
    (b@a).transformer_graphviz()
    for cluster in c_i({Fleche(gc,f)},{Fleche(gc,f)}):
        cluster.nom="f vers f"
        cluster.transformer_graphviz(afficher_identites=True)

if __name__ == '__main__':
    test_CategorieClusters()