from CommaCategorie import CommaCategorie, ObjetCommaCategorie
from TransformationNaturelle import CatTransfoNat,TransfoNat
from Foncteur import Foncteur
from Diagramme import DiagrammeConstant,DiagrammeObjets
# from Cluster import ClusterActif,ProtoClusterActif
from typing import *
import config
if config.GRAPHVIZ_ENABLED:
    from graphviz import Digraph
import itertools

class Cone(TransfoNat):
    """Un cône sur D d'apex A est une transformation naturelle du foncteur constant Δ_A vers D."""
    def __init__(self, objet_champ_perceptif:ObjetCommaCategorie):
        self.__apex = objet_champ_perceptif.e # attribut read-only
        f = objet_champ_perceptif.f
        TransfoNat.__init__(self,f.source,f.cible,f._composantes,f.nom)
    
    @property
    def apex(self):
        return self.__apex
        
    def as_ObjetCommaCategorie(self) -> ObjetCommaCategorie:
        return ObjetCommaCategorie(self.apex,self,0)

class ChampPerceptif(CommaCategorie):
    """Le champ perceptif d'un diagramme(ou foncteur) D:P->Q est défini comme étant la comma-catégorie (Δ|D)
    (cf. https://en.wikipedia.org/wiki/Cone_(category_theory)#Equivalent_formulations)
    Un objet (A,C,Id) du champ perceptif nous donne un cône C d'apex A sur le diagramme D.
    Δ:Q -> (P->Q) est un foncteur dans une catégorie infinie, on ne s'intéresse qu'aux diagrammes constants Δq : P->Q, _|->q,_|->1q avec q \in Q
    et au diagramme D.
    En ne s'intéressant qu'à ces diagrammes, on obtient quand même tous les cônes sur D.
    """
    nb_viz = 0
    
    def __init__(self, diagramme:Foncteur, nom:str = None):
        P = diagramme.source
        Q = diagramme.cible
        self.categorie_diagrammes = CatTransfoNat({DiagrammeConstant(P,Q,o) for o in Q.objets},
        "Categorie de diagrammes du champ perceptif de "+str(diagramme))
        self.categorie_diagrammes |= {diagramme}
        
        self.foncteur_diagonal = Foncteur(Q,self.categorie_diagrammes,{o:DiagrammeConstant(P,Q,o) for o in Q.objets},
        {f: TransfoNat(DiagrammeConstant(P,Q,f.source),DiagrammeConstant(P,Q,f.cible),{p:f for p in P.objets}) for f in Q(abs(Q),abs(Q))})
        ## on associe à chaque objet son diagramme constant et à chaque flèche F:a->b la transformation naturelle qui va de Δa à Δb
        
        self.foncteur_vers_D = DiagrammeObjets(self.categorie_diagrammes,{diagramme})
        
        CommaCategorie.__init__(self,self.foncteur_diagonal,self.foncteur_vers_D, "Champ perceptif de "+str(diagramme) if nom == None else nom)
        
    def objets_cones(self,apices:set) -> set:
        """`apices` est un ensemble d'objets de Q
        Cette fonction renvoie l'ensemble tous les cônes d'apex un objets d'`apices`.
        Un cône est une transformation naturelle du diagramme constant sur l'apex vers D.
        Cette fonction renvoie les objets de la comma-catégorie qui contiennent ces cônes."""
        return {obj for obj in self.objets for apex in apices if obj.e == apex}
        
    def objets_limites(self) -> set:
        """Renvoie un ensemble de cônes limites.
        Les cônes limites sont des objets finaux dans le champ perceptif.
        Cette fonction renvoie les objets de la comma-catégorie qui contiennent ces cônes."""
        if len(self.objets) == 0:
            return set()
        cones = list(self.objets)
        cone_courant = cones[0]
        cones_visites = {cone_courant}
        cones_restants = self.objets-cones_visites
        fleche_accessible = next(self[{cone_courant},cones_restants],None)
        if fleche_accessible == None:
            fleche_accessible = next(self({cone_courant},cones_restants),None)
        while fleche_accessible != None:
            cone_courant = fleche_accessible.cible
            cones_visites |= {cone_courant}
            cones_restants = self.objets-cones_visites
            fleche_accessible = next(self[{cone_courant},cones_restants],None)
            if fleche_accessible == None:
                fleche_accessible = next(self({cone_courant},cones_restants),None)
        # ici cone_courant est un objet final de la catégorie
        result = set()
        for cone in self.objets:
            generateur = self({cone},{cone_courant}) #permet de vérifier qu'il n'y a pas plus d'une flèche entre les deux cones
            next(generateur,None)
            if self.existe_isomorphisme(cone_courant,cone) != None:
                result |= {cone}
            elif self.existe_morphisme(cone,cone_courant) == None:
                return set() 
            elif cone != cone_courant and next(generateur,None) != None:
                return set()
        return result
        
    def transformer_graphviz(self, destination:Union[str,None] = None, afficher_identites:bool = True, complet:bool = True, limite_fleches:int = 30):
        """Permet de visualiser la catégorie avec graphviz.
        `complet` indique s'il faut afficher toutes les flèches ou seulement les flèches élémentaires.
        `limite_fleches` est le nombre maximal de flèches entre deux objets qu'on s'autorise à afficher."""
        ChampPerceptif.nb_viz += 1
        if destination == None:
            destination = "graphviz/"+type(self).__name__+ str(ChampPerceptif.nb_viz)

        graph = Digraph('champ_perceptif')
        graph.attr(concentrate="true" if config.GRAPHVIZ_CONCENTRATE_GRAPHS else "false")
        graph.attr(label=self.nom)
        
        limites = self.objets_limites()
        for o in self.objets:
            graph.node(str(o),color="yellow" if o in limites else "black")
        fleches_elem = set(self[self.objets,self.objets])
        if complet:
            func = lambda x,y : self({x},{y})
        else:
            func = lambda x,y : self[{x},{y}]
        for source,cible in itertools.product(self.objets,repeat=2):
            nb_fleches = 0
            for fleche in func(source,cible):
                if afficher_identites or not fleche.is_identite:
                    if fleche.source not in self.objets:
                        a = fleche.source not in self.objets
                        raise Exception("Source d'une fleche pas dans les objets de la categorie "+repr(fleche.source)+"\n"+str(list(self.objets))+"\n"+str(fleche.source in self.objets))
                    if fleche.cible not in self.objets:
                        raise Exception("Source d'une fleche pas dans les objets de la categorie."+str(fleche.cible)+"\n"+str(self.objets))
                    if len({obj for obj in self.objets if obj == fleche.source}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est source de la fleche.")
                    if len({obj for obj in self.objets if obj == fleche.cible}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est cible de la fleche.")
                    source = {obj for obj in self.objets if obj == fleche.source}.pop() #permet d'avoir toujours un objet de la catégorie comme source
                    cible = {obj for obj in self.objets if obj == fleche.cible}.pop() #permet d'avoir toujours un objet de la catégorie comme cible
                    graph.edge(str(source), str(cible), label=str(fleche), weight="1000", color="grey80" if fleche not in fleches_elem else "black")
                    nb_fleches += 1
                if nb_fleches > limite_fleches:
                    if config.WARNING_LIMITE_FLECHES_ATTEINTE:
                        print("Warning : limite fleches entre "+str(source)+" et "+str(cible)+" atteinte.")
                        break

        graph.render(destination)
        if config.CLEAN_GRAPHVIZ_MODEL:
            import os
            os.remove(destination)

        
def test_ChampPerceptif():
    from GrapheDeComposition import GC,MGC
    from Diagramme import Triangle,DiagrammeIdentite
    
    gc = GC()
    gc |= set("ABCDEF")
    f,g,h,i,j,k,l = [MGC('A','B','f'),MGC('B','C','g'),MGC('D','E','h'),MGC('E','F','i'),MGC('A','D','j'),MGC('B','E','k'),MGC('C','F','l')]
    gc |= {f,g,h,i,j,k,l}
    
    diag_identite = DiagrammeIdentite(gc)
    diag_identite.faire_commuter()
    
    gc.transformer_graphviz()
    
    d1 = Triangle(gc,"DEF",[h,i,i@h])
    d1.transformer_graphviz()
    
    c_p = ChampPerceptif(d1)
    for cone in c_p.objets_cones(gc.objets):
        Cone(cone).transformer_graphviz()
    c_p.transformer_graphviz()
    
    limites = list(c_p.objets_limites())
    for i in range(len(limites)):   
        Cone(limites[i]).transformer_graphviz()
    
if __name__ == '__main__':
    test_ChampPerceptif()