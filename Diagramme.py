from Morphisme import Morphisme
from Foncteur import Foncteur
from GrapheDeComposition import GrapheDeComposition, MorphismeGrapheDeComposition, GC, MGC
from Categorie import CategorieDiscrete, Categorie
import itertools
from config import *
from copy import copy,deepcopy
if GRAPHVIZ_ENABLED:
    from graphviz import Digraph
from typing import *

class Diagramme(Foncteur):
    """
    Un diagramme est simplement un foncteur vu comme une sélection au sein de la catégorie image.
    On peut faire commuter un diagramme si la catégorie image est un graphe de composition.
    """
    nb_viz = 0
    
    def __init__(self, categorie_indexante:Categorie, categorie_indexee:Categorie, 
        application_objets:dict, application_morphismes:dict, representant:Any = None):
        Foncteur.__init__(self,categorie_indexante,categorie_indexee, application_objets,
        application_morphismes,representant=representant if representant != None else "Diagramme "+str(self._id)) 
    
    def faire_commuter(self):
        """
        Change la loi de composition de `MorphismeGrapheDeComposition` pour que le diagramme commute.
        Le diagramme doit cibler un `GrapheDeComposition`.
        """
        if type(self.cible) != GrapheDeComposition:
            raise Exception("Commutation d'un diagramme dont la cible n'est pas un graphe de composition.")
        if type(self.source) != GrapheDeComposition:
            raise Exception("Commutation d'un diagramme dont la source n'est pas un graphe de composition.")
        ##on s'occupe d'abord des cycles
        for objet in self.source.objets:
            for cycle in self.source.trouver_cycles_minimaux(objet):
                image = self(cycle)
                if not image.is_identite:
                    if len(image) == 1:
                        self.cible -= image
                    else:
                        MorphismeGrapheDeComposition.identifier_morphismes(image,self.cible.identite(self(objet)))
        
        for o1,o2 in itertools.product(self.source.objets,repeat=2):
            if o1 != o2:
                composees = {self(composee) for composee in self.source.enumerer_composees_sans_cycle(o1,o2)} #composees à identifier       
                if len(composees) >= 2:
                    composees_triees = sorted(list(composees), key=str)
                    composees_triees = sorted(list(composees_triees), key=len)
                    for c in composees_triees[1:]:
                        composee_depart = c
                        composee_arrivee = composees_triees[0]
                        if composee_depart.is_identite:
                            composee_depart,composee_arrivee = composee_arrivee,composee_depart
                        if composee_depart != composee_arrivee:
                            if len(composee_depart) == len(composee_arrivee) == 1:
                                if composee_depart.is_identite:
                                    raise Exception("Identification d'une identite a un morphisme : "+str(composee_depart)+" et "+str(composee_arrivee))
                                else:
                                    self.cible -= composee_depart
                            else:
                                MorphismeGrapheDeComposition.identifier_morphismes(composee_depart, composee_arrivee)
        
    def implementation(self) -> Categorie:
        """
        Renvoie la sous-catégorie atteinte par le diagramme.
        """
        result = deepcopy(self.cible)
        result.nom = "Implementation du diagramme "+str(self)
        result -= {obj for obj in self.cible.objets if obj not in self._app_objets.values()}
        return result    
        
    def transformer_graphviz(self, destination:Union[str,None] = None,afficher_identite:bool = False, complet:bool = True, limite_fleches:int = 20):
        """Permet de visualiser la categorie indexee par le diagramme avec graphviz"""
        Diagramme.nb_viz += 1
        if destination == None:
            destination = "graphviz/"+type(self).__name__+str(Diagramme.nb_viz)
        graph = Digraph('diagramme')
        graph.attr(concentrate="true" if GRAPHVIZ_CONCENTRATE_GRAPHS else "false")
        graph.attr(label=str(self))
        
        image_objets = [self(obj) for obj in self._app_objets]
        image_objets_count = {obj : image_objets.count(obj) for obj in image_objets} 
        # {obj : nb_occurence} pour avoir la nuance d'orange en fonction du nombre de fois où l'objet est indexé
        image_morph = [self(morph) for morph in self._app_morph]
        image_morph_count = {morph:image_morph.count(morph) for morph in image_morph} 
        # {morph : nb_occurence} pour avoir la nuance d'orange en fonction du nombre de fois où le morph est indexé
        
        for o in self.cible.objets:
            if o in image_objets:
                couleur = [max(255-(image_objets_count[o]-1)*75,150),max(165-(image_objets_count[o]-1)*50,75),0]
                graph.node(str(o), color="#"+''.join(map(lambda x:(2-len(str(hex(x))[2:]))*"0"+str(hex(x))[2:],couleur)))
            else:
                graph.node(str(o),color="black")
        
        for source,cible in itertools.product(self.cible.objets,repeat=2):
            fleches_elem = set(self.cible[{source},{cible}])
            if complet:
                generateur = self.cible({source},{cible})
            else:
                generateur = fleches_elem
            for f in generateur:
                if not f.is_identite or afficher_identite:
                    if f in image_morph:
                        couleur = [max(255-(image_morph_count[f]-1)*75,150),max(165-(image_morph_count[f]-1)*50,0),0]
                        graph.edge(str(f.source),str(f.cible),label=str(f), weight="1000", color="#"+''.join(map(lambda x:(2-len(str(hex(x))[2:]))*"0"+str(hex(x))[2:],couleur)))
                    elif f in fleches_elem:
                        graph.edge(str(f.source),str(f.cible),label=str(f),color="black")
                    else:
                        graph.edge(str(f.source),str(f.cible),label=str(f),color="grey85")
        graph.render(destination)
        if CLEAN_GRAPHVIZ_MODEL:
            import os
            os.remove(destination)
  
def test_Diagramme():
    cat = GC()
    cat |= set("ABCD")
    f,g,h,i = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('D','C','i')]
    cat |= {f,g,h,i}
    
    paralelle = GC()
    paralelle |= set("12")
    a,b = [MGC('1','2','a'),MGC('1','2','b')]
    paralelle |= {a,b}
    
    diagramme = Diagramme(paralelle,cat,{'1':'A','2':'C'},{a:g@f,b:i@h})
    diagramme.transformer_graphviz()
    
    cat.transformer_graphviz()
    diagramme.faire_commuter()
    cat.transformer_graphviz()
  
class DiagrammeObjets(Diagramme):
    """Permet de selectionner des objets sans selectionner de flèche"""
    
    def __init__(self, categorie_indexee:Categorie, objets_a_selectionner:set):
        """`objets_a_selectionner` est une listes d'objets à selectionner dans la catégorie."""
        cat = CategorieDiscrete(set(range(len(objets_a_selectionner))),"Objets")
        objets_a_selectionner = list(objets_a_selectionner)
        Diagramme.__init__(self,cat,categorie_indexee,{i:objets_a_selectionner[i] for i in range(len(objets_a_selectionner))},dict(),representant="O("+','.join(map(str,objets_a_selectionner))+")")

def test_DiagrammeObjets():
    cat = GC()
    cat |= set("ABCD")
    f,g,h,i = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('D','C','i')]
    cat |= {f,g,h,i}
    diag = DiagrammeObjets(cat,"ABCA")
    diag.transformer_graphviz()
   
class Fleche(Diagramme):
    """Permet de selectionner deux objets réliés par une flèche"""
    _cat = GrapheDeComposition({1,2},"1 Flèche")
    _f = MorphismeGrapheDeComposition(1,2,'f')
    _cat |= _f
    
    def __init__(self, categorie_indexee:Categorie, fleche_a_selectionner:Morphisme):
        Diagramme.__init__(self,Fleche._cat,categorie_indexee,{1:fleche_a_selectionner.source,2:fleche_a_selectionner.cible},{Fleche._f:fleche_a_selectionner},representant="Fleche "+str(fleche_a_selectionner))

def test_Fleche():
    cat = GC()
    cat |= set("ABCD")
    f,g,h,i = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('D','C','i')]
    cat |= {f,g,h,i}
    diag = Fleche(cat,h)
    diag.transformer_graphviz()  
    
class Chemins(Diagramme):
    """Diagramme des chemins entre deux objets"""
    
    def __init__(self,categorie_indexee:Categorie, source:Any, cible:Any):
        cat = GrapheDeComposition({1,2},'Chemins')
        chemins = list(categorie_indexee(source,cible))
        morphismes = list(map(lambda x:MorphismeGrapheDeComposition(1,2,x),range(len(chemins))))
        cat |= morphismes
        Diagramme.__init__(self,cat,categorie_indexee,{1:source,2:cible},
        {morphismes[i]:chemins[i] for i in range(len(chemins))}, 'Chemins de '+str(source)+' vers '+str(cible))
  
def test_Chemins():
    cat = GC(set("ABCD"))
    f,g,h,i = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('D','C','i')]
    cat |= {f,g,h,i}
    diag = Chemins(cat,'A','C')
    diag.transformer_graphviz() 
  
class Parallele(Diagramme):
    _cat = GrapheDeComposition({1,2},"Parallele")
    f = MorphismeGrapheDeComposition(1,2,'f')
    g = MorphismeGrapheDeComposition(1,2,'g')
    morphismes = [f,g]
    _cat |= morphismes
    
    def __init__(self, categorie_indexee:Categorie, image_objets:list, image_morphismes:list):
        """  1 ---> 2
               f,g
           `image_objets` et `image_morphismes` sont des listes dans l'ordre tels que les images
            des objets [1,2] et des morphismes [f,g] soient dans l'ordre.
            """
        app_objets = {i+1:image_objets[i] for i in range(len(image_objets))}
        app_morph = {Parallele.morphismes[i]:image_morphismes[i] for i in range(len(image_morphismes))}
        Diagramme.__init__(self,Parallele._cat,categorie_indexee,app_objets,app_morph,
        'Paralleles '+str(image_morphismes[0])+' et '+str(image_morphismes[1]))
  
def test_Parallele():
    cat = GC(set("ABCD"))
    f,g,h,i = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('D','C','i')]
    cat |= {f,g,h,i}
    diag = Parallele(cat,['A','C'],[g@f,i@h])
    diag.transformer_graphviz() 
  
class Triangle(Diagramme):
    _cat = GrapheDeComposition({1,2,3},"Triangle")
    f = MorphismeGrapheDeComposition(1,2,'f')
    g = MorphismeGrapheDeComposition(2,3,'g')
    h = MorphismeGrapheDeComposition(1,3,'h')
    morphismes = [f,g,h]
    
    _cat |= morphismes
    
    def __init__(self, categorie_indexee:Categorie, image_objets:list, image_morphismes:list):
        """  h
            1->3
          f |  ^
            v / g
            2
           `image_objets` et `image_morphismes` sont des listes dans l'ordre tels que les images
            des objets [1,2,3] et des morphismes [f,g,h] soient dans l'ordre.
            """
        app_objets = {i+1:image_objets[i] for i in range(len(image_objets))}
        app_morph = {Triangle.morphismes[i]:image_morphismes[i] for i in range(len(image_morphismes))}
        Diagramme.__init__(self,Triangle._cat,categorie_indexee,app_objets,app_morph,"Triangle "+str(self._id)+" sur "+str(categorie_indexee))

def test_Triangle():
    cat = GC()
    cat |= set("ABCD")
    f,g,h,i = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('D','C','i')]
    cat |= {f,g,h,i}
    diag = Triangle(cat,['A','B','C'],[f,g,g@f])
    diag.transformer_graphviz() 
    
    impl = diag.implementation()
    impl.transformer_graphviz()

class Carre(Diagramme):
    
    _cat = GrapheDeComposition({1,2,3,4},"Carre")
    f = MorphismeGrapheDeComposition(1,2,'f')
    g = MorphismeGrapheDeComposition(2,4,'g')
    h = MorphismeGrapheDeComposition(1,3,'h')
    i = MorphismeGrapheDeComposition(3,4,'i')
    morphismes = [f,g,h,i]
    
    _cat |= morphismes
    
    def __init__(self, categorie_indexee:Categorie, image_objets:list, image_morphismes:list):
        """  h
            1->3
           f|  |i
            v  v
            2->4
             g
           `image_objets` et `image_morphismes` sont des listes dans l'ordre tels que les images
            des objets [1,2,3,4] et des morphismes [f,g,h,i] soient dans l'ordre.
            """
        app_objets = {i+1:image_objets[i] for i in range(len(image_objets))}
        app_morph = {Carre.morphismes[i]:image_morphismes[i] for i in range(len(image_morphismes))}
        Diagramme.__init__(self,Carre._cat,categorie_indexee,app_objets,app_morph,"Carre "+str(self._id)+" sur "+str(categorie_indexee))

def test_Carre():
    cat = GC()
    cat |= set("ABCD")
    f,g,h,i = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('D','C','i')]
    cat |= {f,g,h,i}
    diag = Carre(cat,['A','B','D','C'],[f,g,h,i])
    diag.transformer_graphviz() 

class DiagrammeIdentite(Diagramme):
    """Diagramme qui associe à chaque objet le même objet et à chaque morphisme le même morphisme."""
    
    def __init__(self,categorie:Categorie):
        Diagramme.__init__(self,copy(categorie),categorie,{e:e for e in categorie.objets}
        ,{e:e for e in categorie[categorie.objets,categorie.objets]},"Id("+str(categorie)+')')
        
def test_DiagrammeIdentite():
    cat = GC(set("ABCD"))
    f,g,h,i = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('D','C','i')]
    cat |= {f,g,h,i}
    diag = DiagrammeIdentite(cat)
    diag.transformer_graphviz()
    diag.faire_commuter()
    cat.transformer_graphviz()
    diag.transformer_graphviz()

class DiagrammeConstant(Diagramme):
    """Diagramme constant vers un objet noté Δ.
    Un diagramme constant Δ(A) envoie tous les objets d'une catégorie indexante sur un objet A et toutes
    les flèches de la catégorie indexante vers 1A.
    """
    def __init__(self, categorie_indexante:Categorie, categorie_indexee:Categorie, objet:Any):
        Diagramme.__init__(self,categorie_indexante,categorie_indexee,{o:objet for o in categorie_indexante.objets},
        {f:categorie_indexee.identite(objet) for f in categorie_indexante(abs(categorie_indexante),abs(categorie_indexante))},
        'Δ('+str(objet)+')')

def test_DiagrammeConstant():
    cat = GC(set("ABCD"))
    f,g,h,i = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('D','C','i')]
    cat |= {f,g,h,i}
    diag = DiagrammeConstant(cat,cat,'B')
    diag.transformer_graphviz() 
    Foncteur.transformer_graphviz(diag)

if __name__ == '__main__':
    test_Diagramme()    
    test_DiagrammeObjets()
    test_Fleche()
    test_Chemins()
    test_Parallele()
    test_Triangle()
    test_Carre()
    test_DiagrammeIdentite()
    test_DiagrammeConstant()