from Categorie import Categorie
from CategorieQuotient import CategorieQuotient

class CategorieFactorisee(Categorie):
    """Classe abstraite.
    Un catégorie factorisée est une catégorie de catégories tel que le calcul des foncteurs
    entre deux catégories est factorisée en passant par une catégorie quotient.
    Pour calculer les foncteurs entre C et C', on calcule d'abord les foncteurs F entre
    C/R et C'/R puis on calcule tous les foncteurs F_i entre les sous-catégories classes d'équivalence de C et C' que l'on note
    C[i] et C'[i] tel que F(C[i]) = C'[i].
    On fait ensuite le produit cartésien entre tous ces F_i pour chaque F. On a alors tous les foncteurs de C à C'.
    
    Une catégorie de catégories quotients pour calculer les foncteurs entre catégories quotientées,
    une catégorie de catégories classes d'équivalence pour calculer les foncteurs entre classes d'équivalence."""