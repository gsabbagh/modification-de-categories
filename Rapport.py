from GrapheDeComposition import GC,MGC
from CategoriePreordre import CategoriePreordreEngendree
from CategorieAleatoire import GCA
import Morphisme
import random
from Diagramme import *
from ChampPerceptif import *
from ChampActif import *
import config
from TransformationNaturelle import *
from Cluster import *
'''
## Catégorie

random.seed(26-4)

gc = GC()
gc |= {'A','B','C'}
morphismes = {MGC('A','B','f'),MGC('B','C','g')}
gc |= morphismes
gc.transformer_graphviz()
gc.loi_de_composition_to_csv("lois de composition/loi1.csv")


gc = GC()
gc |= {'A','B','C'}
f,g,h = [MGC('A','B','f'),MGC('B','C','g'),MGC('C','A','h')]
MGC.identifier_morphismes(h@g@f,gc.identite('A'))
MGC.identifier_morphismes(g@f@h,gc.identite('C'))
MGC.identifier_morphismes(f@h@g,gc.identite('B'))
gc |= {f,g,h}
gc.transformer_graphviz()
gc.loi_de_composition_to_csv("lois de composition/loi2.csv")

gc = GCA(15,80)
gc.transformer_graphviz()
gc.loi_de_composition_to_csv("lois de composition/loi3.csv")

## Catégorie Libre

gc = GC()
gc |= {'A','B','C'}
morphismes = {MGC('A','B','f'),MGC('B','C','g')}
gc |= morphismes
gc.transformer_graphviz(complet=False)

## GrapheDeComposition

gc = GC()
gc |= {'A'}
morphismes = {MGC('A','A','f')}
gc |= morphismes
gc.transformer_graphviz()

gc = GC()
gc |= {'A'}
f = MGC('A','A','f')
gc |= {f}
MGC.identifier_morphismes(f@f@f@f,f@f)
gc.transformer_graphviz()
gc.loi_de_composition_to_csv("lois de composition/loi6.csv")

## EnsFinis

from EnsFinis import EnsFinis, EnsParties, Bij

## EnsFinis et EnsParties

Morphisme._id = -18
cat = EnsParties({1,2})
cat.transformer_graphviz(limite_fleches=243)

for app in cat({frozenset({1,2})},{frozenset({1,2})}):
    app.transformer_graphviz()
    
Morphisme._id = -2
    
cat = EnsFinis({frozenset({1,2,3})})
cat.transformer_graphviz(limite_fleches=243)

## Bij

Morphisme._id = -29

cat = EnsParties({1,2,3})
cat_bij = Bij(cat.objets)
cat_bij.transformer_graphviz()

Morphisme._id = -1

cat_bij = Bij({frozenset({1,2,3,4})})
cat_bij.transformer_graphviz()

def tau(x):
    print(x)
    return x[1]+x[0]+x[2:]
    
def rho(x):
    print(x)
    return x[1:]+x[0]
    
def transpo(x,i,j):
    assert(i < j)
    n = len(x)
    for _ in range(i-1):
        x = rho(x)
    for _ in range(j-i-1):
        x = tau(x)
        x = rho(x)
    x = tau(x)
    for _ in range(j-i-1):
        for __ in range(n-1):
            x = rho(x)
        x = tau(x)
    for _ in range(n-i+1):
        x = rho(x)
    return x
        
print(transpo("12345",2,5))
# print(transpo("1234567",3,5))
# print(transpo("123456789",4,7))

## Monoïde

mon = GC({" "})
f = MGC(" "," ","1")
MGC.identifier_morphismes(f@f@f@f,mon.identite(" "))
mon |= {f}

mon.transformer_graphviz()
mon.loi_de_composition_to_csv("lois de composition/loiMonoide.csv")

mon = GC({" "})
a,b = [MGC(" "," ","a"),MGC(" "," ","b")]
MGC.identifier_morphismes(a@a@a,a@a)
MGC.identifier_morphismes(a@a@b,a@a)
MGC.identifier_morphismes(a@b@a,a@b)
MGC.identifier_morphismes(a@b@b,a@b)
MGC.identifier_morphismes(b@a@a,b@a)
MGC.identifier_morphismes(b@a@b,b@a)
MGC.identifier_morphismes(b@b@a,b@b)
MGC.identifier_morphismes(b@b@b,b@b)
mon |= {a,b}

mon.transformer_graphviz()
mon.loi_de_composition_to_csv("lois de composition/loiMonoideStr.csv")

##Groupe
from Foncteur import Foncteur

x = 5
grp1 = GC({" "},"Z/5Z")
f = MGC(" "," ","1")
morph = f
for i in range(x-1):
    morph = morph@f
MGC.identifier_morphismes(morph,grp1.identite(" "))
grp1 |= {f}
grp1.transformer_graphviz()

y = 6
grp2 = GC({" "},"Z/6Z")
g = MGC(" "," ","1")
morph_ = g
for i in range(y-1):
    morph_ = morph_@g
MGC.identifier_morphismes(morph_,grp2.identite(" "))
grp2 |= {g}
grp2.transformer_graphviz()

import CatFinies

cat = CatFinies.CatFinies({grp1,grp2})
cat.transformer_graphviz()

for fonct in cat({grp1},{grp2}):
    fonct.transformer_graphviz()
    
## Foncteur

from Diagramme import Triangle,Carre

gc = GC("1234")
f,g,h,i,j = [MGC('1','2','f'),MGC('2','4','g'),MGC('1','3','h'),MGC('3','4','i'),MGC('1','4','j')]
MGC.identifier_morphismes(g@f,j)
MGC.identifier_morphismes(i@h,j)
gc |= {f,g,h,i,j}
gc.transformer_graphviz()

fonct = Triangle(gc,['1','2','4'],[f,g,j])
Foncteur.transformer_graphviz(fonct)
fonct.transformer_graphviz()

fonct2 = Carre(gc, "1224", [f,g,g,j])
Foncteur.transformer_graphviz(fonct2)
fonct2.transformer_graphviz()


gc = GC("1234")
f,g,h,i = [MGC('1','2','f'),MGC('2','4','g'),MGC('1','3','h'),MGC('3','4','i')]
gc |= {f,g,h,i}
gc.transformer_graphviz()

fonct = Foncteur(gc,gc,{'1':'1','2':'2','3':'3','4':'4'},{f:f,g:g,h:h,i:i})
fonct.transformer_graphviz()

## Transformation naturelle

from TransformationNaturelle import TransfoNat,CatTransfoNat

gc = GC("123456")

f,g,h,i,j,k,l = [MGC('1','2','f'),MGC('2','3','g'),MGC('4','5','h'),MGC('5','6','i'),MGC('1','4','j'),MGC('2','5','k'),MGC('3','6','l')]
gc |= {f,g,h,i,j,k,l}
gc2 = CategoriePreordreEngendree(gc)
gc2.transformer_graphviz()

t1 = Triangle(gc2,"123",[gc2.existe_morphisme('1','2'),gc2.existe_morphisme('2','3'),gc2.existe_morphisme('2','3')@gc2.existe_morphisme('1','2')])
t2 = Triangle(gc2,"456",[gc2.existe_morphisme('4','5'),gc2.existe_morphisme('5','6'),gc2.existe_morphisme('5','6')@gc2.existe_morphisme('4','5')])
t1.transformer_graphviz()
t2.transformer_graphviz()

cat_tn = CatTransfoNat({t1,t2})
cat_tn.transformer_graphviz()
for transfo_nat in cat_tn({t1},{t2}):
    transfo_nat.transformer_graphviz()
    '''
## Comma catégorie

## Catégorie des flèches
from CommaCategorie import *

cat = GC('C')
cat |= {'A','B','C'}
morphismes = [MGC('A','B','f'),MGC('B','C','g')]
cat |= morphismes
cat.transformer_graphviz()

cat_fleches = CategorieFleches(cat)
cat_fleches.transformer_graphviz()

## Catégorie au dessus et en dessous
random.seed(4567)
Morphisme._id = -3

cat = GCA(10,150)
cat.transformer_graphviz()
cat.loi_de_composition_to_csv('lois de composition/loi_au_dessus.csv')

cat_dessus = CategorieSur(cat,random.choice(list(cat.objets)))
cat_dessus.transformer_graphviz()

cat_dessous = CategorieSous(cat,10)
cat_dessous.transformer_graphviz()

## Cône et cocône

'''
random.seed(4567)

Morphisme._id = -9
cat = GCA(20,50)
cat.transformer_graphviz()

f = MGC(43,24,21)
cat |= {f}
cat.verifier_coherence()
cat.transformer_graphviz()

cat -= {30,52,28,47}

f = next(cat[{27},{43}])
g = next(cat[{43},{24}])

from Diagramme import *

diag = Triangle(cat,[27,43,24],[f,g,g@f])
diag.transformer_graphviz()


c_p = ChampPerceptif(diag)
TransfoNat._TransformationNaturelle__id = 0
c_p.T.as_diagram().transformer_graphviz()
# c_p.S.as_diagram().transformer_graphviz()
Categorie.transformer_graphviz(c_p)
c_p.transformer_graphviz()
for cone in c_p.objets:
    Cone(cone).transformer_graphviz()

c_a = ChampActif(diag)
Categorie.transformer_graphviz(c_a)
c_a.transformer_graphviz()
for cocone in c_a.objets:
    Cocone(cocone).transformer_graphviz()
    

## Tout vs Somme

diag2 = DiagrammeObjets(cat,{27,43,24})
diag2.transformer_graphviz()
c_p = ChampPerceptif(diag2)
c_p.transformer_graphviz()
c_a = ChampActif(diag2)
c_a.transformer_graphviz()


cat1 = GC({'A','B','C',})
f1,g1 = [MGC('A','B','f'),MGC('B','C','g')]
cat1 |= {f1,g1}

diag = Fleche(cat1,f1)
c_a = ChampActif(diag)
c_a.transformer_graphviz()
diag.transformer_graphviz()

diag2 = DiagrammeObjets(cat1,"AB")
c_a = ChampActif(diag2)
c_a.transformer_graphviz()
diag2.transformer_graphviz()

# cat1 = GC({'A','B','C','D'})
# f1,g1 = [MGC('A','B','f'),MGC('A','C','g')]
# cat1 |= {f1,g1}

# cat = GC({'A','B','C','D'})
# f,g,h,i = [MGC('A','B','f'),MGC('A','C','g'),MGC('B','D','h'),MGC('C','D','i')]
# cat |= {f,g,h,i}
# MGC.identifier_morphismes(h@f,i@g)

# diag = Diagramme(cat1,cat,{'A':'A','B':'B','C':'C','D':'A'},{f1:f,g1:g})
# c_a = ChampActif(diag)
# c_a.transformer_graphviz()
# diag.transformer_graphviz()

# diag2 = DiagrammeObjets(cat1,cat1.objets)
# diag3 = diag@diag2
# diag3.transformer_graphviz()
# c_a = ChampActif(diag3)
# c_a.transformer_graphviz()
# diag2.transformer_graphviz()



random.seed(4567)

cat = GCA(16,150)
f = MGC(35,17,'f')
cat |= {f}
cat -= {24} 

for s in cat.objets:
    for c in cat.objets:
        fleches  = list(cat({s},{c}))
        for f in fleches[1:]:
            MGC.identifier_morphismes(f,fleches[0])

cat.transformer_graphviz()

diag1 = Carre(cat,[26,22,25,35],[cat.existe_morphisme_elementaire(26,22),cat.existe_morphisme_elementaire(22,35),cat.existe_morphisme_elementaire(26,25),cat.existe_morphisme_elementaire(25,35)])
diag1.transformer_graphviz()

diag2 = Triangle(cat,[17,31,33],[cat.existe_morphisme_elementaire(17,31),cat.existe_morphisme_elementaire(31,33),cat.existe_morphisme(17,33)])
diag2.transformer_graphviz()

clu = CategorieClusters(cat,{diag1,diag2})
clu.transformer_graphviz()

for cluster in clu({diag1},{diag2}):
    cluster.transformer_graphviz()'''