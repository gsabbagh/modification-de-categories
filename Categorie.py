#!/usr/bin/env python
# -*- coding: utf-8 -*-
import types
from collections import defaultdict
import itertools
from Morphisme import Morphisme
import config
import copy
if config.GRAPHVIZ_ENABLED:
    from graphviz import Digraph
from typing import *

def unique_generator(func):
    """Décorateur qui permet de renvoyer les flèches une seule fois par générateur."""
    def wrapper(self, *args, **kwargs) -> Generator[any,None,None]:
        elem_generes = set()
        for elem in func(self,*args,**kwargs):
            if elem not in elem_generes:
                elem_generes |= {elem}
                yield elem
    return wrapper
    
def mise_en_cache_getitem(func):
    """Décorateur qui permer de stocket le résultat de __getitem__ dans un cache et
    cherche dans le cache lors de l'appel de __getitem__."""
    if func.__name__ != '__getitem__':
        raise Exception("Mise en cache d'une fonction differente de __getitem__ : "+str(func))
    
    def wrapper(self, couple_sources_cibles:tuple) -> Generator[Morphisme,None,None]:
        sources,cibles = couple_sources_cibles
        if not hasattr(self,"__cache__getitem__"):
            self.__cache__getitem__ = defaultdict(set)
        nouvelles_sources = set()
        nouvelles_cibles = set()
        for source in sources:
            for cible in cibles:
                if (source,cible) in self.__cache__getitem__:
                    for fleche in self.__cache__getitem__[(source,cible)]:
                        yield fleche
                else:
                    nouvelles_sources |= {source}
                    nouvelles_cibles |= {cible}
        for s in nouvelles_sources:
            for c in nouvelles_cibles:
                for fleche in func(self,({s},{c})):
                    yield fleche
                    self.__cache__getitem__[(s,c)] |= {fleche}
    return wrapper
    
def mise_en_cache_call(func):
    """Décorateur qui permer de stocket le résultat de __call__ dans un cache et
    cherche dans le cache lors de l'appel de __call__."""
    if func.__name__ != '__call__':
        raise Exception("Mise en cache d'une fonction differente de __call__ : "+str(func))
    
    def wrapper(self, sources:set, cibles:set) -> Generator[Morphisme,None,None]:
        if not hasattr(self,"__cache__call__"):
            self.__cache__call__ = defaultdict(set)
        nouvelles_sources = set()
        nouvelles_cibles = set()
        for source in sources:
            for cible in cibles:
                if (source,cible) in self.__cache__call__:
                    for fleche in self.__cache__call__[(source,cible)]:
                        yield fleche
                else:
                    nouvelles_sources |= {source}
                    nouvelles_cibles |= {cible}
        for s in nouvelles_sources:
            for c in nouvelles_cibles:
                for fleche in func(self,{s},{c}):
                    yield fleche
                    self.__cache__call__[(s,c)] |= {fleche}
    return wrapper

class Categorie:
    """Classe abstraite qui définit ce qu'est une catégorie.
    Toutes les catégories qui héritent de Categorie doivent surcharger :
        - l'opérateur __call__ tel que si C est une catégorie :
            C({a_i},{b_i}) renvoie l'ensemble des flèches d'un élément de {a_i} vers un élément de {b_i}.
        - la méthode identite(self,objet) qui renvoie l'identité de l'objet.
        
    On reprend la notation classique C(a,b) pour les flèches de a à b
    et abs(C) = |C| pour les objets de C.
    
    Listes des méthodes importantes :
        - abs(C) (ou C.objets) opérateur pour obtenir les objets
        - C(_,_) opérateur pour obtenir un itérateur sur tous les morphismes
        - C[_,_] opérateur pour obtenir un itérateur sur les morphismes élémentaires (morphismes à partir dequels on peut obtenir tous les morphismes par composition)
        - |= opérateur pour ajouter des objets
        - -= opérateur pour supprimer des objets
        
    Pour toutes les catégories on peut :
        - vérifier sa cohérence avec la méthode verifier_coherence
        - visualiser son graphe sous-jacent avec la méthode transformer_graphviz
        - exporter sa loi de composition en csv avec loi_de_composition_to_csv
    """
    _id = 0
    nb_viz = 0  # nombre de graphes graphviz générés    
    
    def __init__(self, objets:set = set(), nom:str = None):
        Categorie._id += 1
        self._id = Categorie._id
        self.nom = "Categorie "+str(self._id) if nom == None else nom
        self._objets = frozenset() # read-only attribute
        self |= objets
        if config.TOUJOURS_VERIFIER_COHERENCE:
            self.verifier_coherence()

    def __hash__(self) -> int:
        return hash(self.objets)

    @unique_generator # la méthode call doit renvoyer une seule fois chaque flèche
    def __call__(self, sources: set, cibles: set) -> Generator[Morphisme,None,None]:
        """
        Cette méthode est virtuelle pure.
        Les classes filles doivent l'implémenter tel que si C est une catégorie :
            C({a_i},{b_i}) renvoie un itérateur sur l'ensemble des flèches d'un élément de {a_i} vers un élément de {b_i}.
        """
        raise NotImplementedError("Les categories filles doivent implementer cette methode pour renvoyer les morphismes C(a,b) (fleches de a vers b) et C(a) (identite de a)")
    
    def __getitem__(self, couple_sources_cibles:tuple) -> Generator[Morphisme,None,None]:
        """
            C[{a_i},{b_i}] renvoie un itérateur sur l'ensemble des flèches élémentaires d'un élément de {a_i} vers un élément de {b_i}.
            Les flèches élémentaires d'une catégorie C sont des flèches telles que toutes les flèches de C s'obtiennent par composition de flèches élémentaires.
            
            Par défaut C[{a_i},{b_i}] renvoie toutes les flèches (équivalent à C({a_i},{b_i})), les classes filles peuvent surcharger cette méthode
            pour optimiser les calculs.

            Exemple où les flèches élémentaires ont un intérêt : pour construire un foncteur, il suffit de donner une application entre flèches élémentaires.
            
            Surcharger __getitem__ conduit à hériter de CategorieLibre pour s'assurer que toutes les flèches sont bien générées
            par les flèches élémentaires.
            On peut surcharger la méthode __call__ de CategorieLibre pour optimiser les calculs quand c'est pertinent. Il faut alors
            s'assurer que la nouvelle méthode renvoie les mêmes morphismes que l'ancienne méthode aurait renvoyée.
            
            Si on surcharge __getitem__, on peut aussi surcharger __iter__ des morphismes associés à la catégorie pour itérer sur les constituants
            élémentaires du morphisme.
        """
        if len(couple_sources_cibles) != 2:
            raise Exception("On s'attend à avoir un couple : "+str(couple_sources_cibles))
        return self(*couple_sources_cibles)
     

    def identite(self,objet: Any) -> Morphisme:
        """
        Cette méthode est virtuelle pure.
        Les classes filles doivent l'implémenter tel que si C est une catégorie,
        C.identite(objet) renvoie l'identité de l'`objet`.
        """
        raise NotImplementedError("Les categories filles doivent implementer cette methode.")

    def __str__(self) -> str:
        return self.nom
        
    def __abs__(self) -> frozenset:
        """
        Renvoie les objets de la catégorie.
        Cette syntaxe suit la convention mathématiques |C| = objets de C.
        """
        return self.objets
        
    @property
    def objets(self) -> frozenset:
        return self._objets

    def __ior__(self,other:set):
        """
        Soit C une catégorie.
            C |= {a_1,a_2,...} ajoute les objets a_i à la catégorie C.
        """
        self._objets |= other
        if hasattr(self,"__cache__getitem__"):
            self.__cache__getitem__ = defaultdict(set)
        if hasattr(self,"__cache__call__"):
            self.__cache__call__ = defaultdict(set)
        return self
            
    def __isub__(self,other:Any):
        """
        Soit C une catégorie.
            C -= {a_1,a_2,...} supprime les objets a_i de la catégorie C.
        """
        self._objets -= other
        if hasattr(self,"__cache__getitem__"):
            self.__cache__getitem__ = defaultdict(set)
        if hasattr(self,"__cache__call__"):
            self.__cache__call__ = defaultdict(set)
        return self
        
    def verifier_coherence(self):
        """Vérifie la cohérence de la structure (tous les axiomes des catégories sont vérifiés)."""
        
        morphismes = sorted(self(self.objets,self.objets))
        ## On vérifie que les identités sont neutres
        for morphisme in morphismes:
            if not (morphisme@self.identite(morphisme.source) == self.identite(morphisme.cible)@morphisme == morphisme):
                raise Exception("Incoherence Categorie : le morphisme "+str(morphisme)+" est modifie par une identite.\n"\
                +repr(morphisme)+" o "+repr(self.identite(morphisme.source))+" = "+repr(morphisme@self.identite(morphisme.source))+"\n"\
               +repr(self.identite(morphisme.cible))+" o "+repr(morphisme)+" = "+repr(self.identite(morphisme.cible)@morphisme)+"\n"+\
               repr(morphisme))
        
        ## On vérifie l'associativité
        for m1,m2,m3 in itertools.product(morphismes,repeat=3):
            if m1.source == m2.cible and m2.source == m3.cible:
                if (m1@m2)@m3 != m1@(m2@m3):
                    raise Exception("Incoherence Categorie : associativite pas respectee pour "+str(m1)+", "+str(m2)+", "+str(m3)+\
                    "\n(m1@m2)@m3 = "+str((m1@m2)@m3)+"  m1@(m2@m3) = "+str(m1@(m2@m3))+
                    "\nm1@m2 = "+str(m1@m2)+"  m2@m3 = "+str(m2@m3))#+"\nm2@m1 = "+str(m2@m1)+"  m3@m2 = "+str(m3@m2))
                    
        ## On vérifie que toutes les composées existent
        for m1 in morphismes:
            for m2 in self({m1.cible},self.objets):
                if m2@m1 not in morphismes:
                    raise Exception("Incoherence Categorie : composee de deux morphismes pas dans la categorie "+str(m2@m1))
    
    def isomorphismes(self, source:Any, cible:Any) -> Generator[tuple,None,None]:
        """Trouve les isomorphismes entre `source` et `cible`.
        Renvoie un générateur de couples {f,g} tels que gof = Id1 et fog = Id2, et f:source->cible.
        """
        for f in self({source},{cible}):
            for g in self({cible},{source}):
                if (f@g).is_identite and (g@f).is_identite:
                    yield (f,g)
    
    def existe_morphisme(self, source:Any, cible:Any) -> Union[Morphisme,None]:
        """Renvoie un morphisme de `source` à `cible` s'il existe, renvoie None sinon."""
        elem = next(self[{source},{cible}],None)
        if elem != None:
            return elem
        return next(self({source},{cible}),None)
        
    def existe_morphisme_elementaire(self, source:Any, cible:Any) -> Union[Morphisme,None]:
        """Renvoie un morphisme élémentaire de `source` à `cible` s'il existe, renvoie None sinon."""
        return next(self[{source},{cible}],None)
        
    def existe_isomorphisme(self, source:Any, cible:Any) -> Union[tuple,None]:
        """Renvoie un isomorphisme de `source` à `cible` s'il existe sous la forme d'un couple (f,g) avec f:source->cible, et g:cible->source
        renvoie None sinon."""
        return next(self.isomorphismes(source,cible),None)
        
        
    def table_loi_de_composition(self):
        """
        Renvoie un dico de dico tel que table[f][g] = f o g, table[f][g] = None si f et g ne sont pas composables.
        La table contient les compositions triviales f o Id = f, f o g = f o g, f o (g o h) = (f o g) o h etc.
        O(m²)
        """
        table = defaultdict(dict)
        for A,B in itertools.product(self.objets, repeat=2):
            for f in self({A},{B}):
                for g in self({B},abs(self)):
                    table[g][f] = g@f
        return table

            
    def loi_de_composition_to_csv(self, destination:Union[str,None]=None, sep: str=','):
        """
        Exporte la loi de composition de la catégorie en csv.
        Si destination == None, alors on le print sinon on l'écrit dans un fichier à la destination.
        `sep` est le séparateur.
        """
        fleches = list(self(self.objets,self.objets))
        fleches.sort()
        # fleches.sort(key = lambda x:len(str(x)))
        result = sep+sep.join(map(str,fleches))+"\n"
        for f in fleches:
            result += str(f)+sep
            for g in fleches:
                if f.cible == g.source:
                    result += str(g@f)+sep
                else:
                    result += "X"+sep
            result = result[:-1]+'\n'
        if destination != None:
            with open(destination, 'w') as f:
                f.write(result)
        else:
            result = result.replace(',','\t,')
            print(result)

    def transformer_graphviz(self, destination:Union[str,None] = None, afficher_identites:bool = True, complet:bool = True, limite_fleches:int = 30):
        """Permet de visualiser la catégorie avec graphviz.
        `complet` indique s'il faut afficher toutes les flèches ou seulement les flèches élémentaires.
        `limite_fleches` est le nombre maximal de flèches entre deux objets qu'on s'autorise à afficher."""
        Categorie.nb_viz += 1
        if destination == None:
            destination = "graphviz/"+type(self).__name__+ str(Categorie.nb_viz)

        graph = Digraph('categorie')
        graph.attr(concentrate="true" if config.GRAPHVIZ_CONCENTRATE_GRAPHS else "false")
        graph.attr(label=self.nom)
        
        for o in self.objets:
            graph.node(str(o))
        fleches_elem = set(self[self.objets,self.objets])
        if complet:
            func = lambda x,y : self({x},{y})
        else:
            func = lambda x,y : self[{x},{y}]
        for source,cible in itertools.product(self.objets,repeat=2):
            nb_fleches = 0
            for fleche in func(source,cible):
                if afficher_identites or not fleche.is_identite:
                    if fleche.source not in self.objets:
                        a = fleche.source not in self.objets
                        raise Exception("Source d'une fleche pas dans les objets de la categorie "+repr(fleche.source)+"\n"+str(list(self.objets))+"\n"+str(fleche.source in self.objets))
                    if fleche.cible not in self.objets:
                        raise Exception("Source d'une fleche pas dans les objets de la categorie."+str(fleche.cible)+"\n"+str(self.objets))
                    if len({obj for obj in self.objets if obj == fleche.source}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est source de la fleche.")
                    if len({obj for obj in self.objets if obj == fleche.cible}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est cible de la fleche.")
                    source = {obj for obj in self.objets if obj == fleche.source}.pop() #permet d'avoir toujours un objet de la catégorie comme source
                    cible = {obj for obj in self.objets if obj == fleche.cible}.pop() #permet d'avoir toujours un objet de la catégorie comme cible
                    graph.edge(str(source), str(cible), label=str(fleche), weight="1000", color="grey80" if fleche not in fleches_elem else "black")
                    nb_fleches += 1
                if nb_fleches > limite_fleches:
                    if config.WARNING_LIMITE_FLECHES_ATTEINTE:
                        print("Warning : limite fleches entre "+str(source)+" et "+str(cible)+" atteinte.")
                        break

        graph.render(destination)
        if config.CLEAN_GRAPHVIZ_MODEL:
            import os
            os.remove(destination)

def test_Categorie():
    from GrapheDeComposition import GC,MGC
    GC = GC({'A','B','C'})
    f,g,h = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','A','h')]
    MGC.identifier_morphismes(h@h@h,h)
    GC |= {f,g,h}
    GC.transformer_graphviz()
    GC.loi_de_composition_to_csv()

class SousCategoriePleine(Categorie):
    """
    Sous-catégorie pleine d'une catégorie.
    Une sous-catégorie pleine est constituée d'un sous-ensemble d'objets de la catégorie initiale
    et de tous les morphismes entre ces objets.
    """
    
    def __new__(cls, categorie_originelle:Categorie, objets:set) -> 'SousCategoriePleine':
        copie = copy.copy(categorie_originelle)
        copie -= {obj for obj in copie.objets if obj not in objets}
        return copie
    
def test_SousCategoriePleine():
    from GrapheDeComposition import GC,MGC
    GC = GC(set("ABCDE"))
    morphismes = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('D','E','i'),MGC('C','E','j')]
    GC |= morphismes
    GC.transformer_graphviz()
    
    sous_cat_pleine = SousCategoriePleine(GC,set("ADE"))
    sous_cat_pleine.transformer_graphviz()
    
class SousCategorie(Categorie):
    def __init__(self, categorie_originelle:Categorie, objets_a_conserver:frozenset, morph_a_conserver:frozenset,nom:Union[str,None] = None):
        """`objets_a_conserver` contient tous les objets à conserver.
        `morph_a_conserver` contient tous les morphismes à conserver (pas seulement les flèches élémentaires).
        Toutes les flèches deviennent élémentaires (on perd la structure de la catégorie initiale).
        /!\ Les flèches restantes doivent former une catégorie, si deux flèches sont composables alors la composée doit être dans `morph_a_conserver` /!\ """
        self._cat_origin = categorie_originelle
        self._morph = morph_a_conserver
        Categorie.__init__(self,objets_a_conserver,nom if nom != None else "Sous catégorie de "+str(categorie_originelle))
        
    def identite(self,objet:Any) -> Morphisme:
        assert(objet in self.objets)
        return self._cat_origin.identite(objet)
        
    def __call__(self, sources: set, cibles: set) -> Generator[Morphisme,None,None]:
        assert(len(sources-self.objets) == 0 and len(cibles-self.objets) == 0)
        for morph in self._cat_origin(sources,cibles):
            if morph in self._morph or morph.is_identite:
                yield morph

                
def test_SousCategorie():
    from GrapheDeComposition import GC,MGC
    GC = GC(set("ABCDE"))
    morphismes = [MGC('A','B','f'),MGC('B','C','g'),MGC('A','D','h'),MGC('D','E','i'),MGC('C','E','j')]
    GC |= morphismes
    GC.transformer_graphviz()
    
    sous_cat = SousCategorie(GC,set("ABDE"),{morphismes[0],morphismes[2],morphismes[3],morphismes[3]@morphismes[2]})
    sous_cat.transformer_graphviz()
    
class CategorieDiscrete(Categorie):
    """
    Catégorie discrète : catégorie dont les seuls morphismes sont des identités.
    """
    class Identite(Morphisme):
        """
        Tous les morphismes de la catégorie discrète sont des identités.
        """
        def __init__(self,objet:Any):
            Morphisme.__init__(self,objet,objet,"Id"+str(objet),True)
            
        def __matmul__(self,other:'CategorieDiscrete.Identite') -> 'CategorieDiscrete.Identite':
            """
            On peut composer uniquement une identité avec elle même.
            """
            if type(other) != CategorieDiscrete.Identite:
                raise Exception("Composition d'un morphisme de type inconnu avec une Identite\n"+repr(self)+'\n'+repr(other))
            if self.source != other.source:
                raise Exception("Composition incoherente d'Identites")
            return self
            
        def __eq__(self,other:'CategorieDiscrete.Identite') -> bool:
            return type(other) == CategorieDiscrete.Identite and self.source == other.source
            
        def __hash__(self) -> int:
            return hash(self.source)
        
    def __call__(self,sources:set,cibles:set) -> Generator[Morphisme,None,None]:
        return {CategorieDiscrete.Identite(a) for a in sources for b in cibles if a == b}
    
    def identite(self,objet:Any) -> 'CategorieDiscrete.Identite':
        return CategorieDiscrete.Identite(objet)

def test_CategorieDiscrete():
    cat_discrete = CategorieDiscrete(set("ABCDE"))
    cat_discrete.transformer_graphviz(afficher_identites=True)
    
    
if __name__ == '__main__':
    # test_Categorie()
    # test_CategorieDiscrete()
    # test_SousCategoriePleine()
    test_SousCategorie()