from Categorie import Categorie
from Foncteur import Foncteur
from CategorieQuotient import CategorieQuotient
from EnsFinis import EnsFinis, Application
from Morphisme import Morphisme
import itertools

from typing import *

class CategorieComposantesConnexes(CategorieQuotient):
    """`CategorieComposantesConnexes` peut être abrégé en `CatCC`
    Catégorie quotientée par la relation d'équivalence sur les objets suivante :
     x ~ y si il existe f: x -> y ou il existe f: y -> x."""
    
    def __init__(self,categorie_a_quotienter:Categorie, nom:str = None):
        if nom == None:
            nom = "Catégorie des composantes connexes de "+str(categorie_a_quotienter)    
        CategorieQuotient.__init__(self,categorie_a_quotienter,nom)
        for obj1, obj2 in itertools.product(categorie_a_quotienter.objets,repeat=2):
            if obj1 != obj2:
                if categorie_a_quotienter.existe_morphisme(obj1,obj2) or categorie_a_quotienter.existe_morphisme(obj2,obj1):
                    self.identifier_morphismes(categorie_a_quotienter.identite(obj1),categorie_a_quotienter.identite(obj2))
                    for m in categorie_a_quotienter({obj1},{obj2}):
                        self.identifier_morphismes(m,categorie_a_quotienter.identite(obj2))
                    for m in categorie_a_quotienter({obj2},{obj1}):
                        self.identifier_morphismes(m,categorie_a_quotienter.identite(obj2))
            else:
                for m in categorie_a_quotienter({obj1},{obj1}):
                    self.identifier_morphismes(m,categorie_a_quotienter.identite(obj1))
CatCC = CategorieComposantesConnexes      
    
def test_CategorieComposantesConnexes():
    from CategorieAleatoire import GrapheCompositionAleatoire
    import random

    random.seed(3)
    for i in range(3):
        c = GrapheCompositionAleatoire()
        c.transformer_graphviz()
        
        c_a = CategorieComposantesConnexes(c)
        c_a.transformer_graphviz()
        c_a.fonct_surjection.transformer_graphviz()
        
        for obj in c_a.objets:
            c_a.sous_cat_equiv(obj).transformer_graphviz()
        
        
class CategorieCatComposantesConnexes(Categorie):
    """Catégorie des catégories composantes connexes.
    Les foncteurs sont simplement des applications d'objets."""
    
    def __init__(self,objets:set=set(),nom:str="CatCatComposantesConnexes"):
        Categorie.__init__(self,objets,nom)
    
    def __call__(self, sources:set, cibles:set) -> Generator[Application,None,None]:
        '''/!\ __call__ n'appelle pas __getitem__ /!\ '''
        ens_finis = EnsFinis()
        for source in sources:
            for cible in cibles:
                ens_finis |= {source.objets,cible.objets}
                for m in ens_finis.__call__({source.objets},{cible.objets}):
                    m = m.as_dict()
                    yield Foncteur(source,cible,m,{source.identite(obj):cible.identite(m[obj]) for obj in m})
    
    def __getitem__(self, couple_sources_cibles:tuple) -> Generator[Application,None,None]:
        '''/!\ __call__ n'appelle pas __getitem__ /!\ '''
        sources,cibles = couple_sources_cibles
        ens_finis = EnsFinis()
        for source in sources:
            for cible in cibles:
                ens_finis |= {source.objets,cible.objets}
                for m in ens_finis.__getitem__(({source.objets},{cible.objets})):
                    m = m.as_dict()
                    yield Foncteur(source,cible,m,{source.identite(obj):cible.identite(m[obj]) for obj in m})
        
def test_CategorieCatComposantesConnexes():
    from CategorieAleatoire import GrapheCompositionAleatoire
    import random

    random.seed(3)
    C = CategorieCatComposantesConnexes()
    
    c = GrapheCompositionAleatoire()
    c.transformer_graphviz()

    c_a = CategorieComposantesConnexes(c)
    c_a.transformer_graphviz()
    
    C |= {c_a}
    
    c = GrapheCompositionAleatoire()
    c.transformer_graphviz()

    c_a2 = CategorieComposantesConnexes(c)
    c_a2.transformer_graphviz()
    
    C |= {c_a2}
    
    print(len(list(C({c_a},{c_a2}))))
    print(len(list(C({c_a2},{c_a}))))
    
    C.transformer_graphviz()
    
if __name__ == '__main__':
    test_CategorieComposantesConnexes()
    # test_CategorieCatComposantesConnexes()