@startuml
abstract class Categorie {
- objets : set
+ nom : string
+ getObjets()
{abstract} + __call__(sources,cibles)
{abstract} + identite(objet)
}

note right of Categorie::__call__
Cette surcharge d'opérateur doit renvoyer
les flèches de sources à cibles,
on a la convention habituelle C(a,b) = flèches
de a vers b dans la catégorie C.
end note

note right of Categorie::identite
Cette méthode doit renvoyer l'identite d'un objet.
end note

abstract class Morphisme{
+ source : Objet
+ cible : Objet
+ is_identite : bool
+ nom : string
{abstract} + __matmul__(morph)
}


note right of Morphisme::__matmul__
Cette méthode doit renvoyer la
composition du morphisme avec morph.
On a la convention f@g = fog.
end note

abstract class CategorieLibre {
{abstract} + __getitem__(sources,cibles)
 }


note right of CategorieLibre::__getitem__
Cette méthode doit renvoyer les morphismes
élémentaires des sources vers les cibles.
end note


Categorie <|-- CategorieLibre


class GrapheDeComposition{
+ __ior__(morph)
+ __isub__(morph)
}


CategorieLibre <|-- GrapheDeComposition

class MorphismeGrapheDeComposition{
+ {static} loi_de_composition : dict
- chemin : tuple
}

Morphisme <|-- MorphismeGrapheDeComposition

class EnsFinis
EnsFinis --|> Categorie
class CatFinies
CatFinies --|> Categorie
class CategoriePreordre
CategoriePreordre --|> Categorie
class CategorieInteractions
CategorieInteractions --|> Categorie
class CategorieTransformationsNaturelles 
CategorieTransformationsNaturelles  ---|> Categorie
class CommaCategorie
CategorieLibre <|-- CommaCategorie
class ChampActif
CommaCategorie <|-- ChampActif
class ChampPerceptif
CommaCategorie <|-- ChampPerceptif
class CategorieAleatoire
CategorieAleatoire --|> GrapheDeComposition
class CategorieHomologue
CategorieHomologue --|> CatFinies
class CategorieProhomologue
CategorieProhomologue --|> CatFinies


class Application
Application --|> Morphisme
class Foncteur
Foncteur--|> Morphisme
class MorphismePreordre
MorphismePreordre--|> Morphisme
class FlecheCommaCategorie
Morphisme ---|> FlecheCommaCategorie
class Interaction
Interaction --|> Morphisme
class TransformationNaturelle
TransformationNaturelle --|> Interaction
@enduml