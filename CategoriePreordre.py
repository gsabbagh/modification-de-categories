from Categorie import Categorie
from Foncteur import Foncteur
from Morphisme import Morphisme
from typing import *

class MorphismePreordre(Morphisme):
    """MorphismePreordre peut être abrégé en MPO.
    Un morphisme préordre est un morphisme au sein d'une catégorie préordre,
    c'est-à-dire qu'entre deux objets il y a au plus un morphisme.
    Deux morphismes préordre sont égaux ssi ils ont la même source et la même cible."""
    
    def __init__(self, source:Any, cible:Any, nom:str = None):
        if nom == None and source == cible:
            nom = "Id"+str(source)
        Morphisme.__init__(self,source,cible,nom,source==cible)
    
    def __eq__(self, other:'MorphismePreordre') -> bool:
        return type(self) == type(other) and self.source == other.source and self.cible == other.cible
    
    def __hash__(self) -> int:
        return 1
        
    def __matmul__(self, other:'MorphismePreordre') -> 'MorphismePreordre':
        return MorphismePreordre(other.source,self.cible,str(self)+'o'+str(other))

MPO = MorphismePreordre

class CategoriePreordre(Categorie):
    """Catégorie similaire à `CatFine`, `CategoriePreordre` ne garde pas en mémoire tous les morphismes qui ont été réunis en un.
    `CategoriePreordre` est donc moins coûteux en temps et en mémoire que `CatFine`.
    Classe abstraite qui définit ce qu'est une catégorie préordre.
    Les classes filles doivent implémenter la méthode existe_morphisme(self,source:Any,cible:Any) -> Union(Morphisme,None) qui détermine
    entièrement les flèches de la catégorie.
    Une catégorie préordre C est une catégorie telle qu'il n'y a pas plus de deux flèches entre deux objets de C."""
    
    _id = 0
    
    def __init__(self, objets:set = set(), nom:Union[str,None] = None):
        CategoriePreordre._id += 1
        self._id = CategoriePreordre._id
        if nom == None:
            nom = "Catégorie préordre "+str(self._id)
        self.nom = nom
        Categorie.__init__(self, objets, nom)
    
    def identite(self, objet:Any) -> MorphismePreordre:
        return MorphismePreordre(objet,objet)
    
    def __call__(self, sources:set, cibles:set) -> Generator[MorphismePreordre,None,None]:
        for source in sources:
            for cible in cibles:
                m = self.existe_morphisme(source,cible)
                if m != None:
                    yield MorphismePreordre(source,cible,m.nom)
                    
    def __getitem__(self, couple_sources_cibles:tuple) -> Generator[MorphismePreordre,None,None]:
        sources,cibles = couple_sources_cibles
        for source in sources:
            for cible in cibles:
                m = self.existe_morphisme_elementaire(source,cible)
                if m != None:
                    yield MorphismePreordre(source,cible,m.nom)
                          
class CategoriePreordreInstanciable(CategoriePreordre):
    """Categorie pré-ordre instanciable.
    On ajoute des objets comme d'habitude.
    Pour ajouter un morphisme, appeler la méthode ajouter_morphisme ou ajouter_morphismes.
    Possibilité de supprimer un morphisme."""
    
    def __init__(self, objets:set = set(), nom:Union[str,None] = None):
        self._succ = {o:{o} for o in objets} # dictionnaire des successeurs
        self._pred = {o:{o} for o in objets} # dictionnaire des prédecesseurs
        CategoriePreordre.__init__(self, objets, nom)
    
    def ajouter_morphisme(self, source:Any, cible:Any):
        self._succ[source] |= {cible}
        self._pred[cible] |= {source}
        
    def ajouter_morphismes(self, *sources_cibles:Tuple):
        for source,cible in sources_cibles:
            self.ajouter_morphisme(source,cible)
            
    def supprimer_morphisme(self, source:Any, cible:Any):
        self._succ[source] -= {cible}
        self._succ[cible] -= {source}
    
    def existe_morphisme_elementaire(self, source:Any, cible:Any) -> Union[Morphisme,None]:
        if cible in self._succ[source]:
            return MPO(source,cible)
            
    def existe_morphisme(self, source:Any, cible:Any, objets_visites=tuple()) -> Union[Morphisme,None]:
        if source == cible:
            return MPO(source,cible)
        for obj in self._succ[source]:
            if obj not in objets_visites:
                if self.existe_morphisme(obj,cible,objets_visites+(obj,)) != None:
                    return MPO(source,cible)
                    
    def decomposition_morphisme(self, morphisme:MPO, objets_visites=tuple()) -> list:
        source,cible = morphisme.source,morphisme.cible
        if source == cible:
            return [MPO(source,cible)]
        for obj in self._succ[source]:
            if obj not in objets_visites:
                decompo = self.decomposition_morphisme(MPO(obj,cible),objets_visites+(obj,))
                if decompo != None:
                    return decompo+[MPO(obj,decompo[-1].source)]
       
def test_CategoriePreordreInstanciable():
    cat = CategoriePreordreInstanciable(set("ABCDE"))
    cat.ajouter_morphismes("AB","BC","DE","ED")
    cat.transformer_graphviz()
    
       
        
class CategoriePreordreEngendree(CategoriePreordre):
    """La catégorie préordre engendrée par une catégorie C est une sous-catégorie maximale C' de C
    telle qu'il n'y a pas plus de deux flèches entre deux objets de C'.
    L'attribut `foncteur_engendre` est le foncteur de la catégorie initiale vers la catégorie préordre engendrée."""
    
    def __init__(self, categorie:Categorie, nom:str = None):
        self._categorie_initiale = categorie
        CategoriePreordre.__init__(self,categorie.objets, "Catégorie préordre engendrée par "+str(categorie) if nom == None else nom)
    
    def existe_morphisme(self, source:Any, cible:Any) -> Union[Morphisme,None]:
        m = self._categorie_initiale.existe_morphisme(source,cible)
        if m != None:
            return MorphismePreordre(source,cible,m.nom)
        return None
    
    def existe_morphisme_elementaire(self, source:Any, cible:Any) -> Union[Morphisme,None]:
        m = self._categorie_initiale.existe_morphisme_elementaire(source,cible)
        if m != None:
            return MorphismePreordre(source,cible,m.nom)
        return None
        
    def decomposition_morphisme(self,morphisme):
        return self._categorie_initiale.decomposition_morphisme(self._categorie_initiale.existe_morphisme(morphisme.source,morphisme.cible))
        
    @property
    def foncteur_engendre(self):
        c_i = self._categorie_initiale
        return Foncteur(c_i,self,{o:o for o in self.objets},{m:next(self({m.source},{m.cible})) for m in c_i[c_i.objets,c_i.objets]})
    
def test_CategoriePreordreEngendree():
    from GrapheDeComposition import GC,MGC
    
    gc = GC()
    gc |= set("ABCDEF")
    f,g,h,i,j,k,l = [MGC('A','B','f'),MGC('B','C','g'),MGC('D','E','h'),MGC('E','F','i'),MGC('A','D','j'),MGC('B','E','k'),MGC('C','F','l')]
    gc |= {f,g,h,i,j,k,l}
    
    gc.transformer_graphviz()
    
    c_po = CategoriePreordreEngendree(gc)
    c_po.transformer_graphviz()
    
    c_po.foncteur_engendre.transformer_graphviz()
    
if __name__ == '__main__':
    test_CategoriePreordreEngendree()
    test_CategoriePreordreInstanciable()

'''from CategorieQuotient import CategorieQuotient
from Morphisme import Morphisme
from typing import *

class CategoriePreordre(CategorieQuotient):
    """CategoriePreordre est aussi appelée CatFine.
    Une catégorie préordre C est une catégorie telle qu'il n'y a pas plus de deux flèches entre deux objets de C."""
    
    def rel_equiv_morph(self, morph1:Morphisme, morph2:Morphisme) -> bool:
        return morph1.source == morph2.source and morph1.cible == morph2.cible

CatFine = CategoriePreordre
 
    
def test_CategoriePreordre():
    from GrapheDeComposition import GC,MGC
    
    gc = GC()
    gc |= set("ABCDEF")
    f,g,h,i,j,k,l = [MGC('A','B','f'),MGC('B','C','g'),MGC('D','E','h'),MGC('E','F','i'),MGC('A','D','j'),MGC('B','E','k'),MGC('C','F','l')]
    gc |= {f,g,h,i,j,k,l}
    
    gc.transformer_graphviz()
    
    c_po = CategoriePreordre(gc)
    c_po.transformer_graphviz()
    
    c_po.fonct_surjection.transformer_graphviz()
    
    from CategorieAleatoire import GrapheCompositionAleatoire
    import random
    random.seed(1233)
    for i in range(5):
        c = GrapheCompositionAleatoire()
        c.transformer_graphviz()
        
        c_a = CategoriePreordre(c)
        c_a.transformer_graphviz()
        c_a.fonct_surjection.transformer_graphviz()
        
        
if __name__ == '__main__':
    test_CategoriePreordre()'''