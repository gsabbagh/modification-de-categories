from Categorie import Categorie
from Foncteur import Foncteur
from CatFinies import CatFinies
from EnsFinis import CategorieBijections,Bijection
from ChampPerceptif import ChampPerceptif,Cone
from collections import defaultdict
import itertools
from typing import *

class FoncteurOubli(Foncteur):
    """Foncteur d'oubli du champ perceptif.
    Associe à chaque cône sont apex et à chaque flèche entre cône la flèche d'où elle vient."""
    
    def __init__(self, champ_perceptif:ChampPerceptif):
        univers = champ_perceptif.foncteur_diagonal.source
        Foncteur.__init__(self, champ_perceptif, univers, {obj:obj.e for obj in abs(champ_perceptif)},
        {f:f.k for f in champ_perceptif(abs(champ_perceptif),abs(champ_perceptif))})
       
class CategorieProhomologue(CatFinies):
    """Catégorie dont les objets sont des champs perceptifs et les morphismes sont des foncteurs bijectifs qui commutent avec le foncteur d'oubli."""
    
    def verifier_coherence(self):
        CatFinies.verifier_coherence(self)
        for f in self(abs(self),abs(self)):
            fonct_oubli_source = FoncteurOubli(f.source)
            fonct_oubli_cible = FoncteurOubli(f.cible)
            if fonct_oubli_source != fonct_oubli_cible@f:
                raise Exception("Incoherence CategorieProhomologue : "+str(f)+" ne commute pas avec les foncteurs d'oublis")
        
    def __call__(self, sources:set, cibles:set) -> Generator[Foncteur,None,None]:
        for source in sources:
            for cible in cibles:
                dict_apex_cone_source = defaultdict(frozenset)
                for obj in abs(source):
                    dict_apex_cone_source[Cone(obj).apex] |= {obj}
                dict_apex_cone_cible = defaultdict(frozenset)
                for obj in abs(cible):
                    dict_apex_cone_cible[Cone(obj).apex] |= {obj}
                if set(dict_apex_cone_source.keys()) != set(dict_apex_cone_cible.keys()):
                    continue
                
                dict_fleches_k_source = defaultdict(frozenset)
                for fleche in source[source.objets,source.objets]:
                    dict_fleches_k_source[fleche.k] |= {fleche}
                dict_fleches_k_cible = defaultdict(frozenset)
                for fleche in cible[cible.objets,cible.objets]:
                    dict_fleches_k_cible[fleche.k] |= {fleche}
                if set(dict_fleches_k_source.keys()) != set(dict_fleches_k_cible.keys()):
                    continue
                
                bijections_objets = set() # bijections entre cônes de même apex
                bijections_fleches = set()
                cat_bij = CategorieBijections()
                for apex in dict_apex_cone_source:
                    cat_bij |= {dict_apex_cone_source[apex]}
                    cat_bij |= {dict_apex_cone_cible[apex]}
                    bijections = frozenset(cat_bij({dict_apex_cone_source[apex]},{dict_apex_cone_cible[apex]}))
                    bijections_objets |= {bijections}
                    if len(bijections) == 0:
                        break
                if len(bijections) == 0 and len(dict_apex_cone_source.keys()) > 0:
                    continue
                    
                for k in dict_fleches_k_source:
                    cat_bij |= {dict_fleches_k_source[k]}
                    cat_bij |= {dict_fleches_k_cible[k]}
                    bijections = frozenset(cat_bij({dict_fleches_k_source[k]},{dict_fleches_k_cible[k]}))
                    bijections_fleches |= {bijections}
                    if len(bijections) == 0:
                        break
                if len(bijections) == 0 and len(dict_fleches_k_source.keys()) > 0:
                    continue

                for bijection_objet in itertools.product(*bijections_objets):
                    for bijection_fleche in itertools.product(*bijections_fleches):
                        bij_obj = dict([x for bij in bijection_objet for x in bij.as_dict().items()])
                        bij_fleche = dict([x for bij in bijection_fleche for x in bij.as_dict().items()])
                        rate = False
                        for a,b in itertools.product(source.objets,repeat=2):
                            for f in source[{a},{b}]:
                                if bij_fleche[f].source != bij_obj[f.source]:
                                    break
                                if bij_fleche[f].cible != bij_obj[f.cible]:
                                    break
                            else:
                                continue
                            rate = True
                            break
                        if not rate:
                            yield Foncteur(source,cible,bij_obj,bij_fleche)
                    else:
                        continue
                    break
        
def test_FoncteurOubli():
    from GrapheDeComposition import GC,MGC
    from Diagramme import Triangle,DiagrammeIdentite
    
    gc = GC()
    gc |= set("ABCDEF")
    f,g,h,i,j,k,l = [MGC('A','B','f'),MGC('B','C','g'),MGC('D','E','h'),MGC('E','F','i'),MGC('A','D','j'),MGC('B','E','k'),MGC('C','F','l')]
    gc |= {f,g,h,i,j,k,l}
    
    # diag_identite = DiagrammeIdentite(gc)
    # diag_identite.faire_commuter()
    
    d1 = Triangle(gc,"DEF",[h,i,i@h])
    
    c_p = ChampPerceptif(d1)
    
    foncteur_oubli = FoncteurOubli(c_p)
    foncteur_oubli.transformer_graphviz()
    
def test_CategorieProhomologue():
    from GrapheDeComposition import GC,MGC
    from Diagramme import Fleche,DiagrammeIdentite
    
    gc = GC()
    gc |= set("ABCDEFGH")
    f,g,h,i,j,k,l,m,n = [MGC('A','B','f'),MGC('B','C','g'),MGC('D','E','h'),MGC('E','F','i'),MGC('A','D','j'),MGC('B','E','k'),MGC('C','F','l'),MGC('F','G','m'),MGC('F','H','n')]
    gc |= {f,g,h,i,j,k,l,m,n}
    
    diag_identite = DiagrammeIdentite(gc)
    diag_identite.faire_commuter()
    
    d1 = Fleche(gc,m)
    c_p1 = ChampPerceptif(d1)
    d2 = Fleche(gc,n)
    c_p2 = ChampPerceptif(d2)
    c_p1.transformer_graphviz()
    c_p2.transformer_graphviz()
    
    cph = CategorieProhomologue({c_p1,c_p2})
    cph.transformer_graphviz()
    
    f,g = set(cph.isomorphismes(c_p1,c_p2)).pop()
    f.transformer_graphviz()
    g.transformer_graphviz()
    
if __name__ == '__main__':
    test_FoncteurOubli()
    test_CategorieProhomologue()