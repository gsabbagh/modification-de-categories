
class ModelCallable:
    """
        A surcharger
        Permet de s'enregistrer pour recevoir des notifications
        d'une instance Model.
    """
    def informDataModified(self):
        pass

    def informObjectAdded(self, uid, obj:object):
        pass

    def informObjectDeleted(self, uid):
        pass

    def informObjectsAdded(self, uids:list):
        pass

    def informObjectsDeleted(self, uids:list):
        pass


