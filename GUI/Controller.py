import demjson
import pickle

from GUI.Model import Model
from GUI.tk.MainWindow import MainWindow


class Controller:
    _model=None
    def getModel(self): return self._model

    _view=None
    def getView(self): return self._view

    """
        INITIALISATION
    """
    def __init__(self):
        self._model=Model()
        self._view=MainWindow(self)

    """
        IMPORT/EXPORT (JSON/BYTES)
        - use JSON only for basic types (most complex must be iterable)
        - use bytes for all others class (not secured)
    """
    def exportJSON(self, file:str, obj:object):
        assert len(file)>5 and file[-4:]=="json"
        with open(file, "w") as jsonFile:
            jsonFile.write(demjson.encode(obj))

    def importJSON(self, file:str):
        assert len(file)>5 and file[-4:]=="json"

        with open(file, "r") as jsonFile:
            return demjson.decode(jsonFile.read())

    def exportBYTES(self, file:str, obj:object):
        assert len(file)>4 and file[-3:]=="obj"
        with open(file, "wb") as bytesFile:
            pickle.dump(obj,bytesFile)

    def importBYTES(self, file:str):
        assert len(file)>4 and file[-3:]=="obj"
        with open(file, "rb") as bytesFile:
            return pickle.load(bytesFile)