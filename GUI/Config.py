""" Parse the main config file.
    The ConfigParserManager instance associated is shared as Config.config.
"""

from GUI.ConfigParserManager import ConfigParserManager


CONFIG_FILE_DIR='GUI/config.ini'



class Config(ConfigParserManager):

    def __init__(self, file_dir, debug=True, print_file=False, *args, **kwargs):
        super().__init__(file_dir, debug=debug, print_file=print_file, *args, **kwargs)

        confSec=self.getConfigSectionMap("Default")
        self.bg_default= confSec["bg"]


config=Config(CONFIG_FILE_DIR)