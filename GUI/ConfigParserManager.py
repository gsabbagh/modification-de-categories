from configparser import ConfigParser, Error
from re import search
# doc: https://docs.python.org/3/library/re.html

class ConfigParserManager(ConfigParser):

    def __init__(self, file_dir:str, debug=True, print_file=False, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.registered_for_update=[]
        self.file_dir=file_dir
        self.debug=debug
        self.default_update_return:bool=False
        try:
            self.read(self.file_dir)
        except Error as e:
            raise Exception("Error in atempt to read {0} config file. {1}".format(file_dir, e))
        if print_file: self.printConfig()
    
    def getConfigSectionMap(self, section):
        options_dic = {}
        options = self.options(section)
        for option in options:
            try:
                options_dic[option] = self.get(section, option)
                if options_dic[option] == -1:
                    DebugPrint("skip: %s" % option)
            except Exception as e:
                print("Exception on {0}!\n{1}".format(option,e))
                options_dic[option] = None
        return options_dic

    def printConfig(self):
        stars=40
        s=search(r"\\([a-zA-Z0-9\s_\\.\-\(\):])+(.ini){1}$",self.file_dir)
        if s is not None: text=s[0][1:]
        else: text=self.file_dir
        l=int((stars-len(text))//2)
        if l<=0: l=1
        print("*"*l, text, "*"*l)
        for sec in self.sections():
            print(sec)
            for key, value in self.getConfigSectionMap(sec).items():
                print(" "*5, key, " : ", value)
        print("*"*stars, "\n")

    def registerForUpdate(self, obj):
        """ obj must implement
            update(self, section:str, config:str, val:str, robust:bool=False)->bool
            method.
            update must return True if it's accepted, False else.
            None is interpreted like True.
        """
        self.registered_for_update.append(obj)

    def set(self, section:str, config:str, val:str, robust:bool=False)->bool:
        """ @overwrite
            call update(self,... robust:bool=False) methods of registered objects.
            see registerForUpdate(self, obj).
            @param robust: if True update(self,... robust=True) are called and return is True.
            @return True if update is valid, False else.
                    False if one or more obj.update(...) return False.
        """
        if robust:
            for obj in self.registered_for_update:
                obj.update(section=section, config=config, val=val, robust=True)
            return True
        try:
            results=[]
            for obj in self.registered_for_update:
                res=obj.update(section=section, config=config, val=val, robust=False)
                results.append((obj, res))
                if res is not None and res==False:
                    if self.debug:
                        print("WARNING: no update because one registered object blocked it.")
                        for res in results:
                            print("    {res[0]} : {res[1]}")
                    raise Exception("WARNING: no update because one registered object blocked it.")
        except Exception as error:
            return False
        super().set(section, config, val)
        return True

    def save(self):
        if self.debug: print(f"WARNING: in ConfigParserManager {self.file_dir} overwrite...")
        with open(self.file_dir, 'w') as configfile:
            config.write(configfile)


