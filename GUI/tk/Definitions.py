from os import walk, getcwd, path
from re import split

from tkinter import TOP, YES, BOTH, X, \
                    Frame, Toplevel
from tkinter.ttk import Combobox

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
matplotlib.use('TkAgg')
"""
                *INTERPRETATION LATEX AVANCEE*

    Pour interpréter en Latex les définitions, ajouter après les imports:
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.preamble'] = r'\\usepackage{amsmath}'    # ajouter les packages voulus

    ATTENTION:
    Ces imports rendent très lente la compilation!

    De plus, il peut-être nécessaire de supprimer le fichier:
    C:\\Users\\xxx\\.matplotlib\\tex.cache\\540885da22f8d5147f0088a11e43701a.tex.matplotlib-lock
"""

from GUI.tk.Factory import *
from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config

class DefinitionsConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("Definitions")
            self.bg = confSec["bg"]
        else:
            self.bg = "#787878"

def_config=DefinitionsConfig(config)


class Definitions(Toplevel):

    def __init__(self, master=None, **kwargs):
        super().__init__(master, **kwargs)
        self.title("Définitions")
        self.geometry("600x550")
        self.config(bg=def_config.bg)
        self._initFrame()

    def _initFrame(self):
        self._frame=Frame(self)
        self._frame.pack(side=TOP, expand=True, fill=BOTH)
        # self._frame.columnconfigure(0, weight=1, minsize=200)
        # self._frame.rowconfigure(1, minsize=20)
        # self._frame.rowconfigure(1, weight=1, minsize=200)

        # Section
        Factory.buildSectionLabel(self._frame,"Définitions").pack(side=TOP, expand=False, fill=X)#grid(row=0, column=0, sticky="nwe")

        # Combo
        self._files = self.getFilesDir()
        self._combo=Combobox(self._frame,
                            values=list(self._files.keys()),
                            state='readonly')
        self._combo.pack(expand=False, fill=X)#grid(row=1, column=0, sticky="nwe")
        self._combo.bind("<<ComboboxSelected>>", self.setTexShown)

        # Latex
        left, width = 0.05, 0.9
        bottom, height = 0.05, 0.9
        right = left + width
        top = bottom + height

        self.fig = plt.figure()
        self._ax = self.fig.add_axes([0, 0, 1, 1])
        # axes coordinates: (0, 0) is bottom left and (1, 1) is upper right
        self._ax.add_patch(patches.Rectangle(
            (left, bottom), width, height,
            fill=False, transform=self._ax.transAxes, clip_on=False
            ))
        self.canvas = FigureCanvasTkAgg(self.fig, master=self._frame)
        self.canvas.get_tk_widget().pack(side=TOP, expand=True, fill=BOTH)
        self.canvas._tkcanvas.pack(side=TOP, expand=True, fill=BOTH)#grid(row=2, column=0, sticky="nsew")

        # Set the visibility of the Canvas figure
        self._ax.get_xaxis().set_visible(False)
        self._ax.get_yaxis().set_visible(False)


    def setTexShown(self, event):
        # Read .tex
        with open(self._files[self._combo.get()], "r", encoding="utf-8") as fichier:
            text=fichier.read()

        # Print
        left, width = 0.05, 0.9
        bottom, height = 0.05, 0.9
        right = left + width
        top = bottom + height

        self._ax.clear()
        fontsize=10
        # lineWidthInCaracter est calculée à partir de la taille en pixel du canevas
        #                     c'est la taille maximale d'une ligne en caractères
        experimentalCoef=1.3
        lineWidthInCaracter=int(((self.fig.get_size_inches()*self.fig.dpi)[0]*0.9*experimentalCoef)/fontsize)
        self._ax.text(left, top, s=self.formatText(text, lineWidthInCaracter),
                        ha='left',
                        va='top',
                        fontsize = fontsize)
        self.canvas.draw()


    def getFilesDir(self, filter=".tex"):
        dico={}
        for root, dirs, files in walk(getcwd()):
            for file in files:
                if (not filter) or file.endswith(filter):
                    dico[split(r'\\', file)[-1][:-4]]=path.join(root, file)
        return dico

    def formatText(self, text:str, lineWidth=100):
        words=text.split(" ")
        tmp=""
        tmpWidth=0
        for word in words:
            word=word.split("\n")
            flag=False
            for w in word:
                if len(w)>lineWidth:
                    tmp+="\n"+w+"\n"
                    tmpWidth=0
                elif tmpWidth+len(w)>lineWidth:
                    tmp+="\n"+w+" "
                    tmpWidth=len(w)+1
                else:
                    tmp+=w+" "
                    tmpWidth+=len(w)+1
                if flag==True:
                    tmp+="\n"
                    tmpWidth=0
                else:
                    flag=True
        return r"{}".format(tmp)





