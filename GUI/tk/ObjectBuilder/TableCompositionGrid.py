import itertools
import numpy

from tkinter import TOP, X, NO, W, Y, YES, BOTH, RIGHT, VERTICAL, \
                    Frame, Label, Menu

from GUI.tk.InformFrame import InformFrame
from GUI.tk.Factory import *
from GUI.tk.ObjectBuilder.EntryFieldFrame import TableEntry

from Categorie import Categorie



class TableCompositionGrid(Frame):

    def __init__(self, parent, categorie:Categorie):
        """
            @param categorie:Categorie, par exemple un Graphe de composition.
        """
        assert categorie and issubclass(type(categorie), Categorie)
        Frame.__init__(self, parent)
        self._categorie=categorie
        self._initTableComposition()
        self._initFrame()

    def _initTableComposition(self):
        table=[[]]
        tc=self._categorie.table_loi_de_composition()

        # lignes et colones
        G=[]
        F=[]
        for g, fs in tc.items():
            G.append(g)
            F.extend(fs.keys())
        F=list(set(F)) #retire les doublons
        lenF=len(F)
        lenG=len(G)

        # trie
        F.sort()
        G.sort()
        print(F)
        print(G)
        dicF={f:F.index(f) for f in F}
        dicG={g:G.index(g) for g in G}

        # remplissage
        table=numpy.full((lenG+1,lenF+1), "").tolist()
        table[0][0]="g\\f"
        for i,f in enumerate(F):
            table[0][i+1]=f
        for j,g in enumerate(G):
            table[j+1][0]=g

        for g, fs in tc.items():
            for f, label in fs.items():
                table[dicG[g]+1][dicF[f]+1]=label

        # désactivation composées impossibles
        for i,f in enumerate(F):
            for j,g in enumerate(G):
                if f.cible != g.source:
                    table[j+1][i+1]=None
        self.table=table

        # print([str(k)+"===>"+str([str(i) for i in tc[k]]) for k in tc.keys()])


    def _initFrame(self):

        #TODO -----------------------------------------------

        self.columnconfigure(0, weight=1, minsize=100)
        self.columnconfigure(1, minsize=16)
        self.rowconfigure(0, weight=4, minsize=100)
        self.rowconfigure(1, minsize=16)
        self.rowconfigure(2, weight=0)

        self._tableEntry=TableEntry(self, self.table)
        self._tableEntry.grid(row=1, column=0, sticky="nesw")

        # INFORM
        self._informFrame=InformFrame(self)
        self._informFrame.grid(row=2, columnspan = 2, column=0, sticky="nesw")

















