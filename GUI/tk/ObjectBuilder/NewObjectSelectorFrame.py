from tkinter import Frame

from GUI.tk.ObjectBuilder.ComboSelectorFrame import ComboSelectorFrame

from GUI.tk.ObjectBuilder.NewGrapheDeCompositionFrame import NewGrapheDeCompositionFrame
from GUI.tk.ObjectBuilder.NewEnsFinisFrame import NewEnsFinisFrame


class NewObjectSelectorFrame(ComboSelectorFrame):
    """
    Frame objects
    """
    _model=None

    def __init__(self, parent, model, *args, **kwargs):
        assert model and parent
        self._model=model
        ComboSelectorFrame.__init__(self, parent=parent, titre="Nouvel Objet", *args, **kwargs)


    def _initFrame(self):
        # New Object Frames
        self._Frames[""]=Frame(self)
        self._Frames["Graphe de composition"]=NewGrapheDeCompositionFrame(self, self._model)
        self._Frames["Ensembles Finis"]=NewEnsFinisFrame(self, self._model)




























