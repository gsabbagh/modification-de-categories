from tkinter import TOP, BOTTOM, LEFT, RIGHT, X, VERTICAL, END, INSERT, \
                    LabelFrame, Frame, Label, Text, Scrollbar, Button, Spinbox

from GUI.tk.ObjectBuilder.NewObjectFrame import NewObjectFrame
from GUI.tk.ObjectBuilder.EntryFieldFrame import NumberEntryField
from GUI.tk.Factory import *

class NewGrapheDeCompositionAleatoireFrame(NewObjectFrame):
    _nb_flechesEntryField=None
    _nb_tentatives_complexificationEntryField=None
    _nb_tentatives_correction_compositionEntryField=None
    _proba_identifier_identitesEntryField=None

    def __init__(self, parent, model, *args, **kwargs):
        assert model and parent
        NewObjectFrame.__init__(self, parent, model, *args, **kwargs, pady=10)

    # @Overwrite
    def _initFrame(self):
        self.rowconfigure(1, weight=1, minsize=20)
        self.rowconfigure(2, weight=1, minsize=20)
        self.rowconfigure(3, weight=1, minsize=20)

        Factory.buildSubSectionLabel(self,"Génération aléatoire").grid(row=0, column=0, columnspan = 2, sticky="ew")
        self._validateButton.grid(row=1, column=0, columnspan=2, sticky="ew")

        self._nb_flechesEntryField=NumberEntryField(self, "Nbr de fleches", min=0, max=100, default="")
        self._nb_flechesEntryField.grid(row=2, column=0, columnspan=2, sticky="ew")

        self._nb_tentatives_complexificationEntryField=NumberEntryField(self, "Nbr complexifications", min=0, max=100, default="")
        self._nb_tentatives_complexificationEntryField.grid(row=3, column=0, columnspan=2, sticky="ew")

        self._nb_tentatives_correction_compositionEntryField=NumberEntryField(self, "Nbr corrections", min=0, max=100, default="")
        self._nb_tentatives_correction_compositionEntryField.grid(row=4, column=0, columnspan=2, sticky="ew")

        self._proba_identifier_identitesEntryField=NumberEntryField(self, "Proba ", min=0, max=1, default=str(0.2))
        self._proba_identifier_identitesEntryField.grid(row=5, column=0, columnspan=2, sticky="ew")

    # @Overwrite
    def _generate(self):
        self._model.createGrapheDeCompositionAleatoire(nb_fleches=self._nb_flechesEntryField.getText(),
                                            nb_tentatives_complexification=self._nb_tentatives_complexificationEntryField.getText(),
                                            nb_tentatives_correction_composition=self._nb_tentatives_correction_compositionEntryField.getText(),
                                            proba_identifier_identites=self._proba_identifier_identitesEntryField.getText())

























