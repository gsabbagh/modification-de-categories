from tkinter import TOP, BOTTOM, LEFT, RIGHT, X, VERTICAL, END, INSERT, \
                    LabelFrame, Frame, Label, Text, Scrollbar, Button, Spinbox

from GUI.tk.ObjectBuilder.NewObjectFrame import NewObjectFrame
from GUI.tk.ObjectBuilder.EntryFieldFrame import ObjectEntryField, EnsembleEntryField
from GUI.tk.Factory import *


class NewEnsFinisFrame(NewObjectFrame):
    _ensembleObjects=None
    _ensembleParties=None

    def __init__(self, parent, model, *args, **kwargs):
        assert model and parent
        NewObjectFrame.__init__(self, parent, model, *args, **kwargs)
        self.parent=parent

    # @Overwrite
    def _initFrame(self):
        self.rowconfigure(1, weight=1, minsize=20)
        self.rowconfigure(2, weight=1, minsize=20)

        # Objects
        self._ensembleObjects=EnsembleEntryField(self)
        self._ensembleObjects.grid(row=1, column=0)

        # Ensemble des parties
        self._ensembleParties=NewEnsPartiesFrame(self, self._model)
        self._ensembleParties.grid(row=2, column=0)


    # @Overwrite
    def _generate(self):
        self._model.createEnsFinisFromSet(ensembles=self._ensembleObjects.getSet(), nom = None)

class NewEnsPartiesFrame(NewObjectFrame):
    _objectsEntryField=None

    def __init__(self, parent, model, *args, **kwargs):
        assert model and parent
        NewObjectFrame.__init__(self, parent, model, *args, **kwargs, pady=10)

    # @Overwrite
    def _initFrame(self):
        self.rowconfigure(1, weight=1, minsize=20)
        self.rowconfigure(2, weight=1, minsize=20)

        # Subsection
        Factory.buildSubSectionLabel(self,"Ensemble des parties").grid(row=0, column=0, columnspan = 2, sticky="ew")

        # Button
        self._validateButton.grid(row=1, column=0, columnspan=2, sticky="ew")

        # Objects
        self._objectsEntryField=ObjectEntryField(self)
        self._objectsEntryField.grid(row=2, column=0)



    # @Overwrite
    def _generate(self):
        ensemble=self._objectsEntryField.getSet()
        if ensemble:
            self._model.createEnsPartiesFromSet(ensemble=ensemble, nom = None)
        else:
            print("Error: pas d'ensemble défini.'")






















