from re import split, findall
from tkinter import TOP, BOTTOM, LEFT, RIGHT, X, VERTICAL, END, INSERT, \
                    LabelFrame, Frame, Label, Text, Scrollbar, Button, Spinbox

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config

class NewObjectFrameConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("NewObjectFrame")
            self.button_color = confSec["button_color"]
        else:
            self.button_color = "#FFFFFF"

nof_config=NewObjectFrameConfig(config)

class NewObjectFrame(Frame):
    """
        Classe abstraite
        Frame de construction d'un objet
        La méthode _initFrame doit être implémentée par les classes filles
    """
    _model=None

    def __init__(self, parent, model, *args, **kwargs):
        assert model and parent
        Frame .__init__(self, parent, *args, **kwargs)

        self._model=model

        self.columnconfigure(0, weight=1, minsize =30)
        self.rowconfigure(0, weight=1, minsize=20)

        self._validateButton=Button(self, text="Générer", bg=nof_config.button_color, command=self._generate)
        self._validateButton.grid(row=0, column=0, columnspan=2, sticky="ew")
        self._initFrame()

    def _initFrame(self):
        raise NotImplementedError

    def _generate(self):
        raise NotImplementedError






















