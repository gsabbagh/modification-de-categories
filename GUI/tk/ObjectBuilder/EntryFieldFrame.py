from re import split, findall, search
from itertools import combinations

from tkinter import TOP, BOTTOM, LEFT, RIGHT, X, HORIZONTAL, VERTICAL, END, INSERT, \
                    LabelFrame, Frame, Label, Text, Entry, Scrollbar, Button, Spinbox, Checkbutton, BooleanVar, StringVar , IntVar, \
                    TclError

from tkinter.ttk import Combobox

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config

class Config(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("EntryFieldFrame")
            self.frame_bd = int(confSec["frame_bd"])
        else:
            self.frame_bd = 2

_config=Config(config)


"""
            ***Héritages***
LabelFrame
    - NumberEntryField
    - EntryField
        -- ObjectEntryField
        -- EnsembleEntryField
        -- MorphismeEntryField
        -- IdentificationEntryField
Frame
    - SeedEntryField
    - IncrementalEntryWithHistory
Historique
"""

class NumberEntryField(LabelFrame):

    def __init__(self, parent, name:str, min:int, max:int, default:str, description:str=None, horizontal=False):
        LabelFrame .__init__(self, parent,
                                bd=_config.frame_bd,
                                text=name)
        self._description=description
        self._horizontal=horizontal
        self._initFrame(min, max, default)

    def _initFrame(self, min, max, default):
        if self._horizontal:
            self.columnconfigure(0, weight=1, minsize=20)
            self.columnconfigure(1, minsize=16)
            self.rowconfigure(1, weight=1, minsize=20)
        else:
            self.columnconfigure(0, weight=1, minsize=20)
            self.rowconfigure(0, weight=0, minsize=20)
            self.rowconfigure(1, weight=1, minsize=20)

        if self._description:
            Label(self, text=self._description, anchor="w").grid(row=0, column=0, sticky="new")

        self._numberBox=Spinbox(self, from_=min, to=max)
        if self._horizontal:
            self._numberBox.grid(row=0, column=1, sticky="new")
        else:
            self._numberBox.grid(row=1, column=0, sticky="new")
        self._numberBox.delete('0', END)
        self._numberBox.insert(INSERT,str(default))

    def getText(self):
        return self._numberBox.get()


class EntryField(LabelFrame):
    """
        Classes filles:
        - ObjectEntryField
        - EnsembleEntryField
        - MorphismeEntryField
        - IdentificationEntryField
    """
    _TEXT_MAX_SIZE=100
    # _text=None
    # _scrollbarY=None
    # _description=None

    def __init__(self, parent, name:str, description:str=None, horizontal=False, *args, **kwargs):
        LabelFrame .__init__(self, parent,
                                bd=_config.frame_bd,
                                text=name,
                                *args, **kwargs)
        self._description=description
        self._horizontal=horizontal
        self._initFrame()

    def _initFrame(self):
        if self._horizontal:
            self.columnconfigure(0, weight=1, minsize=20)
            self.columnconfigure(1, minsize=16)
            self.columnconfigure(2, minsize=16)
            self.rowconfigure(1, weight=1, minsize=20)
        else:
            self.columnconfigure(0, weight=1, minsize=20)
            self.columnconfigure(1, minsize=16)
            self.rowconfigure(0, weight=0, minsize=20)
            self.rowconfigure(1, weight=1, minsize=20)

        if self._description:
            Label(self, text=self._description, anchor="w").grid(row=0, column=0, sticky="new")

        self._text=Text(self, height=3, width=20)
        if self._horizontal:
            self._text.grid(row=0, column=1, sticky="new")
        else:
            self._text.grid(row=1, column=0, sticky="nw")


        self._scrollbarY=Scrollbar(self, orient =VERTICAL)
        if self._horizontal:
            self._scrollbarY.grid(row=0, column=2, sticky="nsw")
        else:
            self._scrollbarY.grid(row=1, column=1, sticky="nsw")

        self._text.configure(yscrollcommand = self._scrollbarY.set)
        self._scrollbarY.config(command=self._text.yview)

    def getText(self):
        return self._text.get("1.0",'end-1c')

class ObjectEntryField(EntryField):
    # _genererSpinbox=None
    # _genererButton=None

    def __init__(self, parent, description="ex: 1,2,3...", generateur=True, *args, **kwargs):
        EntryField.__init__(self, parent, name="Objets", description=description, *args, **kwargs)
        if generateur:
            self.add_generateur()

    def getList(self):
        return [x for x in split('[,\n]', self.getText()) if x]

    def getSet(self):
        return {e for e in self.getList()}

    def add_generateur(self, min:int=1, max:int=150, default:int=10):
        self._genererSpinbox=Spinbox(self, from_=min, to=max)
        self._genererSpinbox.grid(row=2, column=0, sticky="nw")
        self._genererSpinbox.delete('0', END)
        self._genererSpinbox.insert(INSERT,str(default))

        def generate():
            i=int(self._genererSpinbox.get())
            if i >=min and i<=max:
                txt=""
                for point in range(min,i+1):
                    txt+=str(point)+','
                txt=txt[:-1]
                self._text.delete('1.0', END)
                self._text.insert(INSERT,txt)
            else:
                print(f"Erreur: {i} n'appartient pas à [{min},{max}].")

        self._genererButton=Button(self, text="Ajouter", command=generate)
        self._genererButton.grid(row=2, column=1, sticky="nw")

class EnsembleEntryField(EntryField):
    # _genererSpinbox=None
    # _genererButton=None

    def __init__(self, parent, *args, **kwargs):
        EntryField.__init__(self, parent, name="Ensembles", description="ex: {},{1,2}...", *args, **kwargs)
        self.add_generateur()

    def getList(self):
        # {...} (les ensembles ne peuvent pas contenir d'ensembles)
        def elagSet(s:str):
            s=s[1:-1]
            if len(s)>0 and s[-1]==',':
                s=s[:-1]
            return s
        return [elagSet(x) for x in findall("\{[^\{\}]*\}", self.getText())]# if x

    def getSet(self):
        return {frozenset({x for x in split('[,\n]', e) if x}) for e in self.getList()}

    def add_generateur(self, min:int=1, max:int=100, default:int=3):
        self._genererSpinbox=Spinbox(self, from_=min, to=max)
        self._genererSpinbox.grid(row=2, column=0, sticky="nw")
        self._genererSpinbox.delete('0', END)
        self._genererSpinbox.insert(INSERT,str(default))

        def generate():
            i=int(self._genererSpinbox.get())
            if i >=min and i<=max:
                txt=""
                ensemble={e for e in range(1,i+1)}
                parties={ens for e in range(i+1) for ens in combinations(ensemble,e)}
                for p in parties:
                    txt+="{"+str(p)[1:-1]+'},'
                txt=txt[:-1]
                self._text.delete('1.0', END)
                self._text.insert(INSERT,txt)
            else:
                print(f"Erreur: {i} n'appartient pas à [{min},{max}].")

        self._genererButton=Button(self, text="Ajouter", command=generate)
        self._genererButton.grid(row=2, column=1, sticky="nw")

class MorphismeEntryField(EntryField):

    def __init__(self, parent, *args, **kwargs):
        EntryField .__init__(self, parent, name="Morphismes", description="ex: (1,2,f)(2,3,g)...", *args, **kwargs)

    def getList(self):
        # (...)
        tuples=[x[1:-1] for x in findall("\([^\(\)]+\)", self.getText()) if x]
        # ,\n
        tuplesSplited=[[x for x in split('[,\n]', t) if x] for t in tuples]
        # 3 éléments
        morphismesDescription=[x for x in tuplesSplited if len(x)==3]
        # Création des morphismes
        return morphismesDescription

class IdentificationEntryField(EntryField):

    def __init__(self, parent, *args, **kwargs):
        EntryField .__init__(self, parent, name="Identifications", description="ex: (g@f,h)(h@h,h)...", *args, **kwargs)

    def getList(self):
        # (...)
        tuples=[x[1:-1] for x in findall("\([^\(\)]+\)", self.getText()) if x]
        # ,\n
        tuplesSplited=[[x for x in split('[,\n]', t) if x] for t in tuples]
        # 2 éléments
        identifications=[[split('[@]', x[0]),split('[@]', x[1])] for x in tuplesSplited if len(x)==2]
        # Création des identifications
        return identifications



class SeedEntryField(Frame):

    def __init__(self, parent):
        Frame.__init__(self, parent, bd=_config.frame_bd)
        self._hist=Historique(10)
        self._initFrame()

    def _initFrame(self):
        self.rowconfigure(0, weight=1, minsize=20)
        self.columnconfigure(0, weight=0, minsize=40)
        self.columnconfigure(1, weight=1, minsize=20)
        self.columnconfigure(2, weight=0, minsize=30)
        self.columnconfigure(3, weight=1, minsize=20)

        # check Use seed ?
        self._chkValue = BooleanVar()
        self._chkValue.set(False)
        self._checkbutton=Checkbutton(self, text="Use Seed", var=self._chkValue, command=self.chkValueChanged)

        # spinbox
        self._seed=IntVar()
        self._seedBox=Spinbox(self, from_=0, to=1000000, textvariable=self._seed)
        # historique
        self._labelHist=Label(self, text="Historique: ", anchor="e")
        self._stringVar= StringVar()
        self._combo=Combobox(self,
                            textvariable=self._stringVar,
                            values=[e for e in self._hist.get() if e],
                            state='readonly')
        self._combo.bind("<<ComboboxSelected>>", self.comboSelectSeed)

        #init
        self.gridFalseFrame()




    def gridFalseFrame(self):
        self._checkbutton.grid(row=0, column=0, sticky="ns")
        self._labelHist.grid(row=0, column=2, sticky="ns")
        self._combo.grid(row=0, column=3, sticky="nsew")
        self._seedBox.grid_forget()


    def gridTrueFrame(self):
        self._seedBox.grid(row=0, column=1, sticky="nsew")


    def chkValueChanged(self):
        if self._chkValue.get():
            self.gridTrueFrame()
        else:
            self.gridFalseFrame()


    # COMBO SEED
    def comboSelectSeed(self, event):
        val=self._stringVar.get()
        if val=="":
            return
        try:
            self._seed.set(int(val))
            self._seedBox.delete('0', END)
            self._seedBox.insert(INSERT,val)
        except ValueError:
            return

    def getSeed(self):
        if self._chkValue.get():
            return self._seed.get()
        else:
            return None

    def pushToHistory(self, seed):
        self._hist.pushIfDifferent(seed)
        self._combo['values']=[e for e in self._hist.get() if e]




class IncrementalEntryWithHistory(Frame):

    def __init__(self, parent, alphabetical:bool=False):
        Frame.__init__(self, parent, bd=_config.frame_bd)
        self._alphabetical=alphabetical
        self.hist=Historique(10)
        self._initFrame()
        self.initIncrementation()

    def initIncrementation(self):
        if self._alphabetical:
            entry="a"
        else:
            entry="0"
        self._text.delete('1.0', END)
        self._text.insert(INSERT, entry)
        self._lastText=entry


    def incrementEntry(self):
        def incAlpha(char):
            toInc=ord(char)
            if toInc==122:
                return 'a'
            else:
                return chr(toInc+1)

        def incAlphaText(text):
            if not text or text=="":
                return 'a'
            endInced=incAlpha(text[-1])
            if endInced=='a':
                return incAlphaText(text[:-1])+endInced
            else:
                return text[:-1]+endInced

        if self._alphabetical:
            # on prend la chaîne alphanumérique à la fin
            toInc=search("[a-z]+\s*$", self._lastText)
            if toInc:
                size=len(toInc[0])
                inc=incAlphaText(toInc[0])
                entry=self._lastText[:-size]+inc
            else:
                entry=self._lastText+"a"
        else:
            # on prend le nombre à la fin
            toInc=search("\d+\s*$", self._lastText)
            if toInc:
                size=len(toInc[0])
                toInc=int(toInc[0])
                inc=toInc+1
                entry=self._lastText[:-size]+str(inc)
            else:
                entry=self._lastText+"0"
        self._text.delete('1.0', END)
        self._text.insert(INSERT, entry)

    def _initFrame(self):
        self.rowconfigure(0, weight=1, minsize=20)
        self.rowconfigure(1, minsize=20)
        self.columnconfigure(0, weight=0, minsize=10)
        self.columnconfigure(1, weight=1, minsize=10)

        # text
        self._text=Text(self, height=1, width=10)
        self._text.grid(row=0, column=0, sticky="new")

        # scrollbar
        self._scrollbarX=Scrollbar(self, orient=HORIZONTAL)
        self._scrollbarX.grid(row=1, column=0, sticky="new")

        self._text.configure(xscrollcommand = self._scrollbarX.set)
        self._scrollbarX.config(command=self._text.xview)

        # history
        self._stringVar= StringVar()
        self._combo=Combobox(self,
                            textvariable=self._stringVar,
                            values=[e for e in self.hist.get() if e],
                            state='readonly')
        self._combo.bind("<<ComboboxSelected>>", self.comboSelectText)
        self._combo.grid(row=0, column=1, sticky="new")

    def comboSelectText(self, event):
        val=self._stringVar.get()
        if val:
            self._text.delete("1.0", END)
            self._text.insert(INSERT,val)

    def pushToHistory(self, val):
        self.hist.pushIfDifferent(val)
        self._combo['values']=[e for e in self.hist.get() if e]

    def getText(self):
        self._lastText=self._text.get("1.0",'end-1c')
        self.incrementEntry()
        return self._lastText



class Historique:

    def __init__(self, taille=10):
        self.taille=taille
        self.history=[]
        self.pile=[None for k in range(self.taille)]
        self.curseur=0

    def incrementCursor(self):
        self.curseur+=1
        if self.curseur==self.taille:
            self.curseur=0

    def push(self, obj):
        self.pile[self.curseur]=obj
        self.history.append(obj)
        self.incrementCursor()

    def pushIfDifferent(self, obj):
        if not obj==self.pile[self.curseur-1]:
            self.push(obj)
        else:
            self.history.append(obj)

    def get(self):
        return self.pile[:self.curseur][::-1]+self.pile[self.curseur:][::-1]





class TableEntry(Frame):
    def __init__(self, parent, table=None, pad=10, disableFirst=True):
        Frame.__init__(self, parent, bd=_config.frame_bd)
        self.pad=pad
        self._disableFirst=disableFirst
        self.rowconfigure(0, pad=self.pad)
        self.columnconfigure(0, pad=self.pad)
        if table:
            self.setTable(table)

    def getTableRectSize(self, table):
        l=len(table)
        c=max([len(ligne) for ligne in table])
        return l,c

    def setTable(self, table):
        l,c=self.getTableRectSize(table)
        self.entries=[]
        for i in range(l):
            self.entries.append([])
            for j in range(c):
                e=Entry(self, width=10, fg='blue')
                e.grid(row=i, column=j)
                if not table[i][j]:
                    e["state"]="disable"
                else:
                    e.insert(INSERT, table[i][j])
                self.entries[i].append(e)
        if self._disableFirst:
            self._setFirstLigneColumn()

    def _setFirstLigneColumn(self):
        assert self.entries
        for i in range(len(self.entries)):
            e=self.entries[i][0]
            e["state"]="disable"
            e["fg"]="red"
        for j in range(len(self.entries[0])):
            e=self.entries[0][j]
            e["state"]="disable"
            e["fg"]="red"














