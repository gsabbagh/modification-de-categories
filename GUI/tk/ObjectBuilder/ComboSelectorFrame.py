from tkinter import TOP, YES, BOTH, X, \
                    LabelFrame, StringVar
from tkinter.ttk import Combobox

class ComboSelectorFrame(LabelFrame):

    def __init__(self, parent, titre, *args, **kwargs):
        LabelFrame.__init__(self, parent,
                                text=titre,
                                *args, **kwargs)
        self._initCombo()

    def _initCombo(self):
        self._Frames={}
        self._frame=None
        self._initFrame()

        # Combo
        self._stringVar= StringVar()
        self._combo=Combobox(self,
                            textvariable=self._stringVar,
                            values=list(self._Frames.keys()),
                            state='readonly')
        self._combo.current(0)
        self.selectNewObjectFrame(None)
        self._combo.pack(side = TOP, fill=X)
        self._combo.bind("<<ComboboxSelected>>", self.selectNewObjectFrame)

    def selectNewObjectFrame(self, event):
        if self._frame:
            self._frame.forget()
        self._frame=self._Frames[self._combo.get()]
        self._frame.pack(side = TOP, fill=X)

    def _initFrame(self):
        raise NotImplementedError



























