from tkinter import TOP, BOTTOM, LEFT, RIGHT, X, VERTICAL, END, INSERT, \
                    LabelFrame, Frame, Label, Text, Scrollbar, Button, Spinbox

from GUI.tk.ObjectBuilder.NewObjectFrame import NewObjectFrame
from GUI.tk.ObjectBuilder.EntryFieldFrame import ObjectEntryField, MorphismeEntryField, IdentificationEntryField
from GUI.tk.ObjectBuilder.NewGrapheDeCompositionAleatoireFrame import NewGrapheDeCompositionAleatoireFrame


class NewGrapheDeCompositionFrame(NewObjectFrame):
    _textObjects=None
    _textMorphismes=None
    _textIdentifications=None

    def __init__(self, parent, model, *args, **kwargs):
        assert model and parent
        NewObjectFrame.__init__(self, parent, model, *args, **kwargs)

    # @Overwrite
    def _initFrame(self):
        self.rowconfigure(1, weight=1, minsize=20)
        self.rowconfigure(2, weight=1, minsize=20)
        self.rowconfigure(3, weight=1, minsize=20)
        self.rowconfigure(4, weight=1, minsize=20)

        # Objects
        self._textObjects=ObjectEntryField(self)
        self._textObjects.grid(row=1, column=0)

        # Morphismes
        self._textMorphismes=MorphismeEntryField(self)
        self._textMorphismes.grid(row=2, column=0)

        # Identifications
        self._textIdentifications=IdentificationEntryField(self)
        self._textIdentifications.grid(row=3, column=0)

        # Graphe De Composition Aleatoire
        self._aleatoireFrame=NewGrapheDeCompositionAleatoireFrame(self, self._model)
        self._aleatoireFrame.grid(row=4, column=0, sticky="sew")

    # @Overwrite
    def _generate(self):
        self._model.createGrapheDeComposition(objets=self._textObjects.getList(),
                                                morphismes=self._textMorphismes.getList(),
                                                identifications=self._textIdentifications.getList(),
                                                nom = None)

























