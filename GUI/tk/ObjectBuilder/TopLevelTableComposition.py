from tkinter import TOP, X, NO, E, Y, YES, BOTH, RIGHT, VERTICAL, \
                    Toplevel, Frame, Button

from GUI.tk.ObjectBuilder.TableCompositionGrid import TableCompositionGrid

from GrapheDeComposition import GrapheDeComposition

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config

class TableCompositionConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("TableComposition")
            self.bg = confSec["bg"]
        else:
            self.bg = "#787878"

tc_config=TableCompositionConfig(config)

class TopLevelTableComposition(Toplevel):

    def __init__(self, gc:GrapheDeComposition ,master=None, *args, **kwargs):
        Toplevel.__init__(self, master, **kwargs)
        self.minsize(100, 200)
        self.title("Modifier la table de composition")
        self.config(bg=tc_config.bg)
        self._gc=gc
        self._initFrame()

    def _initFrame(self):
        self._frame=Frame(self)
        self._frame.pack(expand=True, fill=BOTH)

        self._frame.rowconfigure(0, weight=1, minsize=100)
        self._frame.rowconfigure(1, weight=0, minsize=22)

        self._tableComposition=TableCompositionGrid(self._frame, self._gc)
        self._tableComposition.grid(row=0, column=0, sticky="nsew")

        self._askFrame=Frame(self._frame)
        self._askFrame.grid(row=1, column=0, sticky="new")

        Button(self._askFrame, text="Valider", command=self._valider).pack(padx=5,pady=3,side=RIGHT)
        Button(self._askFrame, text="Annuler", command=self._annuler).pack(padx=5,pady=3,side=RIGHT)


    def _valider(self):
        pass

    def _annuler(self):
        self.destroy()











