"""
    Définition de l'espace de droite
"""
from pandas import DataFrame
from itertools import product
from tkinter import TOP, X, NO, W, Y, YES, BOTH, RIGHT, VERTICAL, \
                    Frame, Label, Menu
from tkinter.ttk import Treeview, Style, Scrollbar

from GUI.ModelCallable import ModelCallable
from GUI.ModelObjectsBuilder import Description

from GUI.tk.InformFrame import InformFrame
from GUI.tk.Factory import *
from GUI.tk.ObjetsFrameCallable import ObjetsFrameCallable

from GUI.plot.GraphGenerator import GraphGenerator

from GrapheDeComposition import GrapheDeComposition


class TableCompositionGrid(Frame):

    def __init__(self, parent, gc:GrapheDeComposition, informLastClic:bool=False):
        """
            @param gc:GrapheDeComposition, graphe muni d'une loi de composition et d'identités.
            @param informLastClic:bool, si True: affiche la dernière composition modifiée.
        """
        assert gc and issubclass(type(gc), GrapheDeComposition)
        Frame.__init__(self, parent)
        self._gc=gc
        self._informLastClic=informLastClic
        self._initTableComposition()
        #self._initFrame()

    def _initTableComposition(self, complet=False, afficher_identites=True, limite_fleches=100):
        fleches_elem = set(self._gc[self._gc.objets,self._gc.objets])
        func = lambda x,y : self._gc({x},{y})
        fleches_elementaires=[]
        fleches_non_elementaires=[]

        for source,cible in product(self._gc.objets,repeat=2):
            nb_fleches = 0
            for fleche in func(source,cible):
                if afficher_identites or not fleche.is_identite:
                    if fleche.source not in self._gc.objets:
                        raise Exception("Source d'une fleche pas dans les objets de la categorie.")
                    if fleche.cible not in self._gc.objets:
                        raise Exception("Source d'une fleche pas dans les objets de la categorie.")
                    if len({obj for obj in self._gc.objets if obj == fleche.source}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est source de la fleche.")
                    if len({obj for obj in self._gc.objets if obj == fleche.cible}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est cible de la fleche.")
                    #permet d'avoir toujours un objet de la catégorie comme source
                    source = {obj for obj in self._gc.objets if obj == fleche.source}.pop()
                    #permet d'avoir toujours un objet de la catégorie comme cible
                    cible = {obj for obj in self._gc.objets if obj == fleche.cible}.pop()
                    # descriptions
                    if fleche in fleches_elem:
                        fleches_elementaires.append(fleche)
                    else:
                        fleches_non_elementaires.append(fleche)
                    nb_fleches += 1
                if nb_fleches > limite_fleches:
                    if WARNING_LIMITE_FLECHES_ATTEINTE:
                        print("Warning : limite fleches entre "+str(source)+" et "+str(cible)+" atteinte.")
                        break

        print("---- élémentaires: ")
        for f in fleches_elementaires:
            print(f"{f.source}->{f.cible} : {f}")
        print("---- non élémentaires: ")
        for f in fleches_non_elementaires:
            print(f"{f.source}->{f.cible} : {f} ")
            for g in f:
                print(f"     {g.source}->{g.cible} : {g}")

    def _initFrame(self):
        self.columnconfigure(0, weight=1, minsize=100)
        self.columnconfigure(1, minsize=16)
        self.rowconfigure(0, weight=4, minsize=100)
        self.rowconfigure(1, minsize=16)
        self.rowconfigure(2, weight=0)

        # TREE
        self._tree=Treeview(self, selectmode='extended')
        self._tree.grid(row=1, column=0, sticky="nesw")
        style = Style()
        style.theme_use("default")
        style.map("Treeview")

        self._scrollbarY=Scrollbar(self, orient =VERTICAL)
        self._scrollbarY.grid(row=1, column=1, sticky="nsw")

        self._tree.configure(yscrollcommand = self._scrollbarY.set)
        self._scrollbarY.config(command=self._tree.yview)

        self._tree["columns"]=("class","description")
        self._tree.column("#0", width=100, minwidth=15, stretch=False)
        self._tree.column("class", width=15, stretch=False)
        self._tree.column("description", width=200, minwidth=80, stretch=True)

        self._tree.heading("#0",text="UID",anchor=W)
        self._tree.heading("class", text="Classe",anchor=W)
        self._tree.heading("description", text="Description",anchor=W)

        self._tree.bind("<<TreeviewSelect>>", self.objectSelection)
        self._tree.bind("<Delete>", self.objectSelection)

        # popup tree menu
        self._popupMenu = Menu(self._tree, tearoff=0)

        self._popupMenu.add_command(label="Tableau de composition",
                                    command=self.tableDeComposition)
        self._popupMenu.add_command(label="NetworkX",
                                    command=self.toNetworkX)
        self._popupMenu.add_command(label="Graphviz",
                                    command=self.toGraphviz)

        self._tree.bind("<Button-3>", self.popup) # Button-2 on Aqua

        # INFORM
        self._informFrame=InformFrame(self)
        self._informFrame.grid(row=2, columnspan = 2, column=0, sticky="nesw")

    _selectionRegistered=[]
    def registerSelection(self, ofCallable:ObjetsFrameCallable):
        self._selectionRegistered.append(ofCallable)

    def callRegisteredSelection(self, uid, obj:object):
        for registered in self._selectionRegistered:
            registered.informObjectSelected(uid, obj)

    """
        GETTERS
    """
    def getIfExist(self, d:dict, cle:object):
        if cle in d:
            return d[cle]
        else:
            return None

    def getFolder(self, class_name:str):
        return self.getIfExist(self._folders, class_name)

    def getSelectedObjects(self)->list:
        curItems = self._tree.selection()
        sel=[]
        for i in curItems:
            if self._tree.parent(i):
                sel.append(i)
        return sel

    """
        INFORM FROM MODEL
    """
    def informDataModified(self):
        self.actualizeAllObjects(self._controller.getModel().getData())

    def informObjectAdded(self, uid, obj:object):
        self.addObject(uid, obj)

    def informObjectDeleted(self, uid):
        print(uid)

    def informObjectsAdded(self, uids:list):
        print(uids)

    def informObjectsDeleted(self, uids:list):
        print(uids)

    """
        OPERATIONS ON TREE
    """
    def actualizeAllObjects(self, df:DataFrame):
        for c in self._tree.get_children(): self._tree.delete(c)
        folders=df['class'].unique()
        self._folders={}
        for folder in folders:
            f=self._tree.insert(parent="", index="end", text=folder)
            self._folders[folder]=f
            objects=(df['object'][df['class']==folder]).sort_index(ascending=True)
            for uid, obj in objects.items():
                self._tree.insert(parent=f, iid=uid, index="end", text=uid, values=(folder, Description(obj)))

    def addObject(self, uid, obj:object):
        class_name=type(obj).__name__
        f=self.getFolder(class_name)
        if not f:
            f=self._tree.insert(parent="", index="end", text=class_name)
            self._folders[class_name]=f
        self._tree.insert(parent=f, iid=uid, index="end", text=uid, values=(class_name, Description(obj)))

    def deleteObject(self, uid):
        self._tree.delete(self._tree.item(uid))


    """
        BIND FUNCTIONS/MENU FUNCTIONS
    """
    def objectSelection(self, event):
        curItems = self.getSelectedObjects()
        text=""
        for i in curItems:
            item=self._tree.item(i)
            text+= (str(item['text'])+" : <"+
                    str(item['values'][0])+"> "+
                    str(item['values'][1])+"\n"
                    )
        self._informFrame.setText(text)
        if curItems:
            uid=int(curItems[0])
            self.callRegisteredSelection(uid, self._controller.getModel().getObject(uid))

    def objectDelete(self, event):
        pass

    def popup(self, event):
        try:
            iid = self._tree.identify_row(event.y)
            if iid:
                if self._tree.parent(iid):
                    self._popupObjectIid=int(iid)
                    obj=self._controller.getModel().getObject(self._popupObjectIid)['object']
                    print(type(obj))
                    if not issubclass(type(obj), GrapheDeComposition):
                        self._popupMenu.entryconfig("Tableau de composition", state="disabled")
                self._popupMenu.tk_popup(event.x_root, event.y_root, 0)
            else:
                pass
        finally:
            self._popupMenu.grab_release()

    def tableDeComposition(self):
        TopLevelTableComposition()


    def toNetworkX(self):
        if self._popupObjectIid is not None:
            self._mainWindow.getPlotFrame().setGraph(GraphGenerator.give(self._controller.getModel().getObject(self._popupObjectIid)['object']))
            self._popupObjectIid=None

    def toGraphviz(self):
        if self._popupObjectIid is not None:
            self._mainWindow.getEI().exportGraphviz([self._popupObjectIid])
            self._popupObjectIid=None









