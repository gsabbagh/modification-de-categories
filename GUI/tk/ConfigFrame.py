from tkinter import Frame, Scrollbar, Entry, \
                    END, YES, BOTH, RIGHT, LEFT, Y, CENTER, VERTICAL
from tkinter.ttk import Treeview, Style, Notebook

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager

class ConfigFrame(Frame):

    def __init__(self, config_parser , master=None, sections:list=None, cnf={}, **kw):
        """@param config_parser: the one to show in frame.
           @param gui_config_parser: the one with [ConfigParser] section.
        """
        super().__init__(master=master, cnf=cnf, **kw)
        self.name="ConfigFrame"
        self.config_parser=config_parser
        self.sections_selected=sections
        self._initFrame()
        
    def _initFrame(self):
        self.style = Style(self)
        aktualTheme = self.style.theme_use()
        self.style.theme_create("dummy", parent=aktualTheme)
        self.style.theme_use("dummy")

        self.entryPopup=None
        self.tree=self._createTree()
        self.tree.pack(side=LEFT, expand=True, fill=BOTH)

        self.sb = Scrollbar(self, orient=VERTICAL)
        self.sb.pack(side=RIGHT, fill=Y)

        self.tree.config(yscrollcommand=self.sb.set)
        self.sb.config(command=self.tree.yview)

    def _createTree(self):
        tree=Treeview(self, show='headings', height=8, selectmode='browse')
        tree['columns']=('Attribute', 'Value')
        tree.column('#0', width=0, stretch=False)
        tree.column('Attribute', anchor="w", width=200, stretch=False)
        tree.column('Value', anchor="w")
        tree.heading('#0', text='')
        tree.heading('Attribute', text='Attribute', anchor=CENTER)
        tree.heading('Value', text='Value', anchor=CENTER)
        
        sections=self.config_parser.sections()
        if self.sections_selected is not None:
            sections=[value for value in self.sections_selected if value in set(self.config_parser.sections())]
            if not sections or len(sections)<1:
                sections=self.config_parser.sections()
                print("Warning: in {self.name}. Bad sections selection; ignore it.")
        for iid, sec in enumerate(sections):
            tree.insert(parent="", index="end", iid=iid, values=(sec,""),
                        open=True,
                        tags=("Section"))
            cs=self.config_parser.getConfigSectionMap(sec)
            for it in cs.items():
                tree.insert(parent=iid, index="end", values=it,
                            tags=("Config"))
        tree.tag_configure("Section", foreground="black", background= "#c1d7e1")
        tree.tag_configure("Config", foreground="black", background= "white")
        tree.tag_bind('Config', '<Double-Button-1>', self.onDoubleClick)
        tree.tag_bind('Config', '<Button-1>', self.onClick)
        return tree

    def onClick(self, event):
        if self.entryPopup is not None: self.entryPopup.destroy()

    def onDoubleClick(self, event):
        ''' Executed, when a row is double-clicked. Opens 
        read-only EntryPopup above the item's column, so it is possible
        to select text '''
        if self.entryPopup is not None: self.entryPopup.destroy()

        # what row and column was clicked on
        rowid = self.tree.identify_row(event.y)

        # get column position info
        a_x,a_y,a_width,a_height = self.tree.bbox(rowid, "Attribute")
        x,y,width,height = self.tree.bbox(rowid, "Value")

        # y-axis offset
        padx = a_width
        pady = height // 2

        # place Entry popup properly         
        text = self.tree.item(rowid, 'text')
        self.entryPopup = ConfigEntryPopup(self.tree, rowid, text, self.config_parser)
        self.entryPopup.place( x=padx, y=y+pady, anchor="w", relwidth=1)





class ConfigEntryPopup(Entry):

    def __init__(self, parent, iid, text, config_parser, **kw):
        super().__init__(parent, **kw)
        self.config_parser=config_parser
        self.tree = parent
        self.iid = iid

        self.values=self.tree.item(self.iid)["values"]
        self.insert(0, self.values[1])

        self.focus_force()
        self.bind("<Return>", self.on_return)
        self.bind("<Control-a>", self.select_all)
        self.bind("<Escape>", lambda *ignore: self.destroy())

    def on_return(self, event):
        section=self.tree.item(self.tree.parent(self.iid))['values'][0]
        config=self.values[0]
        new_val=self.get()
        if self.config_parser.set(section, config, new_val, robust=False):
            self.tree.set(self.iid, "Value" , new_val)
        self.destroy()

    def select_all(self, *ignore):
        ''' Set selection on the whole text '''
        self.selection_range(0, 'end')
        return 'break'






class FrameWithConfig(Notebook):

    def __init__(self, config_parser:Union[ConfigParserManager, None]=None, master=None, **kw):
        """@param config_parser: config to show
           @param gui_config_parser: gui config
           @param master: tk master
        """
        super().__init__(master=master, **kw)
        self.config_parser=config_parser
        self._addConfig()
        
    def _addConfig(self):
        self.add(ConfigFrame(self.config_parser, master=self), text="Config")