from os import getcwd
from tkinter import TOP, YES, BOTH, X, \
                    Tk, Frame, Label, Menu, Toplevel, Entry, Button, \
                    filedialog, messagebox

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config

class ExportImportConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("ExportImportConfig")
            self.export_as_dialog_bg = confSec["export_as_dialog_bg"]
            self.highlight_button_color = confSec["highlight_button_color"]
        else:
            self.export_as_dialog_bg = "white"
            self.highlight_button_color="#649BB4"
            


i_e_config=ExportImportConfig(config)

class ExportImport:
    """
        IMPORTS/EXPORTS
            - BYTES
            - pdf (Graphviz)
    """

    def __init__(self, controller, *args, **kwargs):
        assert controller
        self._controller=controller

    #   BYTES

    def importBYTES(self):
        filepaths = filedialog.askopenfilenames(title="Importer un objet",
                                                filetypes=[('BYTES file','.obj')],
                                                initialdir=getcwd()+"\\BYTES")
        for filepath in filepaths:
            self._controller.getModel().addObject(self._controller.importBYTES(filepath))

    def exportBYTES(self, data:list, uids):
        if data.shape[0]==0:
            messagebox.showinfo(title="BYTES Export", message="Aucun objet séléctionné.")
            return
        elif data.shape[0]==1:
            row=data.loc[int(uids[0])]
            filepath = filedialog.asksaveasfilename(title="Exporter un objet",
                                                    initialfile=str(row["class"])+str(data.index[0]),
                                                    filetypes=[('BYTES file','.obj')],
                                                    initialdir=getcwd()+"\\BYTES")+".obj"
            if filepath:
                self._controller.exportBYTES(filepath, row["object"])
        else:
            ExportAsDialog(self, data=data)

    def multipleExportBYTES(self, filePaths, objects):
        for k in range(len(filePaths)):
            self._controller.exportBYTES(filePaths[k], objects[k])

    #   GRAPHVIZ

    def exportGraphviz(self, uids):
        self._controller.getModel().exporterGraphviz(uids)


"""
    EXPORTS DIALOG CLASS
"""
class ExportAsDialog(Toplevel):
    _registered=None

    _frame=None
    _dir=None
    _data=None

    _selectFileNameFrames=None

    def __init__(self, register, data, master=None, **kwargs):
        super().__init__(master, **kwargs)
        self._registered=register
        self.title("Paramètres d'export")
        self.config(bg=i_e_config.export_as_dialog_bg)
        self.grab_set()

        self._data=data
        self._initFrame()

    def _initFrame(self):
        self._frame=Frame(self)
        self._frame.pack(expand=True, fill=BOTH)
        self._frame.columnconfigure(0, weight=1, minsize=200)
        self._frame.rowconfigure(1, weight=1, minsize=20)

        self._initDirectoryFrame()
        self._initObjectsFrame()
        self._initDialogFrame()

    def _initDirectoryFrame(self):
        frame=Frame(self._frame)
        frame.grid(row=0, column=0, sticky="new")

        frame.columnconfigure(1, weight=1)

        Label(frame, text="Dossier: ").grid(row=0, column=0, sticky="nsew")

        self._dir=Entry(frame)
        self._dir.insert(0, getcwd()+"\\BYTES")
        self._dir.grid(row=0, column=1, sticky="ew")

        Button(frame, text="Browse...", command=self.setDirectory).grid(row=0, column=2, sticky="nsew")

    def _initObjectsFrame(self):
        frame=Frame(self._frame)
        frame.grid(row=1, column=0, sticky="nsew")

        self._selectFileNameFrames=[]
        self._data.apply(lambda row: self._selectFileNameFrames.append(SelectFileNameFrame(frame, row)),axis=1)
        for f in self._selectFileNameFrames:
            f.pack(expand = YES, fill=X)

    def _initDialogFrame(self):
        frame=Frame(self._frame)
        frame.grid(row=2, column=0, sticky="sew")
        frame.columnconfigure(0, weight=1)

        b=Button(frame, text="Exporter",
                        highlightcolor=i_e_config.highlight_button_color,
                        command=self.validate)
        b.grid(row=0, column=1, sticky="e")
        b.focus_set()
        b.bind('<Return>', self.validate)

        Button(frame, text="Annuler", command=self.destroy).grid(row=0, column=2, sticky="e")

    """
        BIND FUNCTIONS
    """
    def setDirectory(self):
        dir=filedialog.askdirectory(initialdir=getcwd()+"\\BYTES",
                                    title = "Select directory")
        self._dir.delete(0,"end")
        self._dir.insert(0, dir+"/")

    def validate(self, event=None):
        basePath=self._dir.get()
        filePaths=[]
        for frame in self._selectFileNameFrames:
            filePaths.append(basePath+frame.getFileName())
        self._registered.multipleExportBYTES(filePaths, self._data["object"].tolist())
        self.destroy()



class SelectFileNameFrame(Frame):
    def __init__(self, parent, row, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self._row=row
        self.columnconfigure(0, weight=1, minsize=10)
        self.columnconfigure(2, weight=1, minsize=10)
        self.rowconfigure(1, weight=1, minsize=20)
        self._nameEntry=None
        self._initFrame()

    def _initFrame(self):
        Label(self, text=str(self._row.name)+" <"+str(self._row["class"])+"> "+str(self._row["object"])).grid(row=0, column=0, sticky="nsew")

        self._nameEntry=Entry(self)
        self._nameEntry.insert(0, str(self._row["class"])+str(self._row.name))
        self._nameEntry.grid(row=0, column=1, sticky="ew")

        Label(self, text=".obj").grid(row=0, column=2, sticky="nsew")
    """
        GETTERS
    """
    def getFileName(self):
        return self._nameEntry.get()+".obj"


























