"""
    Définition de l'espace de droite
"""

from tkinter import TOP, X, YES, \
                    Frame, Label

from GUI.tk.Factory import *

from GUI.plot.IntObjectBuilder.IntNewObjectSelectorFrame import IntNewObjectSelectorFrame

class OutilsGraphiquesFrame(Frame):

    _controller=None

    def __init__(self, parent, controller, *args, **kwargs):
        assert controller
        Frame.__init__(self, parent, *args, **kwargs)
        self._controller=controller
        self.columnconfigure(0, weight=1, minsize=100)
        self.rowconfigure(0, minsize=20)
        self.rowconfigure(1, weight=1)
        self._initFrame()

    def _initFrame(self):
        Factory.buildSectionLabel(self,"Outils Graphiques").grid(row=0, column=0, sticky="nwe")

        NOFrame=IntNewObjectSelectorFrame(self, self._controller.getModel())
        NOFrame.grid(row=1, column=0, sticky="nwe")


