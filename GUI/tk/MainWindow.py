from os import getcwd
from tkinter import TOP, YES, BOTH, X, \
                    Tk, Frame, Label, Menu, Toplevel, Entry, Button, \
                    filedialog, messagebox

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config

from GUI.tk.ParametersTopLevel import ParametersTopLevel
from GUI.tk.OutilsFrame import OutilsFrame
from GUI.tk.ObjetsFrame import ObjetsFrame
from GUI.tk.MetriquesFrame import MetriquesFrame
from GUI.tk.ExportImport import ExportImport
from GUI.tk.Definitions import Definitions
from GUI.tk.ConfigFrame import FrameWithConfig

from GUI.plot.PlotFrame import PlotFrame


class MainWindow:
    def __init__(self, controller, *args, **kwargs):
        assert controller
        self._controller=controller
        self.conf=config
        self.config=MainWindowConfig(config)
        self._EI=ExportImport(self._controller)

        self._root=None
        self._initRoot()

        self._frame=None
        self._plotFrame=None
        self._outFrame=None
        self._objFrame=None
        self._metFrame=None
        self._initFrames()
        
        self._menu=None
        self._initMenu()

        self._root.mainloop()

    """
        INITIALISATIONS
    """
    def _initRoot(self):
        self._root= Tk()
        w=self._root.winfo_screenwidth()
        h=self._root.winfo_screenheight()
        self._root.geometry("%dx%d%+d%+d" % (w/2,h-80,0,0))
        self._root.title('Théorie des catégories ')
        self._root.iconbitmap(self.config.icon_path)
        self._root.config(bg = self.config.bg)


    def _initFrames(self):

        self.frame=FrameWithConfig(self.conf, master=self._root)
        self.frame.pack(expand=YES, fill=BOTH)
        # Frame
        self._frame = Frame(self._frame)
        self._frame.pack(expand=YES, fill=BOTH)
        self._frame.columnconfigure(0, weight=3, minsize=500)
        self._frame.columnconfigure(1, weight=1, minsize=200)
        self._frame.columnconfigure(2, weight=2, minsize=200)
        self._frame.rowconfigure(0, weight=1, minsize=20)
        self._frame.rowconfigure(1, weight=1, minsize=20)

        # Plot frame
        self._plotFrame=PlotFrame(self._frame, self._controller)
        self._plotFrame.grid(row=0, column=0, rowspan=2, sticky="nesw")

        # Outils frame
        self._outFrame = OutilsFrame(self._frame, self._controller)
        self._outFrame.grid(row=0, column=1, sticky="nesw")

        # Metriques frame
        self._metFrame=MetriquesFrame(self._frame)
        self._metFrame.grid(row=1, column=1, sticky="nesw")

        # Objets frame
        self._objFrame = ObjetsFrame(self._frame, self._controller, self)
        self._objFrame.grid(row=0, column=2, rowspan=2, sticky="nesw")
        self._objFrame.registerSelection(self._metFrame)
        
        self.frame.add(self._frame, text="                            ")
        self.frame.select(1)


    def _initMenu(self):
        self._menu=Menu()

        # >Menu
        fichierMenu=Menu(self._menu, tearoff=0)
        # >> Importer
        importerMenu=Menu(fichierMenu, tearoff=0)
        importerMenu.add_cascade(label="BYTES", command=self._EI.importBYTES)
        fichierMenu.add_cascade(label="Importer", menu=importerMenu)
        # >> Exporter
        exporterMenu=Menu(fichierMenu, tearoff=0)
        exporterMenu.add_cascade(label="BYTES", command=self.exportBYTES)
        exporterMenu.add_cascade(label="Graphviz", command=self.exportGraphviz)
        fichierMenu.add_cascade(label="Exporter", menu=exporterMenu)
        # ---------
        fichierMenu.add_separator()
        # ) Quitter
        fichierMenu.add_cascade(label="Quitter", command=self._root.destroy)

        self._menu.add_cascade(label="Fichier", menu=fichierMenu)
        self._menu.add_cascade(label="Définitions", command=self.definitions)
        self._menu.add_cascade(label="Paramètres", command=self.parameters)
        self._root.config(menu=self._menu)

    """
        Command
    """
    def exportBYTES(self):
        uids=self._objFrame.getSelectedObjects()
        self._EI.exportBYTES(self._controller.getModel().getObject(uids), uids)

    def exportGraphviz(self):
        self._EI.exportGraphviz(self._objFrame.getSelectedObjects())

    def definitions(self):
        Definitions()

    def parameters(self):
        ParametersTopLevel()


    """
        Getters
    """
    def getEI(self): return self._EI
    def getRoot(self): return self._root   
    def getPlotFrame(self): return self._plotFrame
    





class MainWindowConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("MainWindow")
            self.icon_path= confSec["icon_path"]

            confSec=config.getConfigSectionMap("Default")
            self.bg= confSec["bg"]
        else:
            self.bg="#000000"
            self.icon_path="GUI/ressources/graphe.ico"