from tkinter import Frame, \
                    DISABLED, NORMAL, END, YES, BOTH, RIGHT, LEFT, Y
from tkinter.ttk import Notebook

from typing import Union
from ConfigParserManager import ConfigParserManager

from GUI.ConfigFrame import ConfigFrame

class FrameWithConfig1(Frame):

    def __init__(self, config_parser, gui_config_parser:Union[ConfigParserManager, None]=None, master=None, cnf={}, **kw):
        """@param config_parser: config to show
           @param gui_config_parser: gui config
           @param master: tk master
        """
        super().__init__(master=master, cnf=cnf, **kw)
        print("FrameWithConfig")
        self.config_parser=config_parser
        self.gui_config_parser=gui_config_parser
        self._initFrame()

    def _initFrame(self):
        self.rowconfigure(0, weight=1, minsize=100)
        self.columnconfigure(0, weight=1, minsize=100)

        self._notebook=self._createNotebook()
        self._notebook.grid(column=0, row=0, sticky="nsew")
        
    def _createNotebook(self)->Notebook:
        notebook=Notebook(self)
        notebook.add(ConfigFrame(self.config_parser,
                                self.gui_config_parser,
                                master=notebook), text="Config")
        return notebook

    def addNewFrame(self, text="text", **ka)->Frame:
        frame=Frame(self._notebook, **ka)
        self._notebook.add(frame, text=text)
        return frame


class FrameWithConfig(Notebook):

    def __init__(self, config_parser, gui_config_parser:Union[ConfigParserManager, None]=None, master=None, **kw):
        """@param config_parser: config to show
           @param gui_config_parser: gui config
           @param master: tk master
        """
        super().__init__(master=master, **kw)
        self.config_parser=config_parser
        self.gui_config_parser=gui_config_parser
        self._addConfig()
        
    def _addConfig(self):
        self.add(ConfigFrame(self.config_parser,
                                self.gui_config_parser,
                                master=self), text="Config")