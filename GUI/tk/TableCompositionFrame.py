"""
    Définition de l'espace de droite
"""

from tkinter import TOP, X, YES, \
                    Frame, Label

from GUI.tk.Factory import *

from GUI.tk.ObjectBuilder.NewObjectSelectorFrame import NewObjectSelectorFrame

from Categorie import *
from Morphisme import *

class TableCompositionFrame(Frame):

    _controller=None
    _categorie=None
    _morphismes=None
    _table=None
    _size=None

    def __init__(self, parent, controller, categorie:list):
        assert controller
        Frame.__init__(self, parent, *args, **kwargs)
        self._controller=controller
        self._categorie=categorie
        self._initTable()

        self._initFrame()

    def _initTable(self):
        for morph in self._categorie:
            pretty_print(morph)

    def _initFrame(self):
        Factory.buildSectionLabel(self,"Table de composition").grid(row=0, column=0, sticky="nwe")

