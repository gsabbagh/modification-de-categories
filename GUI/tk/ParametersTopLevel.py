from tkinter import TOP, YES, BOTH, X, \
                    Frame, Toplevel

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config as gui_conf

from GUI.tk.Factory import *


class ParametersTopLevel(Toplevel):

    def __init__(self, master=None, **kwargs):
        super().__init__(master, **kwargs)
        self.gui_config=ParametersTopLevelConfig(gui_conf)
        self.title("Théorie des catégories")
        self.geometry("600x550")
        self._initFrame()

    def _initFrame(self):
        self._frame=Frame(self)
        self._frame.pack(side=TOP, expand=True, fill=BOTH)

        # Section
        Factory.buildSectionLabel(self._frame,"Paramètres").pack(side=TOP, expand=False, fill=X)


class ParametersTopLevelConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("Default")
            self.bg= confSec["bg"]
        else:
            self.bg="#000000"