"""
    Espace de l'affichage des métriques.
"""
from pandas import DataFrame
from tkinter import TOP, X, NO, W, Y, YES, BOTH, RIGHT, VERTICAL, \
                    Frame, Label, Menu
from tkinter.ttk import Treeview, Style, Scrollbar

from GUI.ModelObjectsBuilder import Description

from GUI.tk.InformFrame import InformFrame
from GUI.tk.Factory import *
from GUI.tk.ObjetsFrameCallable import ObjetsFrameCallable

from GUI.plot.GraphGenerator import GraphGenerator




class MetriquesFrame(Frame, ObjetsFrameCallable):
    _informFrame=None

    def __init__(self, parent, *args, **kwargs):
        Frame.__init__(self, parent, *args, **kwargs)
        self.columnconfigure(0, weight=1, minsize=100)
        self.columnconfigure(1, minsize=16)#default value of scrollbar width
        self.rowconfigure(0, minsize=20)
        self.rowconfigure(1, weight=4, minsize=100)

        #self._initFrame()

    def _initFrame(self):
        Factory.buildSectionLabel(self,"Métriques").grid(row=0, column=0, columnspan = 2, sticky="nwe")

        # INFORM
        self._informFrame=InformFrame(self)
        self._informFrame.grid(row=2, columnspan = 2, column=0, sticky="nesw")

    def informObjectSelected(self, uid, obj:object):
        pass








