"""
    Paramètres de la GUI
"""
from tkinter import Label
import tkinter.font as tkFont

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config

class FactoryConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("Factory")
            self.section_color = confSec["section_color"]
            self.section_relief=confSec["section_relief"]
            self.section_font_size = confSec["section_font_size"]
            self.section_font_weight = confSec["section_font_weight"]
            self.section_font_underline=confSec["section_font_underline"]
            self.sub_section_color = confSec["sub_section_color"]
            self.higher_section_color =confSec["higher_section_color"]
        else:
            self.section_color = "#649bb4"
            self.section_relief="groove"
            self.section_font_size = 10
            self.section_font_weight = 'bold'
            self.section_font_underline= 0
            self.sub_section_color = "#7da6b8"
            self.higher_section_color = "#a2b4bb"

factory_config=FactoryConfig(config)

class Factory():

    @staticmethod
    def buildSectionLabel(parent, text, level=0):
        # section
        if level==0:
            return Label(parent, text=text,
                        bg=factory_config.section_color,
                        relief=factory_config.section_relief,
                        font=tkFont.Font(size=factory_config.section_font_size,
                                            weight=factory_config.section_font_weight,
                                            underline=factory_config.section_font_underline))
        #subsection
        elif level==1:
            return Label(parent, text=text,
                        bg=factory_config.sub_section_color,
                        relief=factory_config.section_relief,
                        font=tkFont.Font(size=factory_config.section_font_size,
                                            weight=factory_config.section_font_weight,
                                            underline=factory_config.section_font_underline))
        # higher levels
        else:
            return Label(parent, text=text,
                        bg=factory_config.higher_section_color,
                        relief=factory_config.section_relief,
                        font=tkFont.Font(size=factory_config.section_font_size,
                                            weight=factory_config.section_font_weight,
                                            underline=factory_config.section_font_underline))

    @staticmethod
    def buildSubSectionLabel(parent, text):
        return Factory.buildSectionLabel(parent, text, level=1)
