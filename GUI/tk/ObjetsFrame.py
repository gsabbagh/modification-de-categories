"""
    Espace du tableau des objets.
"""
from pandas import DataFrame
from tkinter import TOP, X, NO, W, Y, YES, BOTH, RIGHT, VERTICAL, \
                    Frame, Label, Menu
from tkinter.ttk import Treeview, Style, Scrollbar

from GUI.ModelCallable import ModelCallable
from GUI.ModelObjectsBuilder import Description

from GUI.tk.InformFrame import InformFrame
from GUI.tk.Factory import *
from GUI.tk.ObjetsFrameCallable import ObjetsFrameCallable
from GUI.tk.ObjectBuilder.TopLevelTableComposition import TopLevelTableComposition

from GUI.plot.GraphGenerator import GraphGenerator
from GUI.plot.IntObjectBuilder.IntNewGrapheDeCompositionFrame import IntNewGrapheDeCompositionFrame

from GrapheDeComposition import GrapheDeComposition


class ObjetsFrame(Frame, ModelCallable):
    

    def __init__(self, parent, controller, mainWindow, *args, **kwargs):
        assert controller and mainWindow
        Frame.__init__(self, parent, *args, **kwargs)
        self._controller=controller
        self._controller.getModel().registerModelCallable(self)
        self._mainWindow=mainWindow
        self._selectionRegistered=[]

        self._tree=None
        self._scrollbarY=None
        self._informFrame=None

        self._popupMenu=None
        self._popupObjectIid=None

        self._folders={}
        
        self._initFrame()
        self._initMenu()

    def _initFrame(self):
        self.columnconfigure(0, weight=1, minsize=100)
        self.columnconfigure(1, minsize=16)#default value of scrollbar width
        self.rowconfigure(0, minsize=20)
        self.rowconfigure(1, weight=4, minsize=100)

        Factory.buildSectionLabel(self,"Objets").grid(row=0, column=0, columnspan = 2, sticky="nwe")
        # TREE
        self._tree=Treeview(self, selectmode='extended')
        self._tree.grid(row=1, column=0, sticky="nesw")
        style = Style()
        style.theme_use("default")
        style.map("Treeview")

        self._scrollbarY=Scrollbar(self, orient =VERTICAL)
        self._scrollbarY.grid(row=1, column=1, sticky="nsw")

        self._tree.configure(yscrollcommand = self._scrollbarY.set)
        self._scrollbarY.config(command=self._tree.yview)

        self._tree["columns"]=("class","description")
        self._tree.column("#0", width=100, minwidth=15, stretch=False)
        self._tree.column("class", width=15, stretch=False)
        self._tree.column("description", width=200, minwidth=80, stretch=True)

        self._tree.heading("#0",text="UID",anchor=W)
        self._tree.heading("class", text="Classe",anchor=W)
        self._tree.heading("description", text="Description",anchor=W)

        self._tree.bind("<<TreeviewSelect>>", self.objectSelection)
        self._tree.bind("<Delete>", self.objectSelection)
        self._tree.bind("<Button-3>", self.popup) # Button-2 on Aqua

        # INFORM
        self._informFrame=InformFrame(self)
        self._informFrame.grid(row=2, columnspan = 2, column=0, sticky="nesw")


    def _initMenu(self):
        # popup tree menu
        self._popupMenu = Menu(self._tree, tearoff=0)

        self._popupMenu.add_command(label="Tableau de composition",
                                    command=self.tableDeComposition)
        self._popupMenu.add_command(label="NetworkX",
                                    command=self.toNetworkX)
        self._popupMenu.add_command(label="Modifier",
                                    command=self.modify)
        self._popupMenu.add_command(label="Graphviz",
                                    command=self.toGraphviz)


    def registerSelection(self, ofCallable:ObjetsFrameCallable):
        self._selectionRegistered.append(ofCallable)

    def callRegisteredSelection(self, uid, obj:object):
        for registered in self._selectionRegistered:
            registered.informObjectSelected(uid, obj)

    """
        GETTERS
    """
    def getIfExist(self, d:dict, cle:object):
        if cle in d:
            return d[cle]
        else:
            return None

    def getFolder(self, class_name:str):
        return self.getIfExist(self._folders, class_name)

    def getSelectedObjects(self)->list:
        curItems = self._tree.selection()
        sel=[]
        for i in curItems:
            if self._tree.parent(i):
                sel.append(i)
        return sel

    """
        INFORM FROM MODEL
    """
    def informDataModified(self):
        self.actualizeAllObjects(self._controller.getModel().getData())

    def informObjectAdded(self, uid, obj:object):
        self.addObject(uid, obj)

    def informObjectDeleted(self, uid):
        print(uid)

    def informObjectsAdded(self, uids:list):
        print(uids)

    def informObjectsDeleted(self, uids:list):
        print(uids)

    """
        OPERATIONS ON TREE
    """
    def actualizeAllObjects(self, df:DataFrame):
        for c in self._tree.get_children(): self._tree.delete(c)
        folders=df['class'].unique()
        self._folders={}
        for folder in folders:
            f=self._tree.insert(parent="", index="end", text=folder)
            self._folders[folder]=f
            objects=(df['object'][df['class']==folder]).sort_index(ascending=True)
            for uid, obj in objects.items():
                self._tree.insert(parent=f, iid=uid, index="end", text=uid, values=(folder, Description(obj)))

    def addObject(self, uid, obj:object):
        class_name=type(obj).__name__
        f=self.getFolder(class_name)
        if not f:
            f=self._tree.insert(parent="", index="end", text=class_name)
            self._folders[class_name]=f
        self._tree.insert(parent=f, iid=uid, index="end", text=uid, values=(class_name, Description(obj)))

    def deleteObject(self, uid):
        self._tree.delete(self._tree.item(uid))


    """
        BIND FUNCTIONS/MENU FUNCTIONS
    """
    def objectSelection(self, event):
        curItems = self.getSelectedObjects()
        text=""
        for i in curItems:
            item=self._tree.item(i)
            text+= (str(item['text'])+" : <"+
                    str(item['values'][0])+"> "+
                    str(item['values'][1])+"\n"
                    )
        self._informFrame.setText(text)
        if curItems:
            uid=int(curItems[0])
            self.callRegisteredSelection(uid, self._controller.getModel().getObject(uid))

    def objectDelete(self, event):
        pass

    def popup(self, event):
        try:
            iid = self._tree.identify_row(event.y)
            if iid:
                if self._tree.parent(iid):
                    self._popupObjectIid=int(iid)
                    self._obj=self._controller.getModel().getObject(self._popupObjectIid)['object']
                    if not issubclass(type(self._obj), GrapheDeComposition):
                        self._popupMenu.entryconfig("Tableau de composition", state="disabled")
                self._popupMenu.tk_popup(event.x_root, event.y_root, 0)
            else:
                pass
        finally:
            self._popupMenu.grab_release()

    def tableDeComposition(self):
        TopLevelTableComposition(self._obj)


    def toNetworkX(self):
        if self._popupObjectIid is not None:
            gc=self._controller.getModel().getObject(self._popupObjectIid)['object']
            self._mainWindow.getPlotFrame().setGraph(GraphGenerator.give(gc))
            self._popupObjectIid=None
            
    def modify(self):
        gc=self._controller.getModel().getObject(self._popupObjectIid)['object']
        graph=GraphGenerator.give(gc)
        pos=self._mainWindow.getPlotFrame().getLayoutAlgo().getPositions(graph)
        IntNewGrapheDeCompositionFrame(None, self._controller, (gc, graph, pos))

    def toGraphviz(self):
        if self._popupObjectIid is not None:
            self._mainWindow.getEI().exportGraphviz([self._popupObjectIid])
            self._popupObjectIid=None









