
from tkinter import TOP, BOTTOM, LEFT, RIGHT, BOTH, \
                    Frame, Label, StringVar

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config as gui_config

class InformFrame(Frame):

    def __init__(self, parent, *args, **kwargs):
        Frame .__init__(self, parent, *args, **kwargs)
        self.gui_config=InformFrameConfig(gui_config)
        self._label=None
        self._text=None
        self._initFrame()

    def _initFrame(self):
        self._text=StringVar()
        self._label=Label(self,
                        bg=self.gui_config.bg,
                        fg=self.gui_config.fg,
                        anchor='nw',
                        textvariable=self._text)
        self._label.pack(expand=True, fill=BOTH)

    """
        SETTERS
    """
    def setText(self, text:str):
        self._text.set(text)

    def inform(self, text:str):
        self._text.set(self._text.get()+text+"\n")


class InformFrameConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("InformFrame")
            self.bg= confSec["bg"]
            self.fg= confSec["fg"]
        else:
            self.bg="#000000"
            self.fg="#C8C8C8"









































