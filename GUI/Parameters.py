""" *************************************
    UTC - TX - Théorie des catégories
                2021 - F.M.

    Paramètres de l'interface graphique
************************************* """

class Parameters():

    """
    ///////////////////// TK
    """
    # Frames
    FRAME_BD=2

    # Section frame
    SECTION_COLOR = "#649BB4"
    SECTION_RELIEF="groove"
    SECTION_FONT_SIZE = 10
    SECTION_FONT_WEIGHT = 'bold'
    SECTION_FONT_UNDERLINE= 0
    # SubSection frame
    SUB_SECTION_COLOR = "#7da6b8"
    # Higher level
    HIGHER_SECTION_COLOR = "#a2b4bb"

    # Inform frame
    INFORM_BG='#C0C0C0'
    INFORM_FG='black'

    # Export frame
    ExportAsDialog_BG='white'
    HIGHLIGHTCOLOR_BUTTON="#649BB4"

    # Definitions Frame
    Definitions_BG='#0033FF'


    # COLORS
    BUTTON_COLOR="#FFFFFF"

    """
    ///////////////////// NetworkX
    """
    # Graph
    NODE_SIZE=300 # Correspond à un carré de côté x avec x*x=300
    NODE_COLOR='#000000'
    EDGE_ELEMENTARY_COLOR='#000000'
    EDGE_NO_ELEMENTARY_COLOR='#0033FF'

    NODE_FONT_COLOR_DEFAULT='#93e3a1'   #
    NODE_FONT_COLOR_SELECTED='#fc927d'  # 3eb453


parameters=Parameters()

