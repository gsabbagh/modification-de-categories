from numpy import sqrt
import networkx as nx

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config

class NodeSelectorConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("NetworkX")
            self.node_size=int(confSec["node_size"]) 
            self.node_color=confSec["node_color"]
            self.edge_elementary_color=confSec["edge_elementary_color"]
            self.edge_no_elementary_color=confSec["edge_no_elementary_color"]

            self.node_font_color_default=confSec["node_font_color_default"]
            self.node_font_color_selected=confSec["node_font_color_selected"]
        else:
            self.node_size=300 # Correspond à un carré de côté x avec x*x=300
            self.node_color="#000000"
            self.edge_elementary_color="#000000"
            self.edge_no_elementary_color="#0033FF"

            self.node_font_color_default="#93e3a1"
            self.node_font_color_selected="#fc927d"

ns_config=NodeSelectorConfig(config)

class NodeSelector:

    @staticmethod
    def trySelectNode(event, data):
        # On utilise les distances au carré
        axes=event.inaxes
        if axes and data:
            # Trouve la limite de distance pour toucher un noeud
            p1=axes.transData.inverted().transform((0,0))
            p2=axes.transData.inverted().transform((0,sqrt(ns_config.node_size)))
            max_dist=(p1[0]-p2[0])**2+(p1[1]-p2[1])**2

            # Cherche le noeud le plus proche
            clickX = event.xdata
            clickY = event.ydata
            distances = []
            selection=None
            smallest_dist = float('inf')
            for x,y,node in data:
                dx = x - clickX
                dy = y - clickY
                dist = dx*dx+dy*dy
                if dist <= smallest_dist and dist <= max_dist:
                    selection=(sqrt(dist),x,y, node)
                    smallest_dist=dist
            return selection
        else:
            return None

    @staticmethod
    def updateSelectedNode(selection, graph, node_color):
        assert selection
        """ selection: (dist, x, y, node)
            graph: nx graph
            node_color: list
        """
        node = selection[3]

        # mise à jour de l'état du noeud (attribut 'selected')
        if graph.nodes[node]['selected']==False:
            nx.set_node_attributes(graph, name='selected', values={node:True})
        else:
            nx.set_node_attributes(graph, name='selected', values={node:False})

        # mise à jour liste des couleurs
        node_color.clear()
        for node in graph.nodes(data=True):
            if node[1]['selected'] == False:
                node_color.append(ns_config.node_font_color_default)
            elif node[1]['selected'] == True:
                node_color.append(ns_config.node_font_color_selected)




















# def drawAnnote(self, axis, x, y, annote):
#     if (x, y) in self._drawnAnnotations:
#         markers = self._drawnAnnotations[(x, y)]
#         for m in markers:
#             m.set_visible(not m.get_visible())
#         axis.figure.canvas.draw()
#     else:
#         t = axis.text(x, y, "%s" % (annote), )
#         m = axis.scatter([x], [y], marker='d', c='r', zorder=100)
#         self._drawnAnnotations[(x, y)] = (t, m)
#         axis.figure.canvas.draw()
