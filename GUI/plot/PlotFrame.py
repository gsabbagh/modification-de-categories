from tkinter import TOP, X, YES, BOTH, \
                    Frame, Label
from GUI.tk.Factory import *
from GUI.plot.IntOutilsGraphiquesFrame import IntOutilsGraphiquesFrame
from GUI.plot.LayoutAlgoSelectorFrame import LayoutAlgoSelectorFrame

from GUI.plot.MainGraphDrawer import MainGraphDrawer

import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("TkAgg")

from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)

import networkx as nx


class PlotFrame(Frame):
    """
        Frame capable d'afficher un graphe NetworkX
    """
    # _controller=None
    #
    # _outilsFrame=None
    #
    # _axes=None
    # _canvas=None
    #
    # _graphDrawer=None


    def __init__(self, parent, controller, *args, **kwargs):
        assert controller
        Frame.__init__(self, parent, *args, **kwargs)
        self._controller=controller
        self.columnconfigure(0, weight=1, minsize=200)
        self.rowconfigure(0, minsize=20)
        self.rowconfigure(1, minsize=20)
        self.rowconfigure(2, minsize=50)
        self.rowconfigure(3, weight=1, minsize=100)
        self._initFrame()

    def _initFrame(self):
        # Section
        Factory.buildSectionLabel(self,"Visualisation NetworkX").grid(row=0, column=0, sticky="nwe")

        # Layout algo selector
        self._layoutAlgoSelector=LayoutAlgoSelectorFrame(self)
        self._layoutAlgoSelector.grid(row=1, column=0, sticky="new")

        # Création graphique
        self._outilsFrame=IntOutilsGraphiquesFrame(self, self._controller)
        self._outilsFrame.grid(row=2, column=0, sticky="nesw")

        # pyplot
        f = Figure(figsize=(5,5), dpi=100)
        self._axes=f.add_subplot(111)

        # graph
        self._canvas=FigureCanvasTkAgg(f, self)
        self._canvas.get_tk_widget().grid(row=3, column=0, sticky="nswe")
        self._graphDrawer=MainGraphDrawer(self._axes,
                                            self._canvas,
                                            self._layoutAlgoSelector)

        # toolbar
        toolbar = NavigationToolbar2Tk(self._canvas, self, pack_toolbar=False)
        toolbar.update()
        toolbar.grid(row=4, column=0, sticky="nswe")

    def setGraph(self, G:nx.Graph):
        self._graphDrawer.setGraph(G)

    def getLayoutAlgo(self)->LayoutAlgoSelectorFrame:
        return self._layoutAlgoSelector





