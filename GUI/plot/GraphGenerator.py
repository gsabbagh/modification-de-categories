from itertools import product

from Categorie import Categorie

import networkx as nx

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config

class GraphGeneratorConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("NetworkX")
            self.node_size=int(confSec["node_size"]) 
            self.node_color=confSec["node_color"]
            self.edge_elementary_color=confSec["edge_elementary_color"]
            self.edge_no_elementary_color=confSec["edge_no_elementary_color"]

            self.node_font_color_default=confSec["node_font_color_default"]
            self.node_font_color_selected=confSec["node_font_color_selected"]
        else:
            self.node_size=300 # Correspond à un carré de côté x avec x*x=300
            self.node_color="#000000"
            self.edge_elementary_color="#000000"
            self.edge_no_elementary_color="#0033FF"

            self.node_font_color_default="#93e3a1"
            self.node_font_color_selected="#fc927d"

gg_config=GraphGeneratorConfig(config)

# Static
class GraphGenerator:
    @staticmethod
    def give(obj) -> nx.Graph:
        """ Construit le nx.Graph associé à un objet par la fonction 'give(object)'
        """
        if issubclass(type(obj), Categorie):
            return GraphGenerator._genererGrapheCategorie(obj)
        else:
            print(f"Impossible de générer le graphe de l'objet: {obj}")



    """
        GENERATEURS PAR CLASSE
    """


    @staticmethod
    def _genererGrapheCategorie(cat,
                                afficher_identites:bool = False,
                                complet:bool = True,
                                limite_fleches:int = 50)-> nx.Graph:
        """
        @param complet: - si True affiche toutes les flèches;
                        - si False affiche seulement les flèches élémentaires.
        @param limite_fleches: nombre maximal de flèches entre deux objets.
        """
        G = nx.MultiDiGraph()

        G.add_nodes_from(cat.objets, color=gg_config.node_color)

        # flesches
        fleches_elem = set(cat[cat.objets,cat.objets])
        if complet:
            func = lambda x,y : cat({x},{y})
        else:
            func = lambda x,y : cat[{x},{y}]

        for source,cible in product(cat.objets,repeat=2):
            nb_fleches = 0
            for fleche in func(source,cible):
                if afficher_identites or not fleche.is_identite:
                    if fleche.source not in cat.objets:
                        raise Exception("Source d'une fleche pas dans les objets de la categorie.")
                    if fleche.cible not in cat.objets:
                        raise Exception("Source d'une fleche pas dans les objets de la categorie.")
                    if len({obj for obj in cat.objets if obj == fleche.source}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est source de la fleche.")
                    if len({obj for obj in cat.objets if obj == fleche.cible}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est cible de la fleche.")
                    #permet d'avoir toujours un objet de la catégorie comme source
                    source = {obj for obj in cat.objets if obj == fleche.source}.pop()
                    #permet d'avoir toujours un objet de la catégorie comme cible
                    cible = {obj for obj in cat.objets if obj == fleche.cible}.pop()
                    # descriptions
                    if fleche in fleches_elem:
                        G.add_edge(str(source),str(cible),
                                    color=gg_config.edge_elementary_color,
                                    label=str(fleche))
                    else:
                        G.add_edge(str(source),str(cible),
                                    color=gg_config.edge_no_elementary_color,
                                    label=str(fleche))
                    nb_fleches += 1
                if nb_fleches > limite_fleches:
                    if WARNING_LIMITE_FLECHES_ATTEINTE:
                        print("Warning : limite fleches entre "+str(source)+" et "+str(cible)+" atteinte.")
                        break

        return G
