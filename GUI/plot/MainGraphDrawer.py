from numpy import sqrt
import networkx as nx

from GUI.plot.NodeSelector import NodeSelector
from GUI.plot.GraphDrawer import GraphDrawer
from GUI.plot.CameraFactory import CameraFactory

class MainGraphDrawer(GraphDrawer):
    # eventManager=None
    #
    # plot=None
    # canvas=None
    #
    # graph=None
    # pos=None
    # node_color=None

    def __init__(self, axes, canvas, layoutAlgoSelector):
        assert layoutAlgoSelector
        GraphDrawer.__init__(self, axes, canvas)
        self.layoutAlgoSelector=layoutAlgoSelector

        # Event Manager
        self.eventManager=EventManager(self)

        # Random Graph
        G = nx.fast_gnp_random_graph(n=10, p=0.5,directed=True)
        self.setGraph(G)

    # @overwrite
    def setGraph(self, G:nx.Graph):
        assert self.layoutAlgoSelector
        GraphDrawer.setGraph(self, G, self.layoutAlgoSelector.getPositions(G))




class EventManager:

    def __init__(self, drawer):
        self.drawer = drawer
        self.drawer.canvas.mpl_connect('button_press_event', self._button_press_event)
        CameraFactory.addScrollZoom(self.drawer.canvas)


    def _button_press_event(self, event):
        selection = NodeSelector.trySelectNode(event, self.drawer.data)
        if selection:
            NodeSelector.updateSelectedNode(selection,
                                            self.drawer.graph,
                                            self.drawer.node_color)
        self.drawer.drawGraph()


















