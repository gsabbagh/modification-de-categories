import networkx as nx

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config

class GraphDrawerConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("NetworkX")
            self.node_size=int(confSec["node_size"]) 
            self.node_color=confSec["node_color"]
            self.edge_elementary_color=confSec["edge_elementary_color"]
            self.edge_no_elementary_color=confSec["edge_no_elementary_color"]

            self.node_font_color_default=confSec["node_font_color_default"]
            self.node_font_color_selected=confSec["node_font_color_selected"]
        else:
            self.node_size=300 # Correspond à un carré de côté x avec x*x=300
            self.node_color="#000000"
            self.edge_elementary_color="#000000"
            self.edge_no_elementary_color="#0033FF"

            self.node_font_color_default="#93e3a1"
            self.node_font_color_selected="#fc927d"

gd_config=GraphDrawerConfig(config)



class GraphDrawer:
    # axes=None
    # canvas=None
    #
    # graph=None
    # pos=None
    # node_color=None

    def __init__(self, axes, canvas):
        assert axes and canvas
        self.axes=axes
        self.canvas=canvas

    def setGraph(self, G:nx.Graph, pos):
        self.graph=G
        self.pos=pos
        self.initDrawableGraph()
        self.drawGraph()

    def initDrawableGraph(self):
        x, y, annotes = [], [], []
        for key in self.pos:
            d = self.pos[key]
            annotes.append(key)
            x.append(d[0])
            y.append(d[1])
        self.data = list(zip(x, y, annotes))
        nx.set_node_attributes(self.graph, name="selected", values=False)
        self.node_color=[gd_config.node_font_color_default for n in self.graph.nodes]

    def getLabelAtt(self, att:dict)->str:
        if 'label' in att:
            return att['label']
        else:
            return ""

    def drawGraph(self):
        self.axes.cla()
        nx.draw_networkx(self.graph,
                        ax=self.axes,
                        pos=self.pos,
                        node_color=self.node_color,
                        alpha=0.9,
                        node_size=gd_config.node_size)
        edge_labels={(s,c):self.getLabelAtt(att) for s, c, att in self.graph.edges(data=True)}
        nx.draw_networkx_edge_labels(self.graph,
                        ax=self.axes,
                        pos=self.pos,
                        edge_labels=edge_labels,
                        font_color='red'
        )
        self.canvas.draw()
















