import matplotlib.pyplot as plt

from tkinter import Button, PhotoImage, SUNKEN, RAISED

MOVE_IMG_PATH="GUI/ressources/cursor_move.png"


class CameraFactory:

    @staticmethod
    def addScrollZoom(canvas, base_scale = 1.2):
        assert base_scale != 0

        def scrollZoom(event):
            ax=event.inaxes
            if not ax:
                return
            # get the current x and y limits
            cur_xlim = ax.get_xlim()
            cur_ylim = ax.get_ylim()
            cur_xrange = (cur_xlim[1] - cur_xlim[0])*.5
            cur_yrange = (cur_ylim[1] - cur_ylim[0])*.5
            xdata = event.xdata # get event x location
            ydata = event.ydata # get event y location
            if event.button == 'up':
                # deal with zoom in
                scale_factor = 1/base_scale
            elif event.button == 'down':
                # deal with zoom out
                scale_factor = base_scale
            else:
                # deal with something that should never happen
                scale_factor = 1
                print(event.button)
            # set new limits
            ax.set_xlim([xdata - cur_xrange*scale_factor,
                        xdata + cur_xrange*scale_factor])
            ax.set_ylim([ydata - cur_yrange*scale_factor,
                        ydata + cur_yrange*scale_factor])
            canvas.draw()

        canvas.mpl_connect('scroll_event', scrollZoom)
        return scrollZoom

    @staticmethod
    def createEnableMoveButton(canvas, master=None)->Button:
        move_manager=MoveManager(canvas)
        return MoveButton(move_manager, master=master)

class MoveButton(Button):

    def __init__(self, move_manager, master=None):
        self.image=PhotoImage(file = MOVE_IMG_PATH)
        super().__init__(master=master,
                         command=self.switchState,
                         image=self.image)
        self.move_manager=move_manager
        self.enable=False

    def switchState(self):
        self.enable= not self.enable
        if self.enable:
            self.config(relief=SUNKEN)
        else:
            self.config(relief=RAISED)
        self.move_manager.setState(self.enable)

    def isEnable(self)->bool:
        return self.enable


class MoveManager:

    def __init__(self, canvas, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.canvas=canvas
        self.setState(False)
        self.canvas.mpl_connect('button_press_event', self._button_press_event)
        self.canvas.mpl_connect('button_release_event', self._button_release_event)

    def setState(self, enable:bool):
        self.enable=enable
        if enable:
            self.canvas.get_tk_widget().config(cursor='fleur')
        else:
            self.canvas.get_tk_widget().config(cursor='tcross')
    
    def _button_press_event(self, event):
        if not self.enable:
            return
        ax=event.inaxes
        if not ax:
            return
        self.press_xdata = event.xdata # get event x location
        self.press_ydata = event.ydata # get event y location

    def _button_release_event(self, event):
        if (not self.enable) or self.press_xdata is None or self.press_ydata is None:
            return
        ax=event.inaxes
        if not ax:
            return
        # get the current x and y limits
        cur_xlim = ax.get_xlim()
        cur_ylim = ax.get_ylim()
        xdata = event.xdata # get event x location
        ydata = event.ydata # get event y location
        
        dx=self.press_xdata-xdata
        dy=self.press_ydata-ydata

        ax.set_xlim([cur_xlim[0]+dx,
                    cur_xlim[1]+dx])
        ax.set_ylim([cur_ylim[0]+dy,
                    cur_ylim[1]+dy])
        self.canvas.draw()






















