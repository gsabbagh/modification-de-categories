from tkinter import TOP, X, YES, BOTH, \
                    Frame, Label

from GUI.plot.IntObjectBuilder.IntGraphDrawer import IntGraphDrawer
from GUI.plot.CameraFactory import CameraFactory

import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("TkAgg")

from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import (FigureCanvasTkAgg, NavigationToolbar2Tk)

import networkx as nx


class IntPlotFrame(Frame):
    """
        Frame de création et affichage d'un graphe NetworkX
    """

    # _plot=None
    # _canvas=None
    #
    # _graphDrawer=None

    def __init__(self, parent, *args, **kwargs):
        assert parent
        Frame.__init__(self, parent, *args, **kwargs)
        self.init_gc_pos=parent.init_gc_pos
        self._controller=parent.controller
        self.parent=parent

        self._initFrame()

    def _initFrame(self):
        self.columnconfigure(0, weight=1, minsize=200)
        self.rowconfigure(0, weight=0, minsize=20)
        self.rowconfigure(1, weight=1, minsize=400)

        # pyplot
        figure = Figure(figsize=(5,5), dpi=100)
        self.plot=figure.add_subplot(111)

        # canvas
        self.canvas=FigureCanvasTkAgg(figure, self)
        self.canvas.get_tk_widget().grid(row=1, column=0, sticky="nswe")
        
        # move
        self.move_button=CameraFactory.createEnableMoveButton(self.canvas, master=self)
        self.move_button.grid(row=0, column=0, sticky="w")

        # interactive graph drawer
        self._intGraphDrawer=IntGraphDrawer(self)

        # toolbar
        # toolbar = NavigationToolbar2Tk(self._canvas, self, pack_toolbar=False)
        # toolbar.update()
        # toolbar.grid(row=4, column=0, sticky="nswe")

    def getGrapheDeComposition(self):
        return self._intGraphDrawer.getGrapheDeComposition()

    def isMoveEnable(self)->bool:
        return self.move_button.isEnable()




