from tkinter import Frame

from GUI.tk.ObjectBuilder.ComboSelectorFrame import ComboSelectorFrame

from GUI.plot.IntObjectBuilder.IntNewGrapheDeCompositionFrame import IntNewGrapheDeCompositionFrame


class IntNewObjectSelectorFrame(ComboSelectorFrame):
    # _controller=None

    def __init__(self, parent, controller, *args, **kwargs):
        assert controller and parent
        self._controller=controller
        ComboSelectorFrame.__init__(self, parent=parent, titre="Nouvel Objet", *args, **kwargs)

    #@overwrite
    def selectNewObjectFrame(self, event):
        self._Frames[self._combo.get()]()

    def _initFrame(self):
        # New Object Frames
        self._Frames[""]=lambda: None
        self._Frames["Graphe de composition"]=lambda: IntNewGrapheDeCompositionFrame(None, self._controller)



























