import networkx as nx
from typing import Callable
import itertools

from GUI.ModelObjectsBuilder import ModelObjectsBuilder
from GUI.plot.GraphGenerator import GraphGenerator
from GUI.plot.NodeSelector import NodeSelector
from GUI.plot.GraphDrawer import GraphDrawer
from GUI.plot.CameraFactory import CameraFactory
import GUI.plot.IntObjectBuilder.IntNewGrapheDeCompositionFrame as ng


class IntGraphDrawer(GraphDrawer):
    # eventManager=None
    #
    # axes=None
    # canvas=None
    #
    # graph=None
    # pos=None
    # node_color=None

    def __init__(self, plot_frame):
        assert plot_frame
        self.plot_frame=plot_frame
        self.controlFrame=plot_frame.parent
        self.init_gc_pos=plot_frame.init_gc_pos

        GraphDrawer.__init__(self, plot_frame.plot, plot_frame.canvas)

        def isBuildEnable():
            return not self.plot_frame.isMoveEnable()
        # Event Manager
        self._eventManager=EventManager(self, isBuildEnable)

        # Init data
        self.gc=None
        self.data=None
        self.objets=[]
        self.pos={}
        self.morphismes=[]

        if self.init_gc_pos is not None:
            self.gc=self.init_gc_pos[0]
            self.pos=self.init_gc_pos[2]
            self.buildObjectsMorphismesFromGraph(self.gc)
            self.setGraph(self.init_gc_pos[1], pos=self.pos)

    def updateGraph(self):
        self.gc=ModelObjectsBuilder.buildGrapheDeCompositionFromStr(objets=self.objets, morphismes=self.morphismes)
        self.setGraph(GraphGenerator.give(self.gc), pos=self.pos)

    def newObject(self, x, y):
        objControler=self.controlFrame.getObjectEntry()
        objName=objControler.getText()
        self.objets.append(objName)
        objControler.pushToHistory(objName)
        self.pos[objName]=(x,y)
        self.updateGraph()

    def newFleche(self, source, cible):
        morphControler=self.controlFrame.getFlecheEntry()
        morphName=morphControler.getText()
        if morphName not in [m[2] for m in self.morphismes]:
            self.morphismes.append([source, cible, morphName])
            morphControler.pushToHistory(morphName)
            self.updateGraph()
        else:
            print(f"WARNING: {morphName} already used.")

    def getGrapheDeComposition(self):
        return self.gc

    def buildObjectsMorphismesFromGraph(self, gc, limite_fleches=10, complet=True):
        self.objets=[o for o in gc.objets]
        self.morphismes=[]

        fleches_elem = set(gc[gc.objets,gc.objets])
        if complet:
            func = lambda x,y : gc({x},{y})
        else:
            func = lambda x,y : gc[{x},{y}]
        for source,cible in itertools.product(gc.objets,repeat=2):
            nb_fleches = 0
            for fleche in func(source,cible):
                if not fleche.is_identite:# important !
                    if fleche.source not in gc.objets:
                        a = fleche.source not in gc.objets
                        raise Exception("Source d'une fleche pas dans les objets de la categorie "+repr(fleche.source)+"\n"+str(list(gc.objets))+"\n"+str(fleche.source in gc.objets))
                    if fleche.cible not in gc.objets:
                        raise Exception("Source d'une fleche pas dans les objets de la categorie."+str(fleche.cible)+"\n"+str(gc.objets))
                    if len({obj for obj in gc.objets if obj == fleche.source}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est source de la fleche.")
                    if len({obj for obj in gc.objets if obj == fleche.cible}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est cible de la fleche.")
                    source = {obj for obj in gc.objets if obj == fleche.source}.pop() #permet d'avoir toujours un objet de la catégorie comme source
                    cible = {obj for obj in gc.objets if obj == fleche.cible}.pop() #permet d'avoir toujours un objet de la catégorie comme cible
                    self.morphismes.append([source, cible, str(fleche)])
                    nb_fleches += 1
                if nb_fleches > limite_fleches:
                    if config.WARNING_LIMITE_FLECHES_ATTEINTE:
                        print("Warning : limite fleches entre "+str(source)+" et "+str(cible)+" atteinte.")
                        break
        


class EventManager:
    # drawer=None

    def __init__(self, drawer, isBuildEnable:Callable):
        self.drawer = drawer
        self.isBuildEnable=isBuildEnable

        self._selectedObject=None
        # Events connections
        self.drawer.canvas.mpl_connect('button_press_event', self._button_press_event)
        self.drawer.canvas.mpl_connect('button_release_event', self._button_release_event)
        CameraFactory.addScrollZoom(self.drawer.canvas)

    def _trySelectObject(self, event, updateDraw=True):
        selection = NodeSelector.trySelectNode(event, self.drawer.data)
        if selection and updateDraw:
            NodeSelector.updateSelectedNode(selection,
                                            self.drawer.graph,
                                            self.drawer.node_color)
            self.drawer.drawGraph()
        return selection

    # MLP COONNECT

    def _button_press_event(self, event):
        if not event.inaxes or not self.isBuildEnable():
            return

        mode=self.drawer.controlFrame.getCurrentMode()
        if mode==ng.Mode.OBJET:
            self._selectedObject=self._trySelectObject(event)
            if not self._selectedObject:
                self._newObject(event)
        elif mode==ng.Mode.FLECHE:
            self._selectedObject=self._trySelectObject(event)
        else:
            pass


    def _button_release_event(self, event):
        if (not self.isBuildEnable()) or (not event.inaxes) or (not self._selectedObject):
            return
        mode=self.drawer.controlFrame.getCurrentMode()
        if mode==ng.Mode.FLECHE:
            cible=self._trySelectObject(event)
            if cible:
                self._newFleche(self._selectedObject, cible)
        else:
            pass


    # NEW OBJECTS

    def _newObject(self, event):
        self.drawer.newObject(event.xdata, event.ydata)

    def _newFleche(self, source, cible):
        self.drawer.newFleche(source[3], cible[3])


