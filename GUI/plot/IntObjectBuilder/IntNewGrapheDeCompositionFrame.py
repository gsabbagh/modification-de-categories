from enum import Enum

from tkinter import TOP, BOTTOM, LEFT, RIGHT, X, VERTICAL, END, INSERT, \
                    LabelFrame, Toplevel, Frame, Label, Text, Scrollbar, Button, Spinbox, \
                    Checkbutton, BooleanVar

from GUI.tk.Factory import *

from GUI.tk.ObjectBuilder.NewObjectFrame import NewObjectFrame
from GUI.tk.ObjectBuilder.EntryFieldFrame import IncrementalEntryWithHistory

from GUI.plot.IntObjectBuilder.IntPlotFrame import IntPlotFrame

from typing import Union
from GUI.ConfigParserManager import ConfigParserManager
from GUI.Config import config

class IntGraphConfig(object):

    def __init__(self, config:Union[ConfigParserManager, None]=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if config:
            confSec=config.getConfigSectionMap("Default")
            self.highlight_button_color=confSec["highlight_button_color"]
        else:
            self.highlight_button_color="#649BB4"

ing_config=IntGraphConfig(config)


class Mode(Enum):
    """
    Pour définir le mode courant afin d'interpréter les événements
    """
    OBJET=0
    FLECHE=1



class IntNewGrapheDeCompositionFrame(Toplevel):

    # self._controller

    def __init__(self, parent, controller, init_gc_pos:tuple=None, *args, **kwargs):
        Toplevel.__init__(self, parent, **kwargs)
        self.init_gc_pos=init_gc_pos
        self.controller=controller
        self._initFrame()

    def _initFrame(self):
        self.rowconfigure(0, weight=0, minsize=20)
        self.rowconfigure(1, weight=0, minsize=20)
        self.rowconfigure(2, weight=1, minsize=20)
        self.rowconfigure(3, weight=1, minsize=20)

        self.columnconfigure(0, weight=1, minsize=20)

        Factory.buildSectionLabel(self,"Nouveau graphe de composition").grid(row=0, column=0, columnspan = 1, sticky="nwe")
        self._initOutilsFrame()
        self._initPlotFrame()
        self._initDialogFrame()

    def _initOutilsFrame(self):
        frame=Frame(self)
        frame.grid(row=1, column=0, sticky="sew")

        frame.columnconfigure(0, weight=1, minsize=50)
        frame.columnconfigure(1, weight=1, minsize=30)
        frame.columnconfigure(2, weight=1, minsize=50)
        frame.columnconfigure(3, weight=1, minsize=30)

        self._checkObject = BooleanVar()
        self._checkObject.set(True)
        self.objectButton=Checkbutton(frame,
                            text="Objet",
                            variable = self._checkObject,
                            command=self.selectObject)
        self.objectButton.grid(row=0, column=0, sticky="ne")

        self.objectEntry=IncrementalEntryWithHistory(frame)
        self.objectEntry.grid(row=0, column=1, sticky="ne")

        self._checkFleche = BooleanVar()
        self._checkFleche.set(False)
        self.flecheButton=Checkbutton(frame,
                            text="Flèche",
                            variable = self._checkFleche,
                            command=self.selectFleche)
        self.flecheButton.grid(row=0, column=2, sticky="ne")

        self.flecheEntry=IncrementalEntryWithHistory(frame, alphabetical=True)
        self.flecheEntry.grid(row=0, column=3, sticky="ew")

    def _initPlotFrame(self):
        self._intPlotFrame=IntPlotFrame(self)
        self._intPlotFrame.grid(row=2, column=0, sticky="nsew")

    def _initDialogFrame(self):
        frame=Frame(self)
        frame.grid(row=3, column=0, sticky="sew")

        frame.columnconfigure(0, weight=1, minsize=20)
        frame.columnconfigure(1, weight=1, minsize=20)

        annulerButton=Button(frame, text="Annuler", command=self.destroy)
        annulerButton.grid(row=0, column=0, sticky="e")

        validerButton=Button(frame, text="Valider",
                        highlightcolor=ing_config.highlight_button_color,
                        command=self.validate)
        validerButton.grid(row=0, column=1, sticky="e")
        validerButton.focus_set()
        validerButton.bind('<Return>', self.validate)

    def getCurrentMode(self):
        if self._checkObject.get():
            return Mode.OBJET
        else:
            return Mode.FLECHE

    def getObjectEntry(self):
        return self.objectEntry

    def getFlecheEntry(self):
        return self.flecheEntry

    """
        BIND FUNCTIONS
    """
    def selectObject(self, event=None):
        self._checkFleche.set(not self._checkObject.get())

    def selectFleche(self, event=None):
        self._checkObject.set(not self._checkFleche.get())

    def validate(self, event=None):
        gc=self._intPlotFrame.getGrapheDeComposition()
        if gc:
            self.controller.getModel().addObject(gc)
        self.destroy()



















