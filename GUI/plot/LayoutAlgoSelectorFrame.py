"""
    *** NetworkX: 10.4 Graph Layout ***

bipartite_layout(G, nodes[, align, scale, …])   Position nodes in two straight lines.
circular_layout(G[, scale, center, dim])        Position nodes on a circle.
kamada_kawai_layout(G[, dist, pos, weight, …])  Position nodes using Kamada-Kawai path-length costfunction.
planar_layout(G[, scale, center, dim])          Position nodes without edge intersections.
random_layout(G[, center, dim, seed])           Position nodes uniformly at random in the unit square.
rescale_layout(pos[, scale])                    Returns scaled position array to (-scale, scale) in all axes.
rescale_layout_dict(pos[, scale])               Return a dictionary of scaled positions keyed by node
shell_layout(G[, nlist, rotate, scale, …])      Position nodes in concentric circles.
spring_layout(G[, k, pos, fixed, …])            Position nodes using Fruchterman-Reingold forcedirected algorithm.
spectral_layout(G[, weight, scale, center, dim])    Position nodes using the eigenvectors of the graph Laplacian.
spiral_layout(G[, scale, center, dim, …])           Position nodes in a spiral layout.
multipartite_layout(G[, subset_key, align, …])  Position nodes in layers of straight lines.

"""
import numpy as np
from typing import Union

from tkinter import Frame, Spinbox, Label

from GUI.tk.ObjectBuilder.ComboSelectorFrame import ComboSelectorFrame
from GUI.tk.ObjectBuilder.EntryFieldFrame import ObjectEntryField, SeedEntryField

import networkx as nx



class LayoutAlgoSelectorFrame(ComboSelectorFrame):
    """
        Permet de séléctionner les algorithmes et paramètres paramètres de spacialisation des graphes.
    """

    def __init__(self, parent, debug=True, *args, **kwargs):
        assert parent
        self._debug=debug
        ComboSelectorFrame.__init__(self, parent=parent, titre="Algorithme de spacialisation", *args, **kwargs)


    def _initFrame(self):
        # New Object Frames
        self._Frames["random_layout"]=SeedEntryField(self)
        self._Frames["spring_layout"]=self._Frames["random_layout"]
        self._Frames["planar_layout"]=self._Frames["random_layout"]
        self._Frames["bipartite_layout"]=ObjectEntryField(self, description=None, generateur=False)
        self._Frames["circular_layout"]=self._Frames["random_layout"]
        self._Frames["kamada_kawai_layout"]=self._Frames["random_layout"]
        self._Frames["shell_layout"]=self._Frames["random_layout"]


    def getPositions(self, graph)->dict:
        """
            @param G [NetworkX graph or list of nodes]

            @return [dict] A dictionary of positions keyed by node
        """
        algo=self._combo.get()

        if(algo=="random_layout"):
            seed=self._Frames[algo].getSeed()
            if not seed:
                seed=np.random.randint(2147483647)
            self._Frames[algo].pushToHistory(seed)
            return nx.random_layout(graph, seed=seed)
        elif(algo=="spring_layout"):
            return nx.spring_layout(graph)
        elif(algo=="planar_layout"):
            if callable(getattr(graph,"check_planarity",None)) and graph.check_planarity()[0]:
                return nx.planar_layout(graph)
            else:
                if self._debug:
                    print("--- WARNING: no check_planarity or negative one, random_layout used.")
                return nx.random_layout(graph)
        elif(algo=="bipartite_layout"):
            return nx.bipartite_layout(graph, self._Frames[algo].getList())
        elif(algo=="circular_layout"):
            return nx.circular_layout(graph)
        elif(algo=="kamada_kawai_layout"):
            return nx.kamada_kawai_layout(graph)
        elif(algo=="shell_layout"):
            return nx.shell_layout(graph)
        else:
            return nx.random_layout(graph)




class BipartiteLayoutFrame(Frame):

    def __init__(self, parent, nodes:Union[list, None]=None):
        Frame.__init__(self, parent)
        self._nodes=nodes
        self._initFrame()

    def _initFrame(self):
        self.columnconfigure(0, weight=0, minsize =30)
        self.columnconfigure(1, weight=1, minsize =30)
        self.rowconfigure(0, weight=1, minsize=20)

        self._label=Label(self, text="Node : ")
        self._label.grid(row=0, column=0, sticky="nsew")

        self._sb=Spinbox(self)
        self._sb.grid(row=0, column=1, sticky="nsew")

    def getNodes(self)->list:
        return [self._sb.get()]






















