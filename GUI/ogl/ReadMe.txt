"""
	Description du dossier [09-2021]

	Ce dossier contient la base nécessaire à la création d'une fenêtre OpenGl.
	L'affichage des garphes de composition (dont la spacialisation est déterminée par un algorithme de NetworkX) n'est pas implémentée, il ne s'agit que d'une démonstration de base.
"""


############################# Code d'incorporation

/* in MainWindow */

from ogl.AppOgl import AppOgl

    _glFrame=None
    ...
    
    def _initFrames(self):
	...
        # OGL frame
        self._glFrame=Frame(self._frame)
        self._glFrame.grid(row=0, column=0, sticky="nesw")
        self.initGLApp()
	...

    def initGLApp(self):
        self._glApp=AppOgl(self._glFrame, width=320, height=200)
        self._glApp.pack(expand = YES, fill=BOTH)