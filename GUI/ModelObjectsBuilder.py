""" *************************************
    -------------------------------------
    | UTC - TX - Théorie des catégories |
    |           2021 - F.M.             |
    -------------------------------------

class:
    [static] ModelObjectsBuilder
        Constructeur des objets contenus dans le modèle.
    Description
        Constructeur de la description des objets pour leur affichage. (str)

************************************* """
from typing import Union
from itertools import product
from concurrent.futures import ThreadPoolExecutor

from GrapheDeComposition import GrapheDeComposition, GC, \
                                MorphismeGrapheDeComposition, MGC
from CategorieAleatoire import GrapheCompositionAleatoire, GCA
from EnsFinis import CategorieEnsemblesFinis, EnsFinis, \
                    CategorieEnsembleParties, EnsParties
from Categorie import Categorie
from Interaction import Interaction
from ChampPerceptif import ChampPerceptif




class ModelObjectsBuilder:
    """
    ! La construction d'un objet pouvant être longue, mieux vaut appeler ces méthodes dans un nouveau Thread. !
    """
    @staticmethod
    def buildGrapheDeCompositionFromStr(objets:set = set(),
                                        morphismes:set=set(),
                                        identifications:set=set(),
                                        nom:str = None)->GC:
        # objets
        gc=GC(objets=objets, nom=nom)

        # morphismes
        dic={str(x[2]):MGC(x[0],x[1],x[2]) for x in morphismes}
        gc|= list(dic.values())

        # identifications
        if identifications:
            func = lambda x,y : gc({x},{y})
            fleches={}

            for source,cible in product(gc.objets,repeat=2):
                for fleche in func(source,cible):
                    S={obj for obj in gc.objets if obj == fleche.source}
                    C={obj for obj in gc.objets if obj == fleche.cible}
                    if len(S) ==0:
                        raise Exception("Aucun objet de la catégorie est source.")
                    if len(C) ==0:
                        raise Exception("Aucun objet de la catégorie est cible.")
                    if len(S) > 1:
                        raise Exception("Plus d'un objet de la catégorie est source.")
                    if len(C) > 1:
                        raise Exception("Plus d'un objet de la catégorie est cible.")
                    fleches[str(fleche)]=fleche


            # vérifier que les morphismes existent
            for x in identifications:
                x[0]=[fleches.get(m) for m in x[0]]
                x[1]=[fleches.get(m) for m in x[1]]
                if (None in x[0]) or (None in x[1]):
                    print(f"WARNING: {x} n'est pas une identification valide.")
                else:
                    compoG=x[0][0]
                    for m in x[0][1:]:
                        compoG @=m
                    compoD=x[1][0]
                    for m in x[1][1:]:
                        compoD @=m
                    MGC.identifier_morphismes(compoG,compoD)

        return gc

    @staticmethod
    def buildGrapheDeCompositionAleatoireFromStr(nb_fleches:str,
                    nb_tentatives_complexification_loi_de_compo:str,
                    nb_tentatives_correction_composition:str,
                    proba_identifier_identites:str,
                    seed:Union[int,None]=None,
                    nom:Union[str,None] = None):
        if seed:
            random.seed(seed)

        gca=GrapheCompositionAleatoire(nb_fleches=ModelObjectsBuilder.toIntOrNone(nb_fleches),
            nb_tentatives_complexification_loi_de_compo=ModelObjectsBuilder.toIntOrNone(nb_tentatives_complexification_loi_de_compo),
            nb_tentatives_correction_composition=ModelObjectsBuilder.toIntOrNone(nb_tentatives_correction_composition),
            proba_identifier_identites=float(proba_identifier_identites))
        return gca

    @staticmethod
    def buildEnsFinisFromSet(self, ensembles:set = set(), nom:str = None):
        return EnsFinis(ensembles, nom)

    @staticmethod
    def buildEnsPartiesFromSet(self, ensemble:set = set(), nom:str = None):
        return CategorieEnsembleParties(ensemble, nom)


    """
        UTILS
    """
    @staticmethod
    def toIntOrNone(s:str)->Union[int,None]:
        if s=="":
            return None
        else:
            return int(s)









class Description:
    """
        Produit des descriptions d'un objet pour son affichage.
    """

    def __init__(self, obj:object, *args, **kwargs):
        self._obj=obj
        with ThreadPoolExecutor(max_workers=1) as executor:
            self._str = executor.submit(self.generer).result()

    def __str__(self):
        return self._str

    def generer(self):
        if issubclass(type(self._obj), Categorie):
            return self._genererDescriptionCategorie()
        else:
            return str(self._obj)

    def _genererDescriptionCategorie(self, afficher_identites:bool = True, complet:bool = True, limite_fleches:int = 30):
        """
        `complet` indique s'il faut afficher toutes les flèches ou seulement les flèches élémentaires.
        `limite_fleches` est le nombre maximal de flèches entre deux objets qu'on s'autorise à afficher.
        """
        descr=""

        # objets
        descr+=" {"
        for o in self._obj.objets:
            descr+=str(o)+","
        if(len(self._obj.objets)>0):
            descr=descr[:-1]+"}"
        else:
            descr+="}"

        # fleches
        fleches_elem = set(self._obj[self._obj.objets,self._obj.objets])
        if complet:
            func = lambda x,y : self._obj({x},{y})
        else:
            func = lambda x,y : self._obj[{x},{y}]

        fleches_elementaires=[]
        fleches_non_elementaires=[]

        for source,cible in product(self._obj.objets,repeat=2):
            nb_fleches = 0
            for fleche in func(source,cible):
                if afficher_identites or not fleche.is_identite:
                    if fleche.source not in self._obj.objets:
                        raise Exception("Source d'une fleche pas dans les objets de la categorie.")
                    if fleche.cible not in self._obj.objets:
                        raise Exception("Source d'une fleche pas dans les objets de la categorie.")
                    if len({obj for obj in self._obj.objets if obj == fleche.source}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est source de la fleche.")
                    if len({obj for obj in self._obj.objets if obj == fleche.cible}) > 1:
                        raise Exception("Plus d'un objet de la catégorie est cible de la fleche.")
                    #permet d'avoir toujours un objet de la catégorie comme source
                    source = {obj for obj in self._obj.objets if obj == fleche.source}.pop()
                    #permet d'avoir toujours un objet de la catégorie comme cible
                    cible = {obj for obj in self._obj.objets if obj == fleche.cible}.pop()
                    # descriptions
                    if fleche in fleches_elem:
                        fleches_elementaires.append(f"({str(source)},{str(cible)},{str(fleche)})")
                    else:
                        fleches_non_elementaires.append(f"({str(source)},{str(cible)},{str(fleche)})")
                    nb_fleches += 1
                if nb_fleches > limite_fleches:
                    if WARNING_LIMITE_FLECHES_ATTEINTE:
                        print("Warning : limite fleches entre "+str(source)+" et "+str(cible)+" atteinte.")
                        break
        descr+=" {"
        flag=False
        for f in fleches_non_elementaires:
            descr+=f+","
            flag=True
        for f in fleches_elementaires:
            descr+=f+","
            flag=True
        if flag:
            descr=descr[:-1]+"}"
        else:
            descr+="}"
        return descr













