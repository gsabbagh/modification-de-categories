import pandas as pd
from enum import Enum, unique
from typing import Union
from itertools import product
from concurrent.futures import ThreadPoolExecutor

from GUI.ModelCallable import ModelCallable

from GrapheDeComposition import GrapheDeComposition, GC, \
                                MorphismeGrapheDeComposition, MGC
from CategorieAleatoire import GrapheCompositionAleatoire, GCA
from EnsFinis import CategorieEnsemblesFinis, EnsFinis, \
                    CategorieEnsembleParties, EnsParties
from Categorie import Categorie
from Interaction import Interaction
from ChampPerceptif import ChampPerceptif

from GUI.ModelObjectsBuilder import ModelObjectsBuilder

@unique
class ModelObjects(Enum):
    GC="Graphe de Composition"
    GCA="Graphe de composition aléatoire"
    EF="sous-Cat EnsFinis"
    EP="EnsParties"
    CI="Catégorie Indéxante"
    It="Intéraction"
    CP="Champ Perceptif"

    def get(obj):
        t=type(obj)
        if t==GrapheDeComposition:
            return ModelObjects.GC
        elif t==EnsFinis or t==CategorieEnsemblesFinis:
            return ModelObjects.EF
        elif t==EnsParties or t==CategorieEnsembleParties:
            return ModelObjects.P
        elif t==Categorie:
            return ModelObjects.CI
        elif t==Interaction:
            return ModelObjects.It
        elif t==ChampPerceptif:
            return ModelObjects.CP
        else:
            return None

class Model:
    """
        Contient tous les objets construits.
    """

    _data=None
    _tmpObject=None
    def getTmpObject(self): return self._tmpObject
    def setTmpObject(self, obj): self._tmpObject=obj

    def __init__(self):
        self._data=pd.DataFrame(columns=["class","object"])
        self._data["class"]=self._data["class"].astype("string")

    """
        CREATION D'OBJETS
    """
    def addObject(self, obj:object):
        newData={"class": type(obj).__name__,
                    "object": obj}
        self._data=self._data.append(newData, ignore_index=True)
        self.informObjectAdded(self._data.index[-1], obj)

    def addObjects(self, objects:list):
        newData=[]
        for obj in objects:
            newData.append({"class": type(obj).__name__,
                            "object": obj})
        i=self._data.index[-1]
        self._data=self._data.append(newData, ignore_index=True)
        self.informObjectsAdded(list(range(i, self._data.index[-1]+1)))

    def createGrapheDeComposition(self, objets:set = set(), morphismes:set=set(), identifications:set=set(), nom:str = None):
        with ThreadPoolExecutor() as executor:
            executor.submit(self.addObject(ModelObjectsBuilder.buildGrapheDeCompositionFromStr(objets=objets,
                                            morphismes=morphismes,
                                            identifications=identifications,
                                            nom=nom)))


    def createGrapheDeCompositionAleatoire(self,
                                            nb_fleches:str,
                                            nb_tentatives_complexification:str,
                                            nb_tentatives_correction_composition:str,
                                            proba_identifier_identites:str,
                                            seed:Union[int,None]=None,
                                            nom:Union[str,None] = None):

        with ThreadPoolExecutor() as executor:
            executor.submit(self.addObject(ModelObjectsBuilder.buildGrapheDeCompositionAleatoireFromStr(nb_fleches=nb_fleches, nb_tentatives_complexification_loi_de_compo=nb_tentatives_complexification, nb_tentatives_correction_composition=nb_tentatives_correction_composition, proba_identifier_identites=proba_identifier_identites)))


    def createEnsFinisFromSet(self, ensembles:set = set(), nom:str = None):
        with ThreadPoolExecutor() as executor:
            executor.submit(self.addObject(ModelObjectsBuilder.buildEnsFinisFromSet(ensembles)))

    def createEnsPartiesFromSet(self, ensemble:set = set(), nom:str = None):
        with ThreadPoolExecutor() as executor:
            executor.submit(self.addObject(ModelObjectsBuilder.buildEnsPartiesFromSet(ensemble, nom)))




    """
        GETTERS
    """
    def getData(self):
        return self._data

    def getObject(self, uid):
        """
        @return DataFrame
        """
        return self._data.iloc[uid]

    """
        INFORM
    """
    _registeredModelCallable=[]
    def registerModelCallable(self, mc:ModelCallable):
        self._registeredModelCallable.append(mc)

    def informDataModified(self):
        for rdm in self._registeredModelCallable:
            rdm.informDataModified()

    def informObjectAdded(self, uid, obj:object):
        for rdm in self._registeredModelCallable:
            rdm.informObjectAdded(uid, obj)

    def informObjectDeleted(self, uid):
        for rdm in self._registeredModelCallable:
            rdm.informObjectDeleted(uid)

    def informObjectsAdded(self, uids:list):
        for rdm in self._registeredModelCallable:
            rdm.informObjectsAdded(uids)

    def informObjectsDeleted(self, uids:list):
        for rdm in self._registeredModelCallable:
            rdm.informObjectsDeleted(uids)

    """
        Graphviz
    """
    def exporterGraphviz(self, uids):
        data=self.getObject(uids)
        with ThreadPoolExecutor(max_workers=len(data)) as executor:
            for index, row in data.iterrows():
                obj=row["object"]
                if callable(getattr(obj,"transformer_graphviz",None)):
                    executor.submit(obj.transformer_graphviz())








