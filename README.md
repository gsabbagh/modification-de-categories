# Modification de catégories par adjonction de limites

*TN10 de Guillaume Sabbagh encadré par Vincent Robin*

## Installation

1. Installer python3+ https://www.python.org/downloads/
2. Installer graphviz https://www.graphviz.org/download/
3. Ajouter graphviz dans le path 
    + MACOS : https://www.pegaxchange.com/2017/06/17/mac-os-x-path-variable/
    + Windows : https://helpdeskgeek.com/windows-10/add-windows-path-environment-variable/
4. Run la commande "python3 -m pip install graphviz" dans un terminal
