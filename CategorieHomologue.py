from Categorie import Categorie, mise_en_cache_call
from Foncteur import Foncteur
from CatFinies import CatFinies
from EnsFinis import CategorieBijections,Bijection
from ChampActif import ChampActif,Cocone
from collections import defaultdict
import itertools
from ProduitGenerateurs import produit_cartesien_generateurs
from typing import *



class FoncteurOubli(Foncteur):
    """Foncteur d'oubli du champ actif.
    Associe à chaque cône sont nadir et à chaque flèche entre cône la flèche d'où elle vient."""
    
    def __init__(self, champ_actif:ChampActif):
        univers = champ_actif.foncteur_diagonal.source
        Foncteur.__init__(self, champ_actif, univers, {obj:obj.d for obj in abs(champ_actif)},
        {f:f.k for f in champ_actif(abs(champ_actif),abs(champ_actif))})
       
class CategorieHomologue(CatFinies):
    """Catégorie dont les objets sont des champs actifs et les morphismes sont des foncteurs bijectifs qui commutent avec le foncteur d'oubli."""
    
    def verifier_coherence(self):
        CatFinies.verifier_coherence(self)
        for f in self(abs(self),abs(self)):
            fonct_oubli_source = FoncteurOubli(f.source)
            fonct_oubli_cible = FoncteurOubli(f.cible)
            if fonct_oubli_source != fonct_oubli_cible@f:
                raise Exception("Incoherence CategorieHomologue : "+str(f)+" ne commute pas avec les foncteurs d'oublis")
    
    @mise_en_cache_call
    def __call__(self, sources:set, cibles:set) -> Generator[Foncteur,None,None]:
        for source in sources:
            for cible in cibles:
                dict_nadir_cocone_source = defaultdict(frozenset)
                for obj in abs(source):
                    dict_nadir_cocone_source[Cocone(obj).nadir] |= {obj}
                dict_nadir_cocone_cible = defaultdict(frozenset)
                for obj in abs(cible):
                    dict_nadir_cocone_cible[Cocone(obj).nadir] |= {obj}
                if set(dict_nadir_cocone_source.keys()) != set(dict_nadir_cocone_cible.keys()):
                    continue
                dict_fleches_k_source = defaultdict(lambda: defaultdict(frozenset)) #{(a,b):{k:{flecheComma1(k),flecheComma2(k),...}}}
                for a,b in itertools.product(source.objets,repeat=2):
                    for fleche in source[{a},{b}]:
                        dict_fleches_k_source[(a,b)][fleche.k] |= {fleche}
                
                dict_fleches_k_cible = defaultdict(lambda: defaultdict(frozenset)) #{(a,b):{k:{flecheComma1(k),flecheComma2(k),...}}}
                for a,b in itertools.product(cible.objets,repeat=2):
                    for fleche in cible[{a},{b}]: #on peut viser que les flèches elem puisqu'on doit commuter avec le foncteur d'oubli qui mène des flèches elem à des flèches elem
                        dict_fleches_k_cible[(a,b)][fleche.k] |= {fleche}     
                        
                bijections_objets = set() # bijections entre cônes de même nadir
                cat_bij = CategorieBijections()
                for nadir in dict_nadir_cocone_source:
                    cat_bij |= {dict_nadir_cocone_source[nadir]}
                    cat_bij |= {dict_nadir_cocone_cible[nadir]}
                    bijections = cat_bij({dict_nadir_cocone_source[nadir]},{dict_nadir_cocone_cible[nadir]})
                    bijections_objets |= {bijections}
                for bijection_objet in produit_cartesien_generateurs(*bijections_objets):
                    bij_obj = dict([x for bij in bijection_objet for x in bij.as_dict().items()])
                    bijections_fleches = set()
                    for a,b in itertools.product(source.objets,repeat=2):
                        for k in dict_fleches_k_source[(a,b)]:
                            cat_bij |= {dict_fleches_k_source[(a,b)][k],dict_fleches_k_cible[(bij_obj[a],bij_obj[b])][k]}
                            bijections = cat_bij({dict_fleches_k_source[(a,b)][k]},{dict_fleches_k_cible[(bij_obj[a],bij_obj[b])][k]})
                            bijections_fleches |= {bijections}      



                    bijections_fleches = set()
                    for a,b in itertools.product(source.objets,repeat=2):
                        for k in dict_fleches_k_source[(a,b)]:
                            cat_bij |= {dict_fleches_k_source[(a,b)][k],dict_fleches_k_cible[(bij_obj[a],bij_obj[b])][k]}
                            bijections = cat_bij({dict_fleches_k_source[(a,b)][k]},{dict_fleches_k_cible[(bij_obj[a],bij_obj[b])][k]})
                            bijections_fleches |= {bijections}
                    for bijection_fleche in produit_cartesien_generateurs(*bijections_fleches):
                        bij_fleche = dict([x for bij in bijection_fleche for x in bij.as_dict().items()])
                        yield Foncteur(source,cible,bij_obj,bij_fleche)
                        
                        
    def isomorphismes(self, source:ChampActif, cible:ChampActif) -> Generator[tuple,None,None]:
        """Trouve les isomorphismes entre `source` et `cible`.
        Renvoie un générateur de couples {f,g} tels que gof = Id1 et fog = Id2, et f:source->cible.
        """
        for f in self({source},{cible}):
            g = Foncteur(cible,source,{f(obj):obj for obj in source.objets},{f(morph):morph for morph in source[source.objets,source.objets]})
            yield (f,g)
        
def test_FoncteurOubli():
    from GrapheDeComposition import GC,MGC
    from Diagramme import Triangle,DiagrammeIdentite
    
    gc = GC()
    gc |= set("ABCDEF")
    f,g,h,i,j,k,l = [MGC('A','B','f'),MGC('B','C','g'),MGC('D','E','h'),MGC('E','F','i'),MGC('A','D','j'),MGC('B','E','k'),MGC('C','F','l')]
    gc |= {f,g,h,i,j,k,l}
    
    # diag_identite = DiagrammeIdentite(gc)
    # diag_identite.faire_commuter()
    
    d1 = Triangle(gc,"DEF",[h,i,i@h])
    
    c_p = ChampActif(d1)
    
    foncteur_oubli = FoncteurOubli(c_p)
    foncteur_oubli.transformer_graphviz()
    
def test_CategorieHomologue():
    from GrapheDeComposition import GC,MGC
    from Diagramme import Fleche,DiagrammeIdentite
    
    gc = GC()
    gc |= set("ABCDEFGH")
    f,g,h,i,j,k,l,m,n = [MGC('A','B','f'),MGC('B','C','g'),MGC('D','E','h'),MGC('E','F','i'),MGC('A','D','j'),MGC('B','E','k'),MGC('C','F','l'),MGC('G','H','m'),MGC('F','H','n')]
    gc |= {f,g,h,i,j,k,l,m,n}
    
    diag_identite = DiagrammeIdentite(gc)
    diag_identite.faire_commuter()
    
    d1 = Fleche(gc,m)
    c_p1 = ChampActif(d1)
    d2 = Fleche(gc,n)
    c_p2 = ChampActif(d2)
    c_p1.transformer_graphviz()
    c_p2.transformer_graphviz()
    
    cph = CategorieHomologue({c_p1,c_p2})
    cph.transformer_graphviz()
    
    f,g = set(cph.isomorphismes(c_p1,c_p2)).pop()
    f.transformer_graphviz()
    g.transformer_graphviz()
    
def test_CategorieHomologue2():
    from GrapheDeComposition import GC,MGC
    from Diagramme import DiagrammeObjets
    
    gc = GC()
    gc |= set("ABCDEFGHIJ")
    f,g,h,i,j,k,l,m,n,o,p,q,r,s = [MGC('C','A','f'),MGC('C','B','g'),MGC('D','A','h'),MGC('D','B','i'),MGC('E','A','j'),MGC('E','B','k'),MGC('F','A','l'),MGC('F','B','m'),
    MGC('A','G','n'),MGC('A','H','o'),MGC('B','H','p'),MGC('B','I','q'),MGC('I','J','r'),MGC('J','J','s')]
    gc |= {f,g,h,i,j,k,l,m,n,o,p,q,r,s}
    MGC.identifier_morphismes(s@s,s)
    gc.transformer_graphviz()
    gc.transformer_graphviz(complet=False)
    
    
    d1 = DiagrammeObjets(gc,set("CD"))
    d2 = DiagrammeObjets(gc,set("EF"))
    c_p1 = ChampActif(d1)
    c_p2 = ChampActif(d2)
    c_p1.transformer_graphviz()
    c_p2.transformer_graphviz()
    
    cph = CategorieHomologue({c_p1,c_p2})
    cph.transformer_graphviz()
    
    f,g = set(cph.isomorphismes(c_p1,c_p2)).pop()
    f.transformer_graphviz()
    g.transformer_graphviz()
    
    for cocone in c_p1.objets_cocones("H"):
        Cocone(cocone).transformer_graphviz()
    
if __name__ == '__main__':
    test_FoncteurOubli()
    test_CategorieHomologue()
    test_CategorieHomologue2()