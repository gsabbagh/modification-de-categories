from Morphisme import Morphisme
from Categorie import Categorie, mise_en_cache_getitem
from CategorieLibre import CategorieLibre
import itertools
from config import *
if GRAPHVIZ_ENABLED:
    from graphviz import Digraph
from typing import *

class Application(Morphisme):
    """Une application A de E vers F (A : E->F) est une relation binaire telle que pour tout x de E il existe un y de F tel que F(x) = y <=> (x,y) \in A.
    On représente l'application par un dictionnaire où les clés sont des éléments de E et les valeurs des éléments de F.
    """
    nb_viz = 0
    
    def __init__(self, source:frozenset, cible:frozenset, application:dict):
        Morphisme.__init__(self,source,cible,None, source==cible and application == {x:x for x in source})
        self._app = application
        if TOUJOURS_VERIFIER_COHERENCE:
            self.verifier_coherence()
      
    def as_dict(self) -> dict:
        return self._app
      
    def verifier_coherence(self):
        if len(frozenset(self._app.keys())) != len(self._app.keys()):
            raise Exception("Incoherence Application : un element a plusieurs images "+str(self._app))
        if frozenset(self._app.keys()) != self.source:
            raise Exception("Incoherence Application : l'application n'a pas suffisament ou a trop d'antecedants\n"+str(self._app)+'\n'+str(self.source))
        
    def __eq__(self,other:'Application') -> bool:
        if not issubclass(type(other),Application):
            # raise TypeError("Tentative de comparaison avec un objet de type inconnu "+str(other))
            return False
        return self.source == other.source and self.cible == other.cible and self._app == other._app
        
    def __hash__(self) -> int:
        return hash((self.source,self.cible,frozenset(self._app.items())))
    
    def __call__(self, element:any) -> any:
        """Renvoie l'image de l'`element` par l'application."""
        return self._app[element]
    
    def __matmul__(self, other:'Application') -> 'Application':
        return Application(other.source,self.cible,{e:self(other(e)) for e in other.source})
        
    def __repr__(self):
        return "/".join(map(lambda x:str(x[0])+':'+str(x[1]),self._app.items()))+'\n'
            
        
    def transformer_graphviz(self, destination:any=None):
        Application.nb_viz += 1
        if destination == None:
            destination = "graphviz/"+type(self).__name__+str(Application.nb_viz)
            
        graph = Digraph('application')
        graph.attr(concentrate="true" if GRAPHVIZ_CONCENTRATE_GRAPHS else "false")
        graph.attr(label="Application "+self.nom)
        with graph.subgraph(name='cluster_0') as cluster:
            cluster.attr(label=str(self.source))
            for o in self.source:
                cluster.node(str(o))

        with graph.subgraph(name='cluster_1') as cluster:
            cluster.attr(label=str(self.cible))
            for o in self.cible:
                cluster.node(str(o)+"'")
            
        for source in self._app:
                graph.edge(str(source),str(self(source))+"'")
                
        graph.render(destination)
        if CLEAN_GRAPHVIZ_MODEL:
            import os
            os.remove(destination) 


class CategorieEnsemblesFinis(Categorie):
    """CategorieEnsFinis peut être abrégé en EnsFinis
    Catégorie des ensembles finis, cette catégorie est infinie, on ajoute uniquement les ensembles dont on a besoin.
    /!\ __call__ n'appelle pas __getitem__ /!\ """
    
    def __init__(self, objets:set = set(), nom:str = "Catégorie d'ensembles finis"):
        Categorie.__init__(self,objets,nom)
    
    def identite(self, ensemble:frozenset) -> Application:
        return Application(ensemble,ensemble,{e:e for e in ensemble})
        
    def __call__(self, sources:set, cibles:set) -> Generator[Application,None,None]:
        '''/!\ __call__ n'appelle pas __getitem__ /!\ '''
        for source in sources:
            for cible in cibles:
                if not issubclass(type(source),frozenset):
                    raise Exception("On s'attend à avoir des frozenset dans l'ensemble source, pas un "+str(type(source))+'\n'+str(sources))
                if not issubclass(type(cible),frozenset):
                    raise Exception("On s'attend à avoir des frozenset dans l'ensemble cible, pas un "+str(type(cible))+'\n'+str(cibles))
                for prod in itertools.product(cible,repeat=len(source)):
                    yield Application(source,cible,dict(zip(source,prod)))
    
    def __getitem__(self, couple_sources_cibles:tuple) -> Generator[Application,None,None]:
        '''/!\ __call__ n'appelle pas __getitem__ /!\ '''
        sources,cibles = couple_sources_cibles
        for source in sources:
            for cible in cibles:
                if not issubclass(type(source),frozenset):
                    raise Exception("On s'attend à avoir des frozenset dans l'ensemble source, pas un "+str(type(source))+'\n'+str(sources)+'\n'+str(couple_sources_cibles))
                if not issubclass(type(cible),frozenset):
                    raise Exception("On s'attend à avoir des frozenset dans l'ensemble cible, pas un "+str(type(cible))+'\n'+str(cibles)+'\n'+str(couple_sources_cibles))
                source_liste = list(source)
                cible_liste = list(cible)
                if len(source) == len(cible):
                    cat_bij = CategorieBijections()
                    cat_bij |= {source,cible}
                    for bij in cat_bij[{source},{cible}]:
                        yield bij
                    if len(source) > 0:
                        source_liste, cible_liste = list(source),list(cible)
                        projection = {source_liste[i]:cible_liste[i] for i in range(len(cible)-1)}
                        projection[source_liste[-1]] = cible_liste[0]
                        yield Application(source,cible,projection)
                elif len(source) > len(cible):
                    if len(cible) > 0:
                        yield Application(source,cible,{source_liste[i]:cible_liste[min(i,len(cible)-1)] for i in range(len(source))})
                else:
                    yield Application(source,cible,{source_liste[i]:cible_liste[i] for i in range(len(source))})
                    
                    
                
        
EnsFinis = CategorieEnsemblesFinis

class CategorieEnsembleParties(EnsFinis):
    """CategorieEnsembleParties peut être abrégé en EnsParties
    Catégorie de l'ensemble des parties d'un ensemble, les morphismes sont des applications."""
    
    def __init__(self, ensemble:set, nom:str = None):
        EnsFinis.__init__(self,{frozenset(ens) for i in range(len(ensemble)+1) for ens in itertools.combinations(ensemble,i)},"Catégorie des parties de "+str(ensemble) if nom == None else nom)
            
EnsParties = CategorieEnsembleParties
    
class Bijection(Application):
    def __init__(self, source:frozenset, cible:frozenset, application:dict):
        Application.__init__(self,source,cible,application)
        if TOUJOURS_VERIFIER_COHERENCE:
            self.verifier_coherence()

    def verifier_coherence(self):
        Application.verifier_coherence(self)
        if len(self.source) != len(self.cible):
            raise Exception("Incoherence Bijection : il n'y a pas autant d'element au depart qu'a l'arrivee.")
        if {self(x) for x in self.source} != set(self.cible):
            raise Exception("Incoherence Bijection : toute l'image n'est pas atteinte.")

class CategorieBijections(EnsFinis):
    """`CategorieBijections` peut être abrégé en `Bij`
    Sous-catégorie d'`EnsFinisCatégorie`.
    Les objets sont des ensembles et les morphismes sont des applications bijectives.
    /!\ __call__ n'appelle pas __getitem__ /!\ """

    def __init__(self, objets:set = set(), nom:str = "Catégorie des bijections"):
        EnsFinis.__init__(self,objets,nom)
    
    def __call__(self, sources:set, cibles:set) -> Generator[Bijection,None,None]:
        '''/!\ __call__ n'appelle pas __getitem__ /!\ '''
        for source in sources:
            for cible in cibles:
                if not issubclass(type(source),frozenset):
                    raise Exception("On s'attend à avoir des frozenset dans l'ensemble source, pas un "+str(type(source))+'\n'+str(sources))
                if not issubclass(type(cible),frozenset):
                    raise Exception("On s'attend à avoir des frozenset dans l'ensemble cible, pas un "+str(type(cible))+'\n'+str(cibles))
                source_liste,cible_liste = list(source),list(cible)
                if len(source) != len(cible):
                    continue
                for prod in itertools.permutations(cible_liste):
                    yield Bijection(source,cible,dict(zip(source_liste,prod)))
    
    def __getitem__(self, couple_sources_cibles:tuple) -> Generator[Bijection,None,None]:
        '''/!\ __call__ n'appelle pas __getitem__ /!\ '''
        sources,cibles = couple_sources_cibles
        for source in sources:
            for cible in cibles:
                if len(source) != len(cible):
                    continue
                if len(source) == len(cible) == 0:
                    yield Bijection(source,cible,dict())
                    continue
                if len(source) == len(cible) == 1:
                    yield Bijection(source,cible,{list(source)[0]:list(cible)[0]})
                    continue
                source_liste = list(source)
                cible_liste = list(cible)
                transposition = [cible_liste[1],cible_liste[0]]+cible_liste[2:]
                rotation = [cible_liste[-1]]+cible_liste[:-1]
                yield Bijection(source,cible,{source_liste[j]:transposition[j] for j in range(len(source_liste))})
                yield Bijection(source,cible,{source_liste[j]:rotation[j] for j in range(len(source_liste))})
                
Bij = CategorieBijections
                
def test_EnsFinis():
    cat = EnsFinis(set(map(frozenset,[{},{1,2},{3,4,5},{6}])))
    cat.transformer_graphviz(limite_fleches=243,complet=False)
    cat.transformer_graphviz(limite_fleches=243,complet=True)
    # for source,cible in itertools.product(cat.objets,repeat=2):
        # print(source,cible)
        # print(len(list(cat({source},{cible}))))
        # print()
    # for fleche in cat({frozenset({1,2})},{frozenset({3,4,5})}):
        # fleche.transformer_graphviz()
        
    # for fleche in cat({frozenset({1,2})},{frozenset({})}):
        # print("fleche de {1,2} vers {}")
        # fleche.transformer_graphviz()

def test_EnsParties():
    cat = EnsParties({1,2,3})
    cat.transformer_graphviz(complet=False)
    for app in cat({frozenset({1,2,3})},{frozenset({1,2})}):
        app.transformer_graphviz()

def test_CategorieBijections():
    cat = EnsParties({1,2,3,4})
    cat = CategorieBijections(cat.objets)
    for bij in cat[{frozenset({1,2,3})},{frozenset("123")}]:
        bij.transformer_graphviz()
    cat.transformer_graphviz(limite_fleches=27)

def test_EnsNbFleches():
    cat = EnsFinis({frozenset({1,2,3,4})})
    for app in cat({frozenset({1,2,3,4})},{frozenset({1,2,3,4})}):
        app.transformer_graphviz()
    

if __name__ == '__main__':
    test_EnsFinis()
    test_EnsParties()
    test_CategorieBijections()
    # test_EnsNbFleches()