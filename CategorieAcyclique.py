from Categorie import Categorie
from CategorieQuotient import CategorieQuotient
from Morphisme import Morphisme
import itertools

class CategorieAcyclique(CategorieQuotient):
    """`CategorieAcyclique` peut être abrégé en `CatAcyclique`.
    Catégorie quotientée par la relation d'équivalence sur les objets suivante :
     x ~ y ssi il existe f: x -> y et il existe g: y -> x."""
    
    def __init__(self,categorie_a_quotienter:Categorie, nom:str = None):
        if nom == None:
            nom = "Catégorie acyclique engendrée par "+str(categorie_a_quotienter)    
        CategorieQuotient.__init__(self,categorie_a_quotienter,nom)
        for obj1, obj2 in itertools.product(categorie_a_quotienter.objets,repeat=2):
            if obj1 != obj2:
                if categorie_a_quotienter.existe_morphisme(obj1,obj2) and categorie_a_quotienter.existe_morphisme(obj2,obj1):
                    self.identifier_morphismes(categorie_a_quotienter.identite(obj1),categorie_a_quotienter.identite(obj2))
                    for m in categorie_a_quotienter({obj1},{obj2}):
                        self.identifier_morphismes(m,categorie_a_quotienter.identite(obj2))
                    for m in categorie_a_quotienter({obj2},{obj1}):
                        self.identifier_morphismes(m,categorie_a_quotienter.identite(obj2))
            else:
                for m in categorie_a_quotienter({obj1},{obj1}):
                    self.identifier_morphismes(m,categorie_a_quotienter.identite(obj1))
                    
                    
    def tri_topologique(self) -> list:
        """Renvoie une liste d'objets de la catégorie dans l'ordre topologique.
        Si a vient avant b dans la liste alors il n'existe aucune flèche de b vers a.
        La construction suit le principe suivant : on prend tous les objets sans pères, on les ajoute au tri,
        puis on les retire et on recommence jusqu'à ce qu'il n'y ait plus d'objets à trier.
        """
        objets_a_trier = self.objets
        tri = []
        while len(objets_a_trier) > 0:
            nouveaux_objets_tries = set()
            for obj in objets_a_trier:
                # on cherche à savoir si obj a un père dans la liste des objets à trier
                # si c'est le cas, il n'y a aucune flèche dans self(objets_a_trier-{obj},{obj})
                if next(self(objets_a_trier-{obj},{obj}),None) == None:
                    tri += [obj]
                    nouveaux_objets_tries |= {obj}
            objets_a_trier -= nouveaux_objets_tries
        return tri
                

CatAcyclique = CategorieAcyclique     
    
def test_CategorieAcyclique():
    from CategorieAleatoire import GrapheCompositionAleatoire
    import random

    random.seed(1)
    for i in range(1):
        c = GrapheCompositionAleatoire(20,150)
        c.transformer_graphviz()
        
        c_a = CategorieAcyclique(c)
        c_a.transformer_graphviz()
        c_a.fonct_surjection.transformer_graphviz()
        for obj in c_a.objets:
            c_a.sous_cat_equiv(obj).transformer_graphviz()
        print(c_a.tri_topologique())
    
if __name__ == '__main__':
    test_CategorieAcyclique()