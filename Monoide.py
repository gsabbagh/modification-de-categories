from Morphisme import Morphisme
from CategorieLibre import CategorieLibre
from GrapheDeComposition import MGC,GC, GrapheDeComposition
from CategorieProduit import CategorieProduit
import random
from config import *
import itertools
from typing import *

class ElementMonoide(Morphisme):
    """Un élément de monoïde est une flèche de l'unique objet (1) de la catégorie vers lui-même."""
    def __init__(self, nom:str = None, is_identite:bool = False):
        Morphisme.__init__(self,1,1,nom,is_identite)
        
    def verifier_coherence(self):
        if not (self.source == self.cible == 1):
            raise Exception("ElementMonoide n'a pas pour cible ou pour source 1."+repr(self))

class Monoide(CategorieLibre):
    """Un `Monoide` est une catégorie à un seul objet.
    Les flèches de la catégorie représentent les éléments du monoïde, l'identité l'élément neutre.
    La loi de composition de la catégorie représente la loi de composition interne.
    
    Les classes filles doivent implémenter les méthodes :
        - identite(self,objet:Any = None) -> Morphisme: renvoie l'element neutre peu import l'objet
        - elements(self) -> set : renvoie l'ensemble des éléments du Monoïde où chaque élément est un ElementMonoide.
        ces deux méthodes définissent le graphe sous-jacent avec les objets de la catégorie"""
    
    def __init__(self, nom:str = "Monoide"):
        CategorieLibre.__init__(self,{1},nom)
        
    def __ior__(self, objets:set) -> "Monoide":
        if objets == {1}:
            CategorieLibre.__ior__(self,{1})
            return self
        raise Exception("Tentative d'ajout d'un objet à un monoide : "+str(objets))
        
    def __isub__(self, objets:set) -> "Monoide":
        raise Exception("Tentative de retirer un objet à un monoide : "+str(objets))
        
    def identite(self) -> ElementMonoide:
        raise NotImplementedError("Les classes filles doivent implementer cette methode qui renvoie l'element neutre.")
        
    def elements(self) -> set:
        raise NotImplementedError("Les classes filles doivent implementer cette methode qui renvoie les elements du monoide.")
        
   
class ElementMonoideGC(ElementMonoide,MGC):
    """Un élément de monoïde graphe de composition est un élément de monoïde instanciable utilisé pour le monoïde
    graphe de composition."""
    def __init__(self,nom:str = None,is_identite:bool = False):
        ElementMonoide.__init__(self,nom,is_identite)
        MGC.__init__(self,1,1,nom,is_identite)
        
    def verifier_coherence(self):
        ElementMonoide.verifier_coherence(self)
        MGC.verifier_coherence(self)
   
class MonoideGC(GrapheDeComposition,Monoide):
    """Un monoïde graphe de composition est un monoïde instanciable.
    C'est un graphe de composition à un objet."""
    
    def __init__(self,nom:str = "MonoïdeGC"):
        GrapheDeComposition.__init__(self,nom=nom)
        Monoide.__init__(self,nom=nom)
    
    def identite(self, objet:Any = None) -> ElementMonoideGC:
        return GrapheDeComposition.identite(self,1)
    
    def elements(self) -> Generator[Morphisme,None,None]:
        return GrapheDeComposition.__call__(self,{1},{1})
        
    def __ior__(self, elements:set) -> 'MonoideGC':
        for elem in elements:
            if elem == 1:
                GrapheDeComposition.__ior__(self,{1})
                return self
            if not issubclass(type(elem),ElementMonoideGC):
                raise Exception("Tentative d'ajouter un element de monoide de type inconnu "+str(elem))
            GrapheDeComposition.__ior__(self,{elem})
        return self
            
    def __isub__(self, elements:set) -> 'MonoideGC':
        for elem in elements:
            if not issubclass(type(elem),ElementMonoideGC):
                raise Exception("Tentative de suppression d'un element de monoide de type inconnu "+str(elem))
            GrapheDeComposition.__isub__(self,{elem})
        return self
