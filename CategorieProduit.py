from Categorie import Categorie, unique_generator
from CategorieLibre import CategorieLibre
from Morphisme import Morphisme
import itertools
from typing import *

class MorphismeProduit(Morphisme,tuple):
    """Un morphisme d'une catégorie produit est le produit des morphismes des catégories."""
    
    def __new__(cls, *morphismes:Morphisme, nom:str = None):
        return super().__new__(cls,morphismes)
    
    def __init__(self, *morphismes:Morphisme, nom:str = None):
        Morphisme.__init__(self,tuple(m.source for m in self), tuple(m.cible for m in self),
        str(tuple(m.nom for m in self)) if nom == None else nom, all((m.is_identite for m in self)))
        
    def __matmul__(self, other:'MorphismeProduit') -> 'MorphismeProduit':
        if len(other) != len(self):
            raise Exception("Composition de MorphismeProduit de taille différentes.")
        return MorphismeProduit(*(self[i]@other[i] for i in range(len(self))))
        
    __eq__ = tuple.__eq__
    __hash__ = tuple.__hash__
    __getitem__ = tuple.__getitem__
    __iter__ = tuple.__iter__
    __len__ = tuple.__len__
        
        
class CategorieProduit(tuple,CategorieLibre):
    """Catégorie produit de plusieurs catégories.
    Soient C1,C2,...,Cn n catégories.
    Un objet de C1xC2x...xCn est un n-uplet (o_1,o_2,...,o_n) avec o_i \in |Ci|.
    Un morphisme de C1xC2x...xCn est un n-uplet (m_1,m_2,...,m_n) avec m_i \in Fl(Ci).
    On peut itérer sur C1xC2x...xCn pour obtenir les catégories les unes après les autres."""
    
    def __new__(cls, *categories:Categorie):
        return super().__new__(cls,categories)
    
    def __init__(self, *categories:Categorie):
        CategorieLibre.__init__(self,set(itertools.product(*list(map(abs,categories)))), "x".join(map(str,categories)))
        
    def identite(self, objet:tuple) -> MorphismeProduit:
        return MorphismeProduit(*(self[i].identite(objet[i]) for i in range(len(objet))))
        
    def __getitem__(self, couple_sources_cibles_ou_index:Union[tuple,int]) -> Union[Generator[MorphismeProduit,None,None],Categorie]:
        """Si un couple d'ensembles d'objets est donné en paramètre, renvoie l'ensemble des flèches élémentaires de sources à cibles.
        Si un entier est donné en paramètre, renvoie la nième catégorie du produit."""
        if type(couple_sources_cibles_ou_index) is int:
            return tuple.__getitem__(self,couple_sources_cibles_ou_index)
        else:
            def fleches_elem() -> Generator[MorphismeProduit,None,None]:
                sources,cibles = couple_sources_cibles_ou_index
                for source in sources:
                    for cible in cibles:
                        if len(source) != len(cible):
                            raise Exception("Source et cible de taille différente "+str(source)+" "+str(cible))
                        for nuplet in itertools.product(*tuple(tuple.__getitem__(self,i)[{source[i]},{cible[i]}] for i in range(len(source)))):
                            yield MorphismeProduit(*nuplet)
            return fleches_elem()
        
        
def test_CategorieProduit():
    from GrapheDeComposition import GC,MGC
    from Diagramme import Triangle,DiagrammeIdentite
    
    gc = GC()
    gc |= set("ABC")
    f,g= [MGC('A','B','f'),MGC('B','C','g')]
    gc |= {f,g}
    
    gc_2 = CategorieProduit(gc,gc)
    gc_2.transformer_graphviz(afficher_identites=True)
    gc_2[0].transformer_graphviz()
    morph = set(gc_2({('A','B')},{('C','C')})).pop()
    
if __name__ == '__main__':
    test_CategorieProduit()