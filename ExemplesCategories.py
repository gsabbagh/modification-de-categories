from EnsFinis import EnsParties
from Diagramme import DiagrammeObjets
from ChampActif import ChampActif,Cocone
from CommaCategorie import ObjetCommaCategorie
from Categorie import SousCategoriePleine
from CategorieHomologue import CategorieHomologue
from Cluster import CategorieClusters

cat = EnsParties({1,2,3})
cat.transformer_graphviz()

d1 = DiagrammeObjets(cat,{frozenset({1,2}),frozenset({3})})
d2 = DiagrammeObjets(cat,{frozenset({3,2}),frozenset({1})})

d1.transformer_graphviz()
d2.transformer_graphviz()

c_p1 = ChampActif(d1)
c_p2 = ChampActif(d2)
# c_p1.transformer_graphviz(complet=False)
# c_p1.transformer_graphviz(complet=True)


sous_c_p1 = SousCategoriePleine(c_p1,c_p1.objets_colimites())
sous_c_p1.transformer_graphviz()


sous_c_p2 = SousCategoriePleine(c_p2,c_p2.objets_colimites())
sous_c_p2.transformer_graphviz()

cat_hom = CategorieHomologue({sous_c_p1,sous_c_p2})
cat_hom.transformer_graphviz()

from Cluster import CategorieClusters

cocone_limite_d1 = Cocone(list(sous_c_p1.objets)[0])

cocone_limite_d1.transformer_graphviz()

cat_clust = CategorieClusters(cat,{d1,d2})

for cocone_d2 in c_p2.objets:
    if Cocone(cocone_d2).nadir == cocone_limite_d1.nadir:
        Cocone(cocone_d2).transformer_graphviz()
        for cluster in cat_clust({d1},{d2}):
            print(type(Cocone(cocone_d2).as_Cluster()))
            print(type(cluster))
            print(cocone_limite_d1 == Cocone(cocone_d2).as_Cluster()@cluster)
            cluster.transformer_graphviz()
            (Cocone(cocone_d2).as_Cluster()@cluster).transformer_graphviz()
            
    
    