#!/usr/bin/env python
# -*- coding: utf-8 -*-

import config
if config.GRAPHVIZ_ENABLED:
    from graphviz import Digraph
import itertools
import functools
from Morphisme import Morphisme
from Categorie import Categorie
from typing import *
from GrapheDeComposition import MGC


class Foncteur(Morphisme):
    """Un `Foncteur` est un morphisme de catégorie.
    Un foncteur F est constitué d'une application d'objets et d'une application de morphismes tels que :
        - F(1_A) = 1_F(A)
        - F(fog) = F(f)oF(g)"""
    
    nb_viz = 0

    def __init__(self, categorie_source:Categorie, categorie_cible:Categorie,
    application_objets:dict, application_morphismes:dict, representant:str = None):
        """
        `application_objets` doit être complète,
        `application_morphismes` doit spécifier les images de chaque morphisme élémentaire (pas besoin de spécifier les images des identités).
        L'image des composées est par construction la composée des images.
        """
        is_identite = categorie_cible == categorie_source and\
                      all([obj == application_objets[obj] for obj in application_objets]) and\
                      all([morph == application_morphismes[morph] for morph in application_morphismes])
        Morphisme.__init__(self,categorie_source,categorie_cible
        ,representant if representant != None else 'Foncteur '+str(Morphisme._id),is_identite)
        self._app_objets = application_objets
        self._app_morph = application_morphismes
        for obj in self.source.objets:
            if self.source.identite(obj) not in self._app_morph:
                if obj not in self._app_objets:
                    raise Exception("Incoherence Foncteur : l'image de "+str(obj)+" n'est pas specifiee.")
                self._app_morph[self.source.identite(obj)] = self.cible.identite(self._app_objets[obj])
        if config.TOUJOURS_VERIFIER_COHERENCE:
            self.verifier_coherence()
        
    def __eq__(self,other:'Foncteur') -> bool:
        if not issubclass(type(other),Foncteur):
            return False
        return self.source == other.source and self.cible == other.cible\
        and self._app_objets == other._app_objets and self._app_morph == other._app_morph
    
    def __hash__(self) -> int:
        return hash((self.source,self.cible,frozenset(self._app_objets.items()),frozenset(self._app_morph.items())))
    
    def __matmul__(self,other:'Foncteur') -> 'Foncteur':
        """
            `self` @ `other` <=> `self` o `other`
            On compose les foncteurs en appliquant le premier puis le deuxième.
        """
        if not issubclass(type(other),Foncteur):
            raise Exception("Composition d'un foncteur avec un morphisme de type different : "+str(self)+" o "+str(other))
        return Foncteur(other.source,self.cible,{obj:self(other(obj)) for obj in other.source.objets},\
        {morph:self(other(morph)) for morph in other._app_morph},representant=str(self)+'o'+str(other))
        
    
    def as_diagram(self) -> 'Diagramme':
        '''Renvoie le foncteur en tant que `Diagramme`'''
        import Diagramme
        return Diagramme.Diagramme(self.source,self.cible,self._app_objets,self._app_morph)
        
    def verifier_coherence(self):
        '''Vérifie la cohérence du foncteur.'''
        for obj in self._app_objets:
            if obj in self._app_morph:
                raise Exception("Incoherence foncteur : "+str(obj)+" est a la fois un objet et un morphisme de la categorie source")
        
        for o in self.source.objets:
            if o not in self._app_objets:
                raise Exception("Incoherence foncteur : l'application d'objet n'est pas une application, "+str(o)+" n'a pas d'image")

        ## Respect de l'identite
        for objet in self.source.objets:
            if self._app_morph[self.source.identite(objet)] != self.cible.identite(self._app_objets[objet]):
                raise Exception("Incoherence foncteur : l'image de l'identite de "+str(objet)+" ("+str(self.source.identite(objet))+\
                ") n'est pas l'identite de l'image de l'objet "+str(self._app_objets[objet])+"("+str(self.cible.identite(self._app_objets[objet]))+")\n"+str(self._app_morph))
                
        ## Images de deux flèches composables sont composables
        for m1,m2 in itertools.product(self._app_morph,repeat=2):
            if m1.cible == m2.source:
                if self._app_morph[m1].cible != self._app_morph[m2].source:
                    raise Exception("Incoherence foncteur : deux fleches composables n'ont pas leur image composables : "+str(m1)+
                    " composable avec "+str(m2)+" mais "+str(self._app_morph[m1])+" pas composable avec "+str(self._app_morph[m2]))
                
        ## Image des composées est la composées des images
        for f in self.source(abs(self.source),abs(self.source)):
            for g in self.source({f.cible},abs(self.source)):
                if self(g@f) != self(g)@self(f):
                    raise Exception("Incoherence foncteur : l'image des composees n'est pas la composee des images \n"+\
                    "F(g o f)="+str(self(g@f))+' != '+"F(g) o F(f)="+str(self(g)@self(f))+'\nf = '+str(f)+'\ng = '+str(g)+\
                    "\nF(f) = "+str(self(f))+"\nF(g) = "+str(self(g)))
            
    def __call__(self,param:Any)->Any:
        '''Applique le foncteur à un objet ou un morphisme de la catégorie source.'''
        if param in self._app_objets:
            return self._app_objets[param]
        if param in self._app_morph:
            return self._app_morph[param]
        decompo = self.source.decomposition_morphisme(param)
        print(param,param.source,param.cible)
        print(list(decompo))
        decompo = self.source.decomposition_morphisme(param)
        return functools.reduce(lambda x,y:x@y,[self._app_morph[morph] for morph in decompo])
        
    def transformer_graphviz(self, destination:Union[str,None]=None, afficher_identites:bool = True):
        from GrapheDeComposition import MGC
        Foncteur.nb_viz += 1
        if destination == None:
            destination = "graphviz/"+type(self).__name__+str(Foncteur.nb_viz)
            
        suffixe1_objet = "_1"
        suffixe1_morph = "-1"
            
        graph = Digraph('foncteur')
        graph.attr(concentrate="true" if config.GRAPHVIZ_CONCENTRATE_GRAPHS else "false")
        graph.attr(label="Foncteur "+self.nom)
        with graph.subgraph(name='cluster_0') as cluster:
            cluster.attr(label=self.source.nom)
            nodes = []
            for o in self.source.objets:
                cluster.node(str(o)+suffixe1_objet)
            
            for morph in set(self.source[self.source.objets,self.source.objets])|set(self._app_morph.keys()):
                if not morph.is_identite or afficher_identites:
                    couleur = 'black' if not issubclass(type(morph),MGC) or len(morph) == 1 else 'grey70'
                    cluster.node(str(morph)+suffixe1_morph,style="invis",shape="point")
                    graph.edge(str(morph.source)+suffixe1_objet,str(morph)+suffixe1_morph,color=couleur,label=str(morph)+suffixe1_morph,headclip="False",arrowhead="none")
                    graph.edge(str(morph)+suffixe1_morph,str(morph.cible)+suffixe1_objet,color=couleur,tailclip="false")
        
        suffixe2_objet = "_2"
        suffixe2_morph = "-2"

        with graph.subgraph(name='cluster_1') as cluster:
            cluster.attr(label=self.cible.nom)
            nodes = []
            for o in self.cible.objets:
                cluster.node(str(o)+suffixe2_objet)
            fleches2 = set(self.cible[self.cible.objets,self.cible.objets])|set(self._app_morph.values())
            for morph in fleches2:
                if not morph.is_identite or afficher_identites or morph in self._app_morph.values():
                    couleur = 'black' if not issubclass(type(morph),MGC) or len(morph) == 1 else 'grey70'
                    cluster.node(str(morph)+suffixe2_morph,style="invis",shape="point")
                    graph.edge(str(morph.source)+suffixe2_objet,str(morph)+suffixe2_morph,color=couleur,label=str(morph)+suffixe2_morph,headclip="False",arrowhead="none")
                    graph.edge(str(morph)+suffixe2_morph,str(morph.cible)+suffixe2_objet,color=couleur,tailclip="false")
        
        identites_ajoutees = set()
        for source in self._app_morph:
            if not source.is_identite or afficher_identites:
                if self(source).is_identite and self(source) not in identites_ajoutees:
                    identites_ajoutees |= {self(source)} 
                    with graph.subgraph(name='cluster_1') as cluster:
                        morph = self(source)
                        couleur = 'black' if not issubclass(type(morph),MGC) or len(morph) == 1 else 'grey70'
                        cluster.node(str(morph)+suffixe2_morph,style="invis",shape="point")
                        graph.edge(str(morph.source)+suffixe2_objet,str(morph)+suffixe2_morph,color=couleur,label=str(morph)+suffixe2_morph,headclip="False",arrowhead="none")
                        graph.edge(str(morph)+suffixe2_morph,str(morph.cible)+suffixe2_objet,color=couleur,tailclip="false")
                fleche2 = {f for f in fleches2 if f == self(source)}.pop()
                graph.edge(str(source)+suffixe1_morph,str(fleche2)+suffixe2_morph,color="cyan")

        for source in self._app_objets:
            obj1 = {e for e in self.source.objets if e == source}.pop()
            obj2 = {e for e in self.cible.objets if e == self(source)}.pop()
            graph.edge(str(obj1)+suffixe1_objet,str(obj2)+suffixe2_objet,color="blue")
        
        graph.render(destination)
        if config.CLEAN_GRAPHVIZ_MODEL:
            import os
            os.remove(destination) 
    
def test_Foncteur():
    from GrapheDeComposition import GC,MGC
    triangle = GC()
    triangle|= {'A','B','C'}
    
    f1,g1= [MGC('A','B','f'),MGC('B','C','g')]
    triangle |= {f1,g1}
    
    carre = GC()
    carre |= {1,2,3,4}

    f,g,h,i = [MGC(1,2,'f'),MGC(2,4,'g'),MGC(1,3,'h'),MGC(3,4,'i')]
    morphismes_carre = {f,g,h,i}
    carre |= morphismes_carre
    
    pentagone = GC()
    pentagone |= set("VWXYZ")
    a,b,c,d,e = [MGC('X','Y','a'),MGC('Y','Z','b'),MGC('W','V','c'),MGC('W','X','d'),MGC('V','Z','e')]
    pentagone |= {a,b,c,d,e}
    pentagone.transformer_graphviz()
    
    app_obj = {'A':1,'B':2,'C':4}
    app_morph = {f1:f,g1:g}
    fonct1 = Foncteur(triangle, carre, app_obj, app_morph)
    fonct1.transformer_graphviz()
    
    app_obj = {1:'W',2:'V',3:'Y',4:'Z'}
    app_morph = {f:c,g:e,h:a@d,i:b}
    fonct2 = Foncteur(carre,pentagone,app_obj,app_morph)
    fonct2.transformer_graphviz()
    
    (fonct2@fonct1).transformer_graphviz()
    

if __name__ == '__main__':
    test_Foncteur()
    