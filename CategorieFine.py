from Categorie import Categorie
from CategorieQuotient import CategorieQuotient
import itertools

from typing import *

class CategorieFine(CategorieQuotient):
    """`CategorieFine` peut être abrégé en CatFine
    `CategorieFine` est similaire à `CategoriePreordre`, elle passe par une catégorie quotient contrairement à cette dernière.
    Catégorie quotientée par la relation d'équivalence sur les morphismes suivante :
     f : A->B ~ g : C->D ssi A = C et B = D"""
    
    def __init__(self,categorie_a_quotienter:Categorie, nom:str = None):
        if nom == None:
            nom = "Catégorie fine engendrée par "+str(categorie_a_quotienter)    
        CategorieQuotient.__init__(self,categorie_a_quotienter,nom)
        for obj1, obj2 in itertools.product(categorie_a_quotienter.objets,repeat=2):
            self.identifier_ensemble_morphismes(set(categorie_a_quotienter({obj1},{obj2})))

CatFine = CategorieFine                  
    
def test_CategorieFine():
    from CategorieAleatoire import GrapheCompositionAleatoire
    import random

    random.seed(3)
    for i in range(3):
        c = GrapheCompositionAleatoire()
        c.transformer_graphviz()
        
        c_a = CategorieFine(c)
        c_a.transformer_graphviz()
        c_a.fonct_surjection.transformer_graphviz()
        
   
    
if __name__ == '__main__':
    test_CategorieFine()