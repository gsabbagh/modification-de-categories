from Categorie import Categorie
from CategorieAcyclique import CatAcyclique
from CategorieQuotient import CategorieQuotient
from Morphisme import Morphisme
import itertools

class CategorieOrdre(CatAcyclique):
    """`CategorieOrdre` peut être abrégé en `CatOrdre`.
    Catégorie acyclique telle qu'il ne peut pas y avoir plus d'une flèche entre deux objets."""
    
    def __init__(self,categorie_a_quotienter:Categorie, nom:str = None):
        if nom == None:
            nom = "Catégorie ordre engendrée par "+str(categorie_a_quotienter)    
        CatAcyclique.__init__(self,categorie_a_quotienter,nom)
        for obj1, obj2 in itertools.product(self.objets,repeat=2):
            self.identifier_ensemble_morphismes(set(categorie_a_quotienter(obj1,obj2)))                

CatOrdre = CategorieOrdre     
    
def test_CategorieOrdre():
    from CategorieAleatoire import GrapheCompositionAleatoire
    import random

    random.seed(1)
    for i in range(1):
        c = GrapheCompositionAleatoire(20,150)
        c.transformer_graphviz()
        
        c_a = CatOrdre(c)
        c_a.transformer_graphviz()
        c_a.fonct_surjection.transformer_graphviz()
        for obj in c_a.objets:
            c_a.sous_cat_equiv(obj).transformer_graphviz()
        print(c_a.tri_topologique())
        
        for morph in c_a(c_a.objets,c_a.objets):
            morph.sous_cat_equiv().transformer_graphviz()
    
if __name__ == '__main__':
    test_CategorieOrdre()