from CategorieAleatoire import GCA
from CommaCategorie import CategorieFleches, CommaCategorie
from Diagramme import DiagrammeObjets, DiagrammeIdentite
import random

random.seed(124)
# random.seed(1243456278)


gc = GCA()
gc.transformer_graphviz()
gc.loi_de_composition_to_csv("lois de composition/decompo.csv")


# cat_fl = CategorieFleches(gc)
# for obj in cat_fl.objets:
    # if obj.f.is_identite:
        # print("supprime "+str(obj))
        # cat_fl -= {obj}
# cat_fl.transformer_graphviz()



comma = CommaCategorie(DiagrammeObjets(gc,gc.objets),DiagrammeIdentite(gc))

# for obj in comma.objets:
    # if obj.f.is_identite:
        # print("supprime "+str(obj))
        # comma -= {obj}
comma.transformer_graphviz()

# comma = CommaCategorie(DiagrammeIdentite(gc),DiagrammeObjets(gc,gc.objets))


# for obj in comma.objets:
    # if obj.f.is_identite:
        # print("supprime "+str(obj))
        # comma -= {obj}
# comma.transformer_graphviz()


# for obj in comma.objets:
    # if obj.f.is_identite:
        # print("supprime "+str(obj))
        # comma -= {obj}
        
# comma.transformer_graphviz()