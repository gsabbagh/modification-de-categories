#!/usr/bin/env python
# -*- coding: utf-8 -*-
from typing import *

class Morphisme:
    """
    Classe abstraite qui définit ce qu'est un morphisme dans une catégorie.
    Un morphisme a une source et une cible, il contient un booléen is_identite qui indique si le morphisme est une identité.
    
    Toutes les classes filles de Morphisme doivent implémenter les opérateur suivants :
        - __matmul__ : g@f est la composée gof
        
    Si un morphisme est défini comme étant la composée de morphismes élémentaires,
    le morphisme doit implémenter la méthode __iter__ et renvoyer ses morphismes élémentaires constituants.
    """
    _id = 0
    def __init__(self, source:Any, cible:Any, nom:str = None, is_identite:bool = False):
        """`nom` es un str qui représente le morphisme pour l'affichage."""
        self._id = Morphisme._id
        Morphisme._id += 1
        self.source = source
        self.cible = cible
        self.is_identite = is_identite
        if nom == None:
            self.nom = str(self._id)
        else:
            self.nom = nom
        if is_identite and source != cible:
            raise Exception("Identite de source et cible differente : "+str(source)+"->"+str(cible))
            
    def __str__(self) -> str:
        return str(self.nom)
        
    def pretty_print(self) -> str:
        return str(self.nom)+' : '+str(self.source)+" -> "+str(self.cible)
        
    def __repr__(self) -> str:
        return self.pretty_print()+","+super().__repr__().split('object at ')[-1].split('>')[0]

    def __matmul__(self,other:'Morphisme') -> 'Morphisme':
        raise NotImplementedError("Les classes filles doivent implémenter la composition des morphismes.")
        
    def __iter__(self) -> 'Morphisme':
        raise NotImplementedError("Les classes filles composees de morphismes elementaires doivent implementer cette methode.")
        
    def __lt__(self,other:'Morphisme') -> bool:
        """Pour trier les Morphismes pour la réplicabilité."""
        return self._id < other._id
