from Categorie import Categorie
from CategorieOrdre import CatOrdre
from CategorieQuotient import CategorieQuotient
import copy
import itertools

from typing import *   
   
class CategorieOrdreTotal(CatOrdre):
    """`CategorieOrdreTotal` peut être abrégé en `CatOrdreTotal`
    Catégorie qui correspond à une relation d'ordre totale : catégorie acyclique telle que tous les éléments sont comparables (toutes les flèches sont composables).
    De plus, au sein d'un objet classe d'équivalence, il n'y a aucune flèche d'un objet vers un autre.
    Autre propriété intéressante : au sein d'une classe d'équivalence, chaque élément à un père parmi les éléments d'une classe d'équivalence parente. 
    On fait un tri topologique. En suivant le tri, les objets sont équivalents tant qu'il n'y a aucune flèche intraclasse.
    """
    
    def __init__(self,categorie_a_quotienter:Categorie, nom:str = None):
        if nom == None:
            nom = "Catégorie ordre total engendrée par "+str(categorie_a_quotienter)    
        CatOrdre.__init__(self,categorie_a_quotienter,nom)
        tri = self.tri_topologique()
        classe_equiv_en_cours = set()
        for obj in tri:
            if next(categorie_a_quotienter(classe_equiv_en_cours,obj),None) == None:
                classe_equiv_en_cours |= obj
            else:
                self.identifier_ensemble_morphismes(set(map(lambda x:categorie_a_quotienter.identite(x),classe_equiv_en_cours)))
                classe_equiv_en_cours = obj
        self.identifier_ensemble_morphismes(set(map(lambda x:categorie_a_quotienter.identite(x),classe_equiv_en_cours)))
        for classe1,classe2 in itertools.product(self.objets,repeat=2):
            self.identifier_ensemble_morphismes(set(categorie_a_quotienter(classe1,classe2)))
            

            

CatOrdreTotal = CategorieOrdreTotal

def test_CatOrdreTotal():
    from CategorieAleatoire import GrapheCompositionAleatoire
    import random

    random.seed(3)
    for i in range(10):
        c = GrapheCompositionAleatoire()
        c.transformer_graphviz()
        c_ot = CatOrdreTotal(c)
        c_ot.transformer_graphviz()

if __name__ == '__main__':
    test_CatOrdreTotal()